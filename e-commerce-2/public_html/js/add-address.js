var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    login()

    cargarComboDepartamento("#cbodepartamentomodal", "seleccione");

    leerDatos()
});

function direccion() {
    var paramstr = window.location.search.substr(1);
    var paramarr = paramstr.split("?");
    var params = {};

    for (var i = 0; i < paramarr.length; i++) {
        var tmparr = paramarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }

    return params['dir'];
}

function leerDatos() {

    if (direccion()) {
        $("#titulo-add-address").html('<span ><i class="fa fa-map-marker"></i> Modifica tu dirección </span>')

        var ruta = DIRECCION_WS + "direccion.cliente.leerdatos.php";
        var token = $.cookie('token');
        var id_cliente = $.cookie('codigo_usuario');
        var id_direccion = direccion();

        $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion}, function () {
        }).done(function (resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                $.each(datosJSON.datos, function (i, item) {

                    $("#txtdireccion").val(item.direccion)
                    $("#txttelefono").val(item.telefono)
                    $("#txtcelular").val(item.celular)


                    $("#cbodepartamentomodal").val(item.id_departamento)
                    $("#cbodepartamentomodal").change()
                    $("#cbodepartamentomodal").on("", function () {
                        alert("hola" + " - " + item.id_provincia)
                        $("#cboprovinciamodal").val(item.id_provincia);
                    });
//
                    $("#cboprovinciamodal").val(item.id_provincia)
                    $("#cboprovinciamodal").change();

//                    $("#cboprovinciamodal").on("shown.bs.select", function () {
//                        $("#cbodistritomodal").val(item.id_distrito)
//                    });

                    $("#titulomodal").text("Editar Dirección.");
                });
            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    } else {
        $("#titulo-add-address").html('<span ><i class="fa fa-map-marker"></i> Añade tus direcciones </span>')
    }
}

function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    if ($.cookie('token') == "" || $.cookie('token') == null) {
        window.location = "index.html"
    }

    $("#sesion-login").html(html);
}

$("#cbodepartamentomodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", id_departamento);
});

$("#cboprovinciamodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    var id_provincia = $("#cboprovinciamodal").val();
    cargarComboDistrito("#cbodistritomodal", "seleccione", id_departamento, id_provincia);
});

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if (direccion()) {
                var ruta = DIRECCION_WS + "direccion.cliente.editar.php";
                var token = $.cookie('token');

                var id_cliente = $.cookie('codigo_usuario');
                var id_direccion = $("#txtnrodireccion").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var celular = $("#txtcelular").val()
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion, direccion: direccion, telefono: telefono, celular: celular, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {
                var ruta = DIRECCION_WS + "direccion.cliente.agregar.personal.php";
                var token = $.cookie('token');

                var id_cliente = $.cookie('codigo_usuario');
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var celular = $("#txtcelular").val()
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, direccion: direccion, telefono: telefono, celular: celular, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        window.location = "my-address.html"
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }

        }
    });
});