var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    login()

    leerDatos()
});

function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    if ($.cookie('token') == "" || $.cookie('token') == null) {
        window.location = "index.html"
    }

    $("#sesion-login").html(html);
}

$("#txtrepassword").focusout(function () {
    if ($("#txtpassword").val() !== $("#txtrepassword").val()) {
        swal("Contraseñas no coinciden", "", "warning");
        $("#txtpassword").val("");
        $("#txtrepassword").val("");
        $("#txtpassword").focus();
        return 0;
    }
});

$("#txtpasswordviejo").focusout(function () {
    if ($("#txtpasswordviejo").val().length > 0) {
        var password = md5($("#txtpasswordviejo").val());
        var id_cliente = $.cookie('codigo_usuario');
        var token = $.cookie('token');

        var ruta = DIRECCION_WS + "cliente.comprobar.password.php";

        $.post(ruta, {token: token, id_cliente: id_cliente, password: password}, function () {
        }).done(function (resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {
                $.each(datosJSON.datos, function (i, item) {
                    if (item.estado === 500) {
                        swal("Mensaje del sistema", item.dato, "warning");
                        $("#txtpasswordviejo").val("");
                        $("#txtpasswordviejo").focus();
                        return 0;
                    }
                });
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    }
});

function leerDatos() {

    var ruta = DIRECCION_WS + "cliente.leerdatos.cliente.php";
    var token = $.cookie('token');
    var id_cliente = $.cookie('codigo_usuario');

    $.post(ruta, {token: token, id_cliente: id_cliente}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#txtdniruc").val(item.id_cliente)
                $("#txtnomdocruc").val(item.nombre_razonsocial)
                $("#txtemail").val(item.email)

            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txtpasswordviejo").val().length > 0) {
                var ruta = DIRECCION_WS + "cliente.editar.cliente.password.php";
                var token = $.cookie('token');

                var id_cliente = $.cookie('codigo_usuario')
                var nombre_razonsocial = $("#txtnomdocruc").val()
                var email = $("#txtemail").val()
                var clave = md5($("#txtpassword").val())

                $.post(ruta, {token: token, id_cliente: id_cliente, nombre_razonsocial: nombre_razonsocial, email: email, clave: clave}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $.cookie('nombreUsuario',$("#txtnomdocruc").val())
                        $("#btncerrar").click(); //cerrar ventana
                        window.location = "account.html"
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {
                var ruta = DIRECCION_WS + "cliente.editar.cliente.php";
                var token = $.cookie('token');

                var id_cliente = $.cookie('codigo_usuario')
                var nombre_razonsocial = $("#txtnomdocruc").val()
                var email = $("#txtemail").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, nombre_razonsocial: nombre_razonsocial, email: email}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $.cookie('nombreUsuario',$("#txtnomdocruc").val())
                        $("#btncerrar").click(); //cerrar ventana
                        window.location = "account.html"
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});