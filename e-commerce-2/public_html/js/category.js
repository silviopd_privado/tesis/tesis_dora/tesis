var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {

    login()    
    listarCategorias()
});


function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    $("#sesion-login").html(html);
    
}

function listarCategorias() {

    var ruta = DIRECCION_WS + "marca.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<li><a href="category.html"> ' + item.nombre + '</a></li>';
            });

            $("#todas-marcas").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}