var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

var producto = new Array();

$(document).ready(function () {

    listarProductos()
    listarCategorias()
    login()
    inicio()
});

function inicio() {
    if ($.cookie('listaProductos') != null) {
        agregar2()
    }        
}

function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    $("#sesion-login").html(html);

}

function listarProductos() {

    var ruta = DIRECCION_WS + "producto.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<div class="item col-lg-3 col-md-3 col-sm-4 col-xs-6">';
                html += '<div class="product">';
                html += '<div class="image">';
                html += '<a href="product-details.html?prod=' + item.id_producto + '"><img src="' + item.foto + '" alt="img" class="img-responsive"></a>';
                html += '<div class="promotion"><span class="new-product">DESCUENTO</span>';
                html += '<span class="discount">$' + item.descuento + '</span>';
                html += '</div>';
                html += '</div>';
                html += '<div class="description">';
                html += '<h4><a href="product-details.html">' + item.nombre + ' - ' + item.id_producto + '</a></h4>';
                html += '<p>' + item.descripcion + '</p>';
                html += '</div>';
                html += '<span class="size">MARCA: ' + item.marca + '</span>';
                html += '<div class="price"><span>$' + item.precio + '</span></div>';
                html += '<div class="action-control"><a class="btn btn-primary" onclick="agregar(' + item.id_producto + ')"> <span class="add2cart">';
                html += '<i class="glyphicon glyphicon-shopping-cart" > </i> Add to cart </span> </a></div>';
                html += '</div>';
                html += '</div>';
            });

            $("#producto-lista").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function listarCategorias() {

    var ruta = DIRECCION_WS + "marca.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<li><a href="category.html"> ' + item.nombre + '</a></li>';
            });

            $("#todas-marcas").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$(document).on("change", "#cantidadmodal", function () {

    $("#carrito-web tr").each(function (i) {
        var cantidad = document.getElementsByName("cantidadmodal")[i].value;
        var precio = document.getElementsByName("preciomodal")[i].innerHTML;
        var total = precio * cantidad;
        document.getElementsByName("frmpreciototal")[i].innerHTML = parseFloat(total).toFixed(2);

        calcularTotales()

        var id_producto = document.getElementsByName("id_producto")[i].innerHTML;
        if (producto[i]["id_producto"] == id_producto) {
            producto[i]["cantidad"] = cantidad;
        }
    });

    $.removeCookie('listaProductos');
    $.cookie('listaProductos', JSON.stringify(producto))
//    alert($.cookie('listaProductos'))
});

function agregar(id_producto) {

    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var html = "";
    var filtro = "no";

    $.post(ruta, {id_producto: id_producto}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            $.each(datosJSON.datos, function (i, item) {
                //AGREGAR PRODUCTOS
                html += '<tr class="miniCartProduct" id="productomodal">';
                html += '<td style="width:20%" class="miniCartProductThumb">';
                html += '<div><a href="product-details.html?prod=' + item.id_producto + '"> <img src="' + item.foto + '" alt="img">';
                html += '</a></div>';
                html += '</td>';
                html += '<td style="width:40%">';
                html += '<div class="miniCartDescription">';
                html += '<h4><a href="product-details.html">' + item.nombre + '</a></h4>';
                html += '<span class="size">' + item.marca + '</span>';
                html += '<div class="price" id="precio" name="precio"><span> $' + item.precio + ' </span></div>';
                html += '</div>';
                html += '</td>';
                html += '<td ><input id="cantidadmodal"  name="cantidadmodal" step="any" type="number" min="1" value="1" style="width:70%"></td>';
                html += '<td style="width:15%" class="miniCartSubtotal" id="frmpreciototal" name="frmpreciototal"> ' + item.precio + '</td>';
                html += '<td style="display:none;" id="id_producto" name="id_producto"> ' + item.id_producto + '</td>';
                html += '<td style="display:none;" id="preciomodal" name="preciomodal"> ' + item.precio + '</td>';
                html += '<td style="width:5%" class="delete" id="celiminar"><a href="javascript:void()"> <i class="fa fa-close text-danger"></i> </a></td>';
                html += '</tr>';

                $("#carrito-web tr").each(function () {
                    var id_producto = $(this).find("td").eq(4).html();

                    if (id_producto == item.id_producto) {
                        filtro = "si";
                    } else {
                        filtro = "no";
                    }
                });

            });

            if (filtro == "no") {
                $("#carrito-web").append(html);

                //cookie//
                var productoDetalle = new Object()
                productoDetalle.id_producto = id_producto;
                productoDetalle.cantidad = 1;
                producto.push(productoDetalle);

//                $.removeCookie('listaProductos');
                $.cookie('listaProductos', JSON.stringify(producto))
//                alert($.cookie('listaProductos'))
            }

            calcularTotales()

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}
function agregar2() {

    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var html = "";
    var producto = JSON.parse($.cookie('listaProductos'))

    for (var i = 0; i < producto.length; i++) {

        var id_producto = producto[i]["id_producto"]
        var cantidad = producto[i]["cantidad"]
//        alert(id_producto + " - " + cantidad)

        $.post(ruta, {id_producto: id_producto}, function () {
        }).done(function (resultado) {

            var datosJSON = resultado;
            if (datosJSON.estado === 200) {

                $.each(datosJSON.datos, function (i, item) {
                    //AGREGAR PRODUCTOS
                    html += '<tr class="miniCartProduct" id="productomodal">';
                    html += '<td style="width:20%" class="miniCartProductThumb">';
                    html += '<div><a href="product-details.html"> <img src="' + item.foto + '" alt="img">';
                    html += '</a></div>';
                    html += '</td>';
                    html += '<td style="width:40%">';
                    html += '<div class="miniCartDescription">';
                    html += '<h4><a href="product-details.html">' + item.nombre + '</a></h4>';
                    html += '<span class="size">' + item.marca + '</span>';
                    html += '<div class="price" id="precio" name="precio"><span> $' + item.precio + ' </span></div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td ><input id="cantidadmodal"  name="cantidadmodal" step="any" type="number" min="1" value= ' + cantidad + ' " style="width:70%"></td>';
                    html += '<td style="width:15%" class="miniCartSubtotal" id="frmpreciototal" name="frmpreciototal"> ' + parseFloat(item.precio * cantidad).toFixed(2) + '</td>';
                    html += '<td style="display:none;" id="id_producto" name="id_producto"> ' + item.id_producto + '</td>';
                    html += '<td style="display:none;" id="preciomodal" name="preciomodal"> ' + item.precio + '</td>';
                    html += '<td style="width:5%" class="delete" id="celiminar"><a href="javascript:void()"> <i class="fa fa-close text-danger"></i> </a></td>';
                    html += '</tr>';
                });

                $("#carrito-web").html(html);

                calcularTotales()

            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    }
}

$(document).on("click", "#celiminar", function () {
    var filaEliminar = $(this).parents().get(0);

//    alert(document.getElementsByName("id_producto").innerHTML)

    filaEliminar.remove();//<-elimina        

    calcularTotales()
});

function calcularTotales() {

    var precioTotal = 0;

    var html2 = "";
    var html3 = "";

    $("#carrito-web tr").each(function () {
        var precio = $(this).find("td").eq(3).html();
        precioTotal += parseFloat(precio);
    });

    //CALCULAR TOTALES
    html2 += '<h3 class="text-right subtotal"> $' + parseFloat(precioTotal).toFixed(2) + '</h3>';
    html2 += '<a class="btn btn-sm btn-danger" href="cart.html"> <i class="fa fa-shopping-cart"> </i> VIEW';
    html2 += 'CART </a>';
    html2 += '<a class = "btn btn-sm btn-primary" href="checkout-1.html"> CHECKOUT </a>';
    //CALCULAR TOTALES


    //CALCULAR TOTALES DROPDOWN
    html3 += '<i class="fa fa-shopping-cart"> </i> ';
    html3 += '<span class="cartRespons"> Cart ($' + parseFloat(precioTotal).toFixed(2) + ') </span>';
    html3 += '<b class="caret"> </b>';
    //CALCULAR TOTALES DROPDOWN


    $("#calcular-totales").html(html2);
    $("#calcular-totales-dropdown").html(html3);
}
