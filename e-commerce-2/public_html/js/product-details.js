var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    datosProducto();
    login()
});

$(document).on("change", "#cantidadmodal1", function () {

    var total = 0;
    var precio = document.getElementById("preciomodal1").innerHTML;
    var cantidad = $("#cantidadmodal1").val();

    total = precio * cantidad;

    var html = "";
    html += '<label>PRECIO TOTAL: ' + parseFloat(total).toFixed(2) + '</label>';
    $("#frmpreciototal1").html(html);

    calcularTotales()
});

function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    if ($.cookie('token') == "" || $.cookie('token') == null) {
        window.location = "index.html"
    }

    $("#sesion-login").html(html);
}

$(document).on("change", "#cantidadmodal", function () {
    
    var total = 0;
    var precio = document.getElementById("preciomodal").innerHTML;

    var cantidad = $("#cantidadmodal").val();

    total = precio * cantidad;
    var html = "";
    html += '' + parseFloat(total).toFixed(2) + '';
    $("#frmpreciototal").html(html);

    calcularTotales()
});

function calcularTotales() {

    var precioTotal = 0;

    var html2 = "";
    var html3 = "";

    $("#carrito-web tr").each(function () {
        var precio = $(this).find("td").eq(3).html();
        precioTotal += parseFloat(precio);
    });

    //CALCULAR TOTALES
    html2 += '<h3 class="text-right subtotal"> $' + parseFloat(precioTotal).toFixed(2) + '</h3>';
    html2 += '<a class="btn btn-sm btn-danger" href="cart.html"> <i class="fa fa-shopping-cart"> </i> VIEW CART </a>';
    html2 += '<a class = "btn btn-sm btn-primary" > CHECKOUT </a>';
    //CALCULAR TOTALES


    //CALCULAR TOTALES DROPDOWN
    html3 += '<i class="fa fa-shopping-cart"> </i> ';
    html3 += '<span class="cartRespons"> Cart ($' + parseFloat(precioTotal).toFixed(2) + ') </span>';
    html3 += '<b class="caret"> </b>';
    //CALCULAR TOTALES DROPDOWN


    $("#calcular-totales").html(html2);
    $("#calcular-totales-dropdown").html(html3);
}

function datosProducto() {
    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var id_producto;
    
    var paramstr = window.location.search.substr(1);
    var paramarr = paramstr.split("?");
    var params = {};

    for (var i = 0; i < paramarr.length; i++) {
        var tmparr = paramarr[i].split("=");
        params[tmparr[0]] = tmparr[1];
    }

    if (params['prod']) {
        id_producto = params['prod'];

        $.post(ruta, {id_producto: id_producto}, function () {
        }).done(function (resultado) {
            var datosJSON = resultado;
            if (datosJSON.estado === 200) {

                var html = "";
                var html2 = "";
                var html3 = "";

                $.each(datosJSON.datos, function (i, item) {
                    /*producto-barra*/
                    html += '<ul class="breadcrumb">';
                    html += '<li><a href="index.html">Home</a></li>';
                    html += '<li><a href="category.html?mar=' + item.id_marca + '">' + item.marca + '</a></li>';
                    html += '<li><a href="category.html?cat=' + item.id_categoria + '">' + item.categoria + '</a></li>';
                    html += '<li class="active">' + item.nombre + '</li>';
                    html += '</ul>';
                    /*producto-barra*/

                    /*imagen-producto*/
                    html2 += '<a href="' + item.foto + '"><img src="' + item.foto + '" class="img-responsive" alt="img"></a>';
                    /*imagen-producto*/

                    /*detalle-producto*/
                    html3 += '<h1 class="product-title">' + item.nombre + '</h1>';
                    html3 += '<h3 class="product-code">Product Code : ' + item.id_producto + '</h3>';
                    html3 += '<div class="product-price"><span class="price-sales"> S/. ' + item.precio + '</span>';// <span class="price-standard">$95</span>';
                    html3 += '<div id="preciomodal1" name="preciomodal1" hidden="">' + item.precio + '</div>';
                    html3 += '</div>';
                    html3 += '<div class="details-description">';
                    html3 += '<p>' + item.descripcion + '</p>';
                    html3 += '</div>';
                    html3 += '<div class="productFilter productFilterLook2">';
                    html3 += '<div class="row">';
                    html3 += '<div class="col-lg-6 col-sm-6 col-xs-6">';
                    html3 += '<div class="filterBox">';
                    html3 += '<input id="cantidadmodal1" name="cantidadmodal1" step="any" type="number" min="1" value="1">';
                    html3 += '</div>';
                    html3 += '</div>';
                    html3 += '<div class="col-lg-6 col-sm-6 col-xs-6">';
                    html3 += '<div class="filterBox" id="frmpreciototal1">';
                    html3 += '<label>PRECIO TOTAL: ' + item.precio + '</label>';
                    html3 += '</div>';
                    html3 += '</div>';
                    html3 += '</div>';
                    html3 += '</div>';
                    html3 += '<div class="cart-actions">  ';
                    html3 += '<div class="addto row">';
                    html3 += '<div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">';
                    html3 += '<button onclick="agregar(' + item.id_producto + ')" class="button btn-block btn-cart cart first" title="Add to Cart" type="button">Agrega Al Carrito';
                    html3 += '</button>';
                    html3 += '</div>';
                    html3 += '</div>';
                    html3 += '<div style="clear:both"></div>';
                    html3 += '<h3 class="incaps"><i class="fa fa fa-check-circle-o color-in"></i> In stock</h3>';
                    html3 += '<h3 style="display:none" class="incaps"><i class="fa fa-minus-circle color-out"></i> Out of stock </h3>';
                    html3 += '<h3 class="incaps"><i class="glyphicon glyphicon-lock"></i> Secure online ordering</h3>';
                    html3 += '</div>';
                    html3 += '<div class="clear"></div>';
                    html3 += '<div class="product-tab w100 clearfix">';
                    html3 += '<ul class="nav nav-tabs">';
                    html3 += '<li class="active"><a href="#details" data-toggle="tab">Detalles</a></li>';
                    html3 += '<li><a href="#size" data-toggle="tab">Size</a></li>';
                    html3 += '</ul>';
                    html3 += '<div class="tab-content">';
                    html3 += '<div class="tab-pane active" id="details">';
                    html3 += 'Nombre Producto: ' + item.nombre + '<br>';
                    html3 += 'Precio: S/. ' + item.precio + '<br>';
                    html3 += 'Unidad/Caja: ' + item.unidad_x_caja + '<br>';
                    html3 += 'Marca: ' + item.marca + '<br>';
                    html3 += 'Categoria: ' + item.categoria + '<br>';
                    html3 += '</div>';
                    html3 += '<div class="tab-pane" id="size"> 16" waist<br>';
                    html3 += '34" inseam<br>';
                    html3 += '10.5" front rise<br>';
                    html3 += '8.5" knee<br>';
                    html3 += '7.5" leg opening<br>';
                    html3 += '<br>';
                    html3 += 'Measurements taken from size 30<br>';
                    html3 += 'Model wears size 31. Model is 62 <br>';
                    html3 += '<br>';
                    html3 += '</div>';

                    html3 += '</div>';
                    html3 += '</div>';
                    /*detalle-producto*/
                });

                /*producto-barra*/
                $("#producto-barra").html(html);
                /*producto-barra*/

                /*imagen-producto*/
                $("#imagen-producto").html(html2);
                /*imagen-producto*/

                /*detalle-producto*/
                $("#detalle-producto").html(html3);
                /*detalle-producto*/

            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function (error) {
            $("#txtEmail").val("");
            $("#txtPassword").val("");
            $("#txtEmail").focus();

            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");

        })
    } else if (params['prod'] == null) {
        window.location = "index.html"
    }
}

function agregar(id_producto) {

    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var html = "";
    var filtro = "no";

    $.post(ruta, {id_producto: id_producto}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            $.each(datosJSON.datos, function (i, item) {
                //AGREGAR PRODUCTOS
                html += '<tr class="miniCartProduct" id="productomodal">';
                html += '<td style="width:20%" class="miniCartProductThumb">';
                html += '<div><a href="product-details.html"> <img src="' + item.foto + '" alt="img">';
                html += '</a></div>';
                html += '</td>';
                html += '<td style="width:40%">';
                html += '<div class="miniCartDescription">';
                html += '<h4><a href="product-details.html">' + item.nombre + '</a></h4>';
                html += '<span class="size">' + item.marca + '</span>';
                html += '<div class="price" id="precio"><span> $' + item.precio + ' </span></div>';
                html += '</div>';
                html += '</td>';
                html += '<td ><input id="cantidadmodal"  name="cantidadmodal" step="any" type="number" min="1" value="1" style="width:70%"></td>';
                html += '<td style="width:15%" class="miniCartSubtotal" id="frmpreciototal"> ' + item.precio + '</td>';
                html += '<td style="display:none;" id="id_producto"> ' + item.id_producto + '</td>';
                html += '<td style="display:none;" id="preciomodal"> ' + item.precio + '</td>';
                html += '<td style="width:5%" class="delete" id="celiminar"><a href="javascript:void()"> <i class="fa fa-close text-danger"></i> </a></td>';
                html += '</tr>';

                $("#carrito-web tr").each(function () {
                    var id_producto = $(this).find("td").eq(4).html();
                    
                    if (id_producto == item.id_producto) {
                        filtro = "si";
                    }else{
                        filtro = "no";
                    }
                });
//                $.cookie('productos', item.id_producto)
            });

            if (filtro == "no") {
                $("#carrito-web").append(html);
            }
            

            calcularTotales()

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

$(document).on("click", "#celiminar", function () {
    var filaEliminar = $(this).parents().get(0);

    filaEliminar.remove();//<-elimina    

    calcularTotales()
});

