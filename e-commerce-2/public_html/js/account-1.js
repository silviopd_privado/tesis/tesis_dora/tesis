var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    cargarComboDepartamento("#cbodepartamento", "seleccione");
});

$("#cbodepartamento").change(function () {
    var id_departamento = $("#cbodepartamento").val();
    cargarComboProvincia("#cboprovincia", "seleccione", id_departamento);
});

$("#cboprovincia").change(function () {
    var id_departamento = $("#cbodepartamento").val();
    var id_provincia = $("#cboprovincia").val();
    cargarComboDistrito("#cbodistrito", "seleccione", id_departamento, id_provincia);
});

$("#txtrepassword").focusout(function () {      
    if ($("#txtpassword").val() !== $("#txtrepassword").val()) {
        swal("Contraseñas no coinciden", "", "warning");
        $("#txtpassword").val("");
        $("#txtrepassword").val("");
        $("#txtpassword").focus();
        return 0;
    }
});

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();
    
    if ($("#txtpassword").val() !== $("#txtrepassword").val()) {
        swal("Contraseñas no coinciden", "", "warning");
        $("#txtpassword").val("");
        $("#txtrepassword").val("");
        $("#txtpassword").focus();
        return 0;
    }
    
    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) {

            var id_cliente = $("#txtdocruc").val();
            var nombre_razonsocial = $("#txtnomdocruc").val();
            var direccion = $("#txtdireccion").val();
            var telefono = $("#txttelefono").val();
            var celular = $("#txtcelular").val();
            var email = $("#txtemail").val();
            var clave = md5($("#txtpassword").val());
            var id_departamento = $("#cbodepartamento").val();
            var id_provincia = $("#cboprovincia").val();
            var id_distrito = $("#cbodistrito").val();

            var ruta = DIRECCION_WS + "cliente.agregar.php";

            $.post(ruta, {id_cliente: id_cliente, nombre_razonsocial: nombre_razonsocial, direccion: direccion, telefono: telefono, celular: celular, email: email, clave: clave, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito}, function () {
            }).done(function (resultado) {
                var datosJSON = resultado;
                if (datosJSON.estado === 200) {
                    swal("Exito", datosJSON.mensaje, "success");
                    $("#btncerrar").click(); //cerrar ventana
                    window.location="index.html";
                } else {
                    swal("Mensaje del sistema", resultado, "warning");
                }
            }).fail(function (error) {
                var datosJSON = $.parseJSON(error.responseText);
                swal("Error", datosJSON.mensaje, "error");
            })

        }
    });
});

