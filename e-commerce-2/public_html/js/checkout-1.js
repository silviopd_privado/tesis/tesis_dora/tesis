var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";
$(document).ready(function () {

    login()
    agregar()
    carritoDetalle()
    leerDatos()
//    alert($.cookie('listaProductos'))

});
function login() {
    var html = "";
    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    $("#sesion-login").html(html);
}

function carritoDetalle() {
    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var producto = JSON.parse($.cookie('listaProductos'))
    var html = "";
    var precioTotal = 0;

    html += '<tr class="CartProduct cartTableHeader">';
    html += '<td style="width:15%"> Product</td>';
    html += '<td style="width:40%">Details</td>';
    html += '<td style="width:10%">QNT</td>';
//    html += '<td style="width:10%">Discount</td>';
    html += '<td style="width:15%">Total</td>';
    html += '<td style="width:10%" class="delete">&nbsp;</td>';
    html += '</tr>';

    for (var i = 0; i < producto.length; i++) {
        var id_producto = producto[i]["id_producto"]
        var cantidad = producto[i]["cantidad"]
//        alert(id_producto + " - " + cantidad)

        $.post(ruta, {id_producto: id_producto}, function () {
        }).done(function (resultado) {

            var datosJSON = resultado;
            if (datosJSON.estado === 200) {

                $.each(datosJSON.datos, function (i, item) {
                    html += '<tr class="CartProduct">';
                    html += '<td class="CartProductThumb">';
                    html += '<div><a href="product-details.html"><img src="' + item.foto + '" alt="img"></a>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td>';
                    html += '<div class="CartDescription">';
                    html += '<h4><a href="product-details.html">' + item.nombre + '</a></h4>';
                    html += '<span class="size">' + item.marca + '</span>';
                    html += '<div class="price"><span>' + item.precio + '</span></div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td><input class="quanitySniper" type="text" value=' + cantidad + ' name="quanitySniper"></td>';
//                  html += '<td>0</td>';
                    html += '<td class="price">' + parseFloat(item.precio * cantidad).toFixed(2) + '</td>';
                    html += '<td class="delete" id="celiminar"><a title="Delete" href="javascript:void()"> <i class="glyphicon glyphicon-trash fa-2x"></i></a></td>';
                    html += '</tr>';
                });

                $("#tabla-carrito-detalle").html(html);
            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    };

   var precioTotal = 0;
    var html2 = "";
    $("#carrito-web tr").each(function () {
        var precio = $(this).find("td").eq(3).html();
        alert(precio)
        precioTotal += parseFloat(precio);
    });
    
    
    html2 += '<tr>';
    html2 += '<td>Total products</td>';
    html2 += '<td class="price">$'+parseFloat(precioTotal).toFixed(2)+'</td>';
    html2 += '</tr>';
    html2 += '<tr style="">';
    html2 += '<td>Shipping</td>';
    html2 += '<td class="price"><span class="success">Free shipping!</span></td>';
    html2 += '</tr>';
    html2 += '<tr class="cart-total-price ">';
    html2 += '<td>Total (tax excl.)</td>';
    html2 += '<td class="price">$216.51</td>';
    html2 += '</tr>';
    html2 += '<tr>';
    html2 += '<td>Total tax</td>';
    html2 += '<td class="price" id="total-tax">$0.00</td>';
    html2 += '</tr>';
    html2 += '<tr>';
    html2 += '<td> Total</td>';
    html2 += '<td class=" site-color" id="total-price">$216.51</td>';
    html2 += '</tr>';
    
    $("#producto-detalle-final").html(html2);
}

function agregar() {

    var ruta = DIRECCION_WS + "producto.leerdatos.cliente.php";
    var html = "";
    var producto = JSON.parse($.cookie('listaProductos'))

    for (var i = 0; i < producto.length; i++) {

        var id_producto = producto[i]["id_producto"]
        var cantidad = producto[i]["cantidad"]
//        alert(id_producto + " - " + cantidad)

        $.post(ruta, {id_producto: id_producto}, function () {
        }).done(function (resultado) {

            var datosJSON = resultado;
            if (datosJSON.estado === 200) {

                $.each(datosJSON.datos, function (i, item) {
                    //AGREGAR PRODUCTOS
                    html += '<tr class="miniCartProduct" id="productomodal">';
                    html += '<td style="width:20%" class="miniCartProductThumb">';
                    html += '<div><a href="product-details.html"> <img src="' + item.foto + '" alt="img">';
                    html += '</a></div>';
                    html += '</td>';
                    html += '<td style="width:40%">';
                    html += '<div class="miniCartDescription">';
                    html += '<h4><a href="product-details.html">' + item.nombre + '</a></h4>';
                    html += '<span class="size">' + item.marca + '</span>';
                    html += '<div class="price" id="precio" name="precio"><span> $' + item.precio + ' </span></div>';
                    html += '</div>';
                    html += '</td>';
                    html += '<td ><input id="cantidadmodal"  name="cantidadmodal" step="any" type="number" min="1" value= ' + cantidad + ' " style="width:70%"></td>';
                    html += '<td style="width:15%" class="miniCartSubtotal" id="frmpreciototal" name="frmpreciototal"> ' + parseFloat(item.precio * cantidad).toFixed(2) + '</td>';
                    html += '<td style="display:none;" id="id_producto" name="id_producto"> ' + item.id_producto + '</td>';
                    html += '<td style="display:none;" id="preciomodal" name="preciomodal"> ' + item.precio + '</td>';
                    html += '<td style="width:5%" class="delete" id="celiminar"><a href="javascript:void()"> <i class="fa fa-close text-danger"></i> </a></td>';
                    html += '</tr>';
                });

                $("#carrito-web").html(html);

                calcularTotales()

            } else {
                swal("Mensaje del sistema", resultado, "warning");
            }
        }).fail(function (error) {
            var datosJSON = $.parseJSON(error.responseText);
            swal("Error", datosJSON.mensaje, "error");
        })
    }
}

function calcularTotales() {

    var precioTotal = 0;
    var html2 = "";
    var html3 = "";
    $("#carrito-web tr").each(function () {
        var precio = $(this).find("td").eq(3).html();
        precioTotal += parseFloat(precio);
    });

//CALCULAR TOTALES
    html2 += '<h3 class="text-right subtotal"> $' + parseFloat(precioTotal).toFixed(2) + '</h3>';
    html2 += '<a class="btn btn-sm btn-danger" href="cart.html"> <i class="fa fa-shopping-cart"> </i> VIEW';
    html2 += 'CART </a>';
    html2 += '<a class = "btn btn-sm btn-primary" href="checkout-1.html"> CHECKOUT </a>';
    //CALCULAR TOTALES


    //CALCULAR TOTALES DROPDOWN
    html3 += '<i class="fa fa-shopping-cart"> </i> ';
    html3 += '<span class="cartRespons"> Cart ($' + parseFloat(precioTotal).toFixed(2) + ') </span>';
    html3 += '<b class="caret"> </b>';
    //CALCULAR TOTALES DROPDOWN


    $("#calcular-totales").html(html2);
    $("#calcular-totales-dropdown").html(html3);
}

$(document).on("change", "#cantidadmodal", function () {

    var producto = JSON.parse($.cookie('listaProductos'))

    $("#carrito-web tr").each(function (i) {
        var cantidad = document.getElementsByName("cantidadmodal")[i].value;
        var precio = document.getElementsByName("preciomodal")[i].innerHTML;
        var total = precio * cantidad;
        document.getElementsByName("frmpreciototal")[i].innerHTML = parseFloat(total).toFixed(2);
        calcularTotales()

        var id_producto = document.getElementsByName("id_producto")[i].innerHTML;
//        alert(producto[i]["id_producto"])

        if (producto[i]["id_producto"] == id_producto) {
            producto[i]["cantidad"] = cantidad;
        }
    });
$.removeCookie('listaProductos');
    $.cookie('listaProductos', JSON.stringify(producto))
});

$(document).on("click", "#celiminar", function () {
    var filaEliminar = $(this).parents().get(0);
//    alert(document.getElementsByName("id_producto").innerHTML)

    filaEliminar.remove(); //<-elimina        

    calcularTotales()
});

function leerDatos() {

    var ruta = DIRECCION_WS + "cliente.leerdatos.cliente.php";
    var token = $.cookie('token');
    var id_cliente = $.cookie('codigo_usuario');

    $.post(ruta, {token: token, id_cliente: id_cliente}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#InputName").val(item.id_cliente)
                $("#InputLastName").val(item.nombre_razonsocial)
                $("#InputEmail").val(item.email)

            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

