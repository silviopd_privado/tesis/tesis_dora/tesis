var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    login()

    listarDirecciones()

});

function login() {
    var html = "";

    if ($.cookie('nombreUsuario') == "" || $.cookie('nombreUsuario') == null) {
        html += '<li><a href="#" data-toggle="modal" data-target="#ModalLogin"> <span class="hidden-xs">Iniciar Sesión</span><i class="glyphicon glyphicon-log-in hide visible-xs "></i> </a></li>';
        html += '<li class="hidden-xs"><a href="account-1.html"> Crear Cuenta</a></li>';
    } else {
        html += '<li><a href="account.html"><span class="hidden-xs"> ' + $.cookie('nombreUsuario') + '(Mi Cuenta)</span> <i class="glyphicon glyphicon-user hide visible-xs "></i></a></li>';
        html += '<li class="hidden-xs"><a id="cerrar-sesion"> Cerrar Sesión</a></li>';
    }

    if ($.cookie('token') == "" || $.cookie('token') == null) {
        window.location = "index.html"
    }

    $("#sesion-login").html(html);
}

function listarDirecciones() {


    var ruta = DIRECCION_WS + "direccion.cliente.listar.cliente.php";
    var token = $.cookie('token');
    var id_cliente = $.cookie('codigo_usuario');

    $.post(ruta, {token: token, id_cliente: id_cliente}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<div class="col-xs-12 col-sm-6 col-md-4">';
                html += '<div class="panel panel-default">';
                html += '<div class="panel-heading">';
                html += '<h3 class="panel-title"><strong>Mi Direccion ' + parseFloat(parseFloat(i) + 1) + '</strong></h3>';
                html += '</div>';
                html += '<div class="panel-body">';
                html += '<ul>';
                html += '<li><span class="address-name"> <strong>' + item.direccion + '</strong></span></li>';
                html += '<li><span> <strong>Telefono</strong> : ' + item.telefono + ' </span></li>';
                html += '<li><span> <strong>Celular</strong> : ' + item.celular + ' </span></li>';
                html += '<li><span> <strong>Departamento</strong> : ' + item.departamento + ' </span></li>';
                html += '<li><span> <strong>Provincia</strong> : ' + item.provincia + ' </span></li>';
                html += '<li><span> <strong>Distrito</strong> : ' + item.distrito + ' </span></li>';
                html += '</ul>';
                html += '</div>';
                html += '<div class="panel-footer panel-footer-address">';
                html += '<a href="add-address.html?dir=' + item.id_direccion + '" class="btn btn-sm btn-success"><i class="fa fa-edit"> </i> Edit </a> ';
                html += '<a class="btn btn-sm btn-danger" onclick="eliminar(' + item.id_direccion + ')"><i class="fa fa-minus-circle"></i> Delete </a>';
                html += '</div>';
                html += '</div>';
                html += '</div>';
            });

            $("#mis-direcciones").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(id_direccion) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar la dirección seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "direccion.cliente.eliminar.php";
                    var token = $.cookie('token');
                    var id_cliente = $.cookie('codigo_usuario');
                    ;

                    $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listarDirecciones();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}