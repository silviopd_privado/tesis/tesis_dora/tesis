$("#frminiciosesion").submit(function (evento) {
    evento.preventDefault();

    var ruta = "http://localhost:8080/tesis/e-commerce-ws/webservice/sesion.validar.cliente.php";
    var txtDni = $("#login-user").val();
    var txtPassword = md5($("#login-password").val());

    $.post(ruta, {dni: txtDni, clave: txtPassword}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.cookie('nombreUsuario', datosJSON.datos.usuario)
            $.cookie('codigo_usuario', datosJSON.datos.codigo_usuario)

            $.cookie('token', datosJSON.datos.token)

//            window.location = "index.html";
            location.reload();
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        $("#txtEmail").val("");
        $("#txtPassword").val("");
        $("#txtEmail").focus();

        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");

    })
});

$(document).on("click", "#cerrar-sesion", function () {
    swal({
        title: "Confirme",
        text: "¿Esta seguro que desea cerrar sesión?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) {

            $.removeCookie('nombreUsuario');
            $.removeCookie('codigo_usuario');
            $.removeCookie('token');
            $.removeCookie('listaProductos');

//            window.location = "index.html";
            location.reload();
        }
    });
});


