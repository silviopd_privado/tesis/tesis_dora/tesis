<?php
require_once '../util/funciones/definiciones.php';
?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!-- This file has been downloaded from Bootsnipp.com. Enjoy! -->
        <title><?php echo C_NOMBRE_SOFTWARE; ?></title>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet">
        <link href="css/inicio-sesion.css" rel="stylesheet" type="text/css" />
        <link href="css/loader.css" rel="stylesheet" type="text/css" />
        <script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>

        <!--sweetalert-->
        <link rel="stylesheet" href="../util/swa/sweetalert.css">
        <link href="../util/bootstrap/css/dropdown.css" rel="stylesheet" type="text/css" />

    </head>
    <!--
        you can substitue the span of reauth email for a input with the email and
        include the remember me checkbox
    -->
    <div class="container">
        <div class="card card-container">
            <!-- <img class="profile-img-card" src="//lh3.googleusercontent.com/-6V8xOA6M7BA/AAAAAAAAAAI/AAAAAAAAAAA/rzlHcD0KYwo/photo.jpg?sz=120" alt="" /> -->
            <img id="profile-img" class="profile-img-card" src="../imagenes/iniciosesion.png" />
            <p id="profile-name" class="profile-name-card"></p>
            <form class="form-signin" id="frminiciosesion">
                <span id="reauth-email" class="reauth-email"></span>
                <input type="text" id="txtEmail" class="form-control" placeholder="Dni Usuario" required autofocus>
                <input type="password" id="txtPassword" class="form-control" placeholder="Clave" required>
                <div id="remember" class="checkbox">
                    <label>
                        <input type="checkbox" value="remember-me"> Remember me
                    </label>
                </div>
                <button class="btn btn-lg btn-primary btn-block btn-signin" type="submit">Sign in</button>
            </form><!-- /form -->
            <a href="#" class="forgot-password">
                Forgot the password?
            </a>
        </div><!-- /card-container -->
    </div><!-- /container -->

    <!--sweetalert-->
    <script src="../util/swa/sweetalert-dev.js"></script>
    <!-- jQuery 2.1.3 -->
    <script src="../util/jquery/jquery-3.1.1.js"></script>
    <script src="js/inicio-sesion.js"></script>
</body>
</html>