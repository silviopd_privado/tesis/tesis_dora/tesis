<?php
require_once '../util/funciones/definiciones.php';
?>

<header class="main-header">

    <nav class="navbar navbar-static-top">
        <div class="container-fluid">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
                    <i class="fa fa-bars"></i>
                </button>
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="">
                        <a href="producto.vista.php"> <i class="glyphicon glyphicon-home"></i> <?php echo C_NOMBRE_SOFTWARE; ?></a>
                    </li>

                    <li class="dropdown" id="mantenimiento">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-edit"></i>&nbsp;Mantenimientos <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu" >
                            <li><a href="marca.vista.php">Marcas</a></li>
                            <li><a href="categoria.vista.php">Categorias</a></li>
                            <li><a href="producto.vista.php">Productos</a></li>
                            <li class="divider"></li>
                            <li><a href="personal.vista.php">Personal</a></li>
                            <li class="divider"></li>
                            <li><a href="cliente.vista.php">Cliente</a></li>
                            <li><a href="direcciones-cliente.vista.php">Direcciones del Cliente</a></li>
                            <li class="divider"></li>
                            <li><a href="cargo.vista.php">Cargo</a></li>
                            <li><a href="area.vista.php">Area</a></li>
                            <li class="divider"></li>
                            <li><a href="tipocomprobante.vista.php">Tipo Comprobante</a></li>
                            <li><a href="seriecomprobante.vista.php">Serie Comprobante</a></li>
                            <li class="divider"></li>
                            <li><a href="formaspago.vista.php">Formas de Pago</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" id="transaccion">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-laptop"></i>&nbsp;Transacciones <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="venta.listado.vista.php">Registro de Venta</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" id="administracion">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="glyphicon glyphicon-save"></i>&nbsp;Extracción Data <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="extraccion.data.mayorista.vista.php">Mayoristas</a></li>
                        </ul>
                    </li>
                    <li class="dropdown" id="administracion">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"> <i class="fa fa-gears"></i>&nbsp;Administración <span class="caret"></span></a>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="#">Grupos de usuarios</a></li>
                            <li><a href="#">Usuarios</a></li>
                            <li class="divider"></li>
                            <li><a href="#">Permisos</a></li>
                        </ul>
                    </li>                    
                </ul>
                <ul class="nav navbar-nav navbar-right">      
                    
                    <li class="dropdown user user-menu" id="frmcabecera">
                        
                    </li>          
                </ul>
            </div><!-- /.navbar-collapse -->

        </div><!-- /.container-fluid -->
    </nav>
    
</header>


