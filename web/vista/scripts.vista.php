<!-- jQuery 2.1.3 -->
<script src="../util/jquery/jquery-3.1.1.js"></script>

<!-- Bootstrap 3.3.2 JS -->
<script src="../util/bootstrap/js/bootstrap.js" type="text/javascript"></script>

<!-- DATA TABLE -->
<script src="../util/lte/plugins/datatables/jquery.dataTables.js" type="text/javascript"></script>
<script src="../util/lte/plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>

<!-- Scroll -->
<script src="../util/scroll/jquery.nicescroll.min.js"></script>

<!-- AdminLTE App -->
<script src="../util/lte/js/app.js" type="text/javascript"></script>

<!--UTIL-->
<script src="js/util.js" type="text/javascript"></script>

<!--sweetalert-->
<script src="../util/swa/sweetalert-dev.js"></script>

<!-- Select2 -->
<script src="../util/lte/plugins/select2/select2.js"></script>
<script src="../util/lte/plugins/select2/select2.full.js"></script>

<!-- Latest compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.2/js/bootstrap-select.min.js"></script>

<script>
    $("html").niceScroll();  // The document page (body)
</script>

<!--Cabecera-->
<script src="js/cabecera.js"></script>
<script src="js/inicio-sesion.js"></script>
