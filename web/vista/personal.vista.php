<?php
require_once '../util/funciones/definiciones.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo C_NOMBRE_SOFTWARE; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <?php
        include 'estilos.vista.php';
        ?>

    </head>
    <body class="skin-red layout-top-nav">
        <!-- Site wrapper -->
        <div class="wrapper">

            <?php
            include 'cabecera.vista.php';
            ?>

            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 class="text-bold text-black" style="font-size: 20px;">Mantenimiento De Personal</h1>
                </section>

                <section class="content">

                    <!-- INICIO del formulario modal -->
                    <small>
                        <form id="frmgrabar">
                            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="titulomodal">Título de la ventana</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="txttipooperacion" id="txttipooperacion" class="form-control">
                                            <div class="row">
                                                <div class="col-xs-4">
                                                    <p>Dni Personal<input type="text" name="txtcodigo" id="txtcodigo" class="form-control input-sm text-center text-bold" placeholder="" ></p>
                                                </div>
                                            </div>
                                            <p>Apellido Paterno <font color = "red">*</font>
                                                <input type="text" name="txtapellidopaterno" id="txtapellidopaterno" class="form-control input-sm" placeholder="" required="">
                                            </p>
                                            <p>Apellido Materno<font color = "red">*</font>
                                                <input type="text" name="txtapellidomaterno" id="txtapellidomaterno" class="form-control input-sm" placeholder="" required="" >
                                            </p>
                                            <p>Nombres<font color = "red">*</font>
                                                <input type="text" name="txtnombres" id="txtnombres" class="form-control input-sm" placeholder="" required="" >
                                            </p>
                                            <p>Direccion<font color = "red">*</font>
                                                <input type="text" name="txtdireccion" id="txtdireccion" class="form-control input-sm" placeholder="" required="" >
                                            </p>                                            
                                            <p>Telefono<font color = "red">*</font>
                                                <input type="text" name="txttelefono" id="txttelefono" class="form-control input-sm" placeholder="" required="" >
                                            </p>
                                            <p>Email<font color = "red">*</font>
                                                <input type="text" name="txtemail" id="txtemail" class="form-control input-sm" placeholder="" required="">
                                            </p>
                                            <p>clave<font color = "red">*</font>
                                                <input type="text" name="txtclave" id="txtclave" class="form-control input-sm" placeholder="" required="" >
                                            </p>
                                            <p>
                                                Departamento <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbodepartamentomodal" id="cbodepartamentomodal" required="" >
                                                </select>
                                            </p>  
                                            <p>
                                                Provincia <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cboprovinciamodal" id="cboprovinciamodal" required="" >
                                                </select>
                                            </p>    
                                            <p>
                                                Distrito <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbodistritomodal" id="cbodistritomodal" required="" >
                                                </select>
                                            </p>                                              
                                            <p>
                                                Area <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cboareamodal" id="cboareamodal" required="" >
                                                </select>
                                            </p>
                                            <p>
                                                Cargo <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbocargomodal" id="cbocargomodal" required="" >
                                                </select>
                                            </p>
                                            <p>
                                                Estado <font color = "red">*</font>
                                                <select class="form-control input-sm" id="cboestado" name="cboestado" required="" >
                                                    <option value="" disabled selected>Seleccione un estado</option>
                                                    <option value="A" selected="">Activo</option>
                                                    <option value="I" >Inactivo</option>
                                                </select>
                                            </p>
                                            <p>
                                                <font color = "red">* Campos obligatorios</font>
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success" aria-hidden="true"><i class="fa fa-save"></i> Grabar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btncerrar"><i class="fa fa-close"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </small>
                    <!-- FIN del formulario modal -->

                    <div class="row">
                        <div class="col-xs-3">
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal" id="btnagregar"><i class="fa fa-copy"></i> Agregar Nuevo Personal</button>
                        </div>
                    </div>
                    <p>
                    <div class="box box-success">
                        <div class="box-body">
                            <div id="listado">
                            </div>
                        </div>
                    </div>
                    </p>
                </section>
            </div>
        </div><!-- ./wrapper -->
        <?php
        include 'scripts.vista.php';
        ?>
        <!--JS-->
        <script src="js/util.js" type="text/javascript"></script>
        <script src="js/cargar-combos.js" type="text/javascript"></script>
        <script src="js/personal.js" type="text/javascript"></script>

    </body>
</html>



