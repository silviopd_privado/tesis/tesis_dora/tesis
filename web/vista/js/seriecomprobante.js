//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    cargarComboTipoComprobante("#cbomarcamodal", "seleccione");
    listar();
});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtdescripcion").focus();
});

$("#btnagregar").click(function () {

    $("#cbomarcamodal").removeAttr('disabled');

    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#txtdescripcion2").val("");
    $("#cbomarcamodal").val("");
    $("#titulomodal").text("Agregar Nueva Serie Comprobante.");

    document.getElementById("txtcodigo").disabled = false;
});

function listar() {

    var ruta = DIRECCION_WS + "seriecomprobante.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">TIPO COMPROBANTE</th>';
            html += '<th style="text-align: center">NUMERO SERIE</th>';
            html += '<th style="text-align: center">NUMERO DOCUMENTO</th>';
            html += '<th style="text-align: center">ULTIMO NUMERO DOCUMENTO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.tipo_comprobante + '</td>';
                html += '<td align="center">' + item.numero_serie + '</td>';
                html += '<td align="center">' + item.numero_documento + '</td>';
                html += '<td align="center">' + item.ultimo_numero + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.id_tipo_comprobante + ',' + item.numero_serie + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.id_tipo_comprobante + ',' + item.numero_serie + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

function eliminar(id_tipo_comprobante, numero_serie) {
    
    var nuevo;

    if (id_tipo_comprobante < 9) {
        nuevo = "0" + id_tipo_comprobante.toString()
    } else {
        nuevo = id_tipo_comprobante;
    }
    
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "seriecomprobante.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, id_tipo_comprobante: nuevo, numero_serie: numero_serie}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}


$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "seriecomprobante.agregar.php";
                var token = $.cookie('token');
                var numero_serie = $("#txtcodigo").val()
                var id_tipo_comprobante = $("#cbomarcamodal").val()
                var numero_documento = $("#txtdescripcion").val()
                var ultimo_numero = $("#txtdescripcion2").val()

                $.post(ruta, {token: token, numero_serie: numero_serie, numero_documento: numero_documento, id_tipo_comprobante: id_tipo_comprobante,ultimo_numero:ultimo_numero}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "seriecomprobante.editar.php";
                var token = $.cookie('token');
                var numero_serie = $("#txtcodigo").val()
                var id_tipo_comprobante = $("#cbomarcamodal").val()
                var numero_documento = $("#txtdescripcion").val()
                var ultimo_numero = $("#txtdescripcion2").val()

                $.post(ruta, {token: token, numero_serie: numero_serie, numero_documento: numero_documento, id_tipo_comprobante: id_tipo_comprobante,ultimo_numero:ultimo_numero}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(id_tipo_comprobante, numero_serie) {

    var nuevo;

    if (id_tipo_comprobante < 9) {
        nuevo = "0" + id_tipo_comprobante.toString()
    } else {
        nuevo = id_tipo_comprobante;
    }

    document.getElementById("txtcodigo").disabled = true;
    $("#cbomarcamodal").attr('disabled', 'disabled');

    var ruta = DIRECCION_WS + "seriecomprobante.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_tipo_comprobante: nuevo, numero_serie: numero_serie}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#txttipooperacion").val("editar");
                $("#txtcodigo").val(item.numero_serie);
                $("#txtdescripcion").val(item.numero_documento);
                $("#txtdescripcion2").val(item.ultimo_numero);
                $("#cbomarcamodal").val(item.id_tipo_comprobante);
                $("#titulomodal").text("Editar Serie Comprobante.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}





