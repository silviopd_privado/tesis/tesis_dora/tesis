//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    listar();
});

$("#myModal").on("shown.bs.modal", function () {
    $("#txtdescripcion").focus();
});

$("#btnagregar").click(function () {

    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#titulomodal").text("Agregar Nuevo Personal.");

    document.getElementById("cboestado").disabled = true;
    document.getElementById("txtcodigo").disabled = false;
});

function listar() {

    var ruta = DIRECCION_WS + "cliente.listar.personal.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">DNI/RUC</th>';
            html += '<th style="text-align: center">NOMBRE/RAZON SOCIAL</th>';
            html += '<th style="text-align: center">EMAIL</th>';
            html += '<th style="text-align: center">ESTADO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.id_cliente + '</td>';
                html += '<td align="center">' + item.nombre_razonsocial + '</td>';
                html += '<td align="center">' + item.email + '</td>';
                html += '<td align="center">' + item.estado + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.id_cliente + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.id_cliente + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "cliente.agregar.personal.php";
                var token = $.cookie('token');

                var id_cliente = $("#txtcodigo").val()
                var nombre_razonsocial = $("#txtnombres").val()
                var email = $("#txtemail").val()
                var clave = $.md5($("#txtclave").val())

                $.post(ruta, {token: token, id_cliente: id_cliente, nombre_razonsocial: nombre_razonsocial, email: email, clave: clave}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "cliente.editar.php";
                var token = $.cookie('token');

                var id_cliente = $("#txtcodigo").val()
                var nombre_razonsocial = $("#txtnombres").val()
                var email = $("#txtemail").val()
                var clave = $.md5($("#txtclave").val())
                var estado = $("#cboestado").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, nombre_razonsocial: nombre_razonsocial, email: email, clave: clave, estado: estado}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(id_cliente) {

    document.getElementById("cboestado").disabled = false;
    document.getElementById("txtcodigo").disabled = true;

    var ruta = DIRECCION_WS + "cliente.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_cliente: id_cliente}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#txtcodigo").val(item.id_cliente)
                $("#txtnombres").val(item.nombre_razonsocial)
                $("#txtemail").val(item.email)
                $("#txtclave").val(item.clave)
                $("#cboestado").val(item.estado)

                $("#titulomodal").text("Editar Personal.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(id_cliente) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "cliente.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, id_cliente: id_cliente}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}



