//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    cargarComboMarca("#cbomarcamodal", "seleccione");
    cargarComboBonificacion("#cbobonificacionmodal", "seleccione");
    listar();

    $("#imagen").change(function () {
        readURL(this);
    });

});

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var template =
                    '<img src="' + e.target.result + '" height="450px" width="550px"> '
            $('#images-to-upload').html(template);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$("#myModal").on("shown.bs.modal", function () {
    $("#txtnombre").focus();
});

$("#cbomarcamodal").change(function () {
    var id_marca = $("#cbomarcamodal").val();
    cargarComboCategoria("#cbocategoriamodal", "seleccione", id_marca);
});

$("#btnagregar").click(function () {

    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtnombre").val("");
    $("#txtdescripcion").val("");
    $("#txtprecio").val("");
    $("#txtcantidad").val("");
    $("#txtcantidadxcaja").val("");
    $("#txtcantidadbonificacion").val("");
    $("#txtdescuento").val("");
    $("#txtpreciooferta").val("");
    $("#txtprecioxbotella").val("");
    $("#txtstock").val("");
    $("#cbobonificacionmodal").val("");
    $("#cbomarcamodal").val("");
    $("#cbocategoriamodal").empty();

    $('#images-to-upload').empty();
    $('#imagen').val("");

    $("#titulomodal").text("Agregar Nueva Producto.");

    document.getElementById("cboestado").disabled = true;
});

function listar() {

    var ruta = DIRECCION_WS + "producto.listar.personal.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">NOMBRE</th>';
            html += '<th style="text-align: center">DESCRIPCION</th>';
            html += '<th style="text-align: center">PRECIO</th>';
            html += '<th style="text-align: center">CANTIDAD</th>';
            html += '<th style="text-align: center">UNIDAD/CAJA</th>';
            html += '<th style="text-align: center">CANTIDAD BONIFICACION</th>';
            html += '<th style="text-align: center">BONIFICACION</th>';
            html += '<th style="text-align: center">DESCUENTO</th>';
            html += '<th style="text-align: center">PRECIO OFERTA</th>';
            html += '<th style="text-align: center">PRECIO/BOTELLA</th>';
            html += '<th style="text-align: center">STOCK</th>';
            html += '<th style="text-align: center">MARCA</th>';
            html += '<th style="text-align: center">CATEGORIA</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.nombre + '</td>';
                html += '<td align="center">' + item.descripcion + '</td>';
                html += '<td align="center">' + item.precio + '</td>';
                html += '<td align="center">' + item.cantidad + '</td>';
                html += '<td align="center">' + item.unidad_x_caja + '</td>';
                html += '<td align="center">' + item.cantidad_bonificacion + '</td>';
                html += '<td align="center">' + item.bonificacion + '</td>';
                html += '<td align="center">' + item.descuento + '</td>';
                html += '<td align="center">' + item.precio_oferta + '</td>';
                html += '<td align="center">' + item.precio_x_botella + '</td>';
                html += '<td align="center">' + item.stock + '</td>';
                html += '<td align="center">' + item.marca + '</td>';
                html += '<td align="center">' + item.categoria + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.id_producto + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.id_producto + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

function eliminar(id_producto) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "producto.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, id_producto: id_producto}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}


$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "producto.agregar.php";
                var token = $.cookie('token');

                var nombre = $("#txtnombre").val()
                var descripcion = $("#txtdescripcion").val()
                var precio = $("#txtprecio").val()
                var cantidad = $("#txtcantidad").val()
                var unidad_x_caja = $("#txtunidadxcaja").val()
                var bonificacion = $("#cbobonificacionmodal").val()
                var descuento = $("#txtdescuento").val()
                var precio_oferta = $("#txtpreciooferta").val()
                var precio_x_botella = $("#txtprecioxbotella").val()
                var stock = $("#txtstock").val()
                var id_marca = $("#cbomarcamodal").val()
                var id_categoria = $("#cbocategoriamodal").val()
                var cantidad_bonificacion = $("#txtcantidadbonificacion").val()

                var imagen = document.getElementById('imagen').files[0];
                var nombreimagen = imagen['name'];

                $.post(ruta, {token: token, nombre: nombre, descripcion: descripcion, precio: precio, cantidad: cantidad, unidad_x_caja: unidad_x_caja, bonificacion: bonificacion, cantidad_bonificacion: cantidad_bonificacion, descuento: descuento, precio_oferta: precio_oferta, precio_x_botella: precio_x_botella, stock: stock, id_marca: id_marca, id_categoria: id_categoria, nombreimagen: nombreimagen}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {

                        //COPIAR IMAGEN                        
                        var formData = new FormData();
                        formData.append('my_uploaded_file', imagen);
                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", DIRECCION_WS + "imagen.producto.php");
                        xhr.send(formData);
                        //COPIAR IMAGEN

                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "producto.editar.php";
                var token = $.cookie('token');
                var id_producto = $("#txtcodigo").val()
                var nombre = $("#txtnombre").val()
                var descripcion = $("#txtdescripcion").val()
                var precio = $("#txtprecio").val()
                var cantidad = $("#txtcantidad").val()
                var unidad_x_caja = $("#txtunidadxcaja").val()
                var bonificacion = $("#cbobonificacionmodal").val()
                var descuento = $("#txtdescuento").val()
                var precio_oferta = $("#txtpreciooferta").val()
                var precio_x_botella = $("#txtprecioxbotella").val()
                var stock = $("#txtstock").val()
                var id_marca = $("#cbomarcamodal").val()
                var id_categoria = $("#cbocategoriamodal").val()
                var cantidad_bonificacion = $("#txtcantidadbonificacion").val()
                var estado = $("#cboestado").val()
                
                var imagen = document.getElementById('imagen').files[0];
                var nombreimagen = imagen['name'];

                $.post(ruta, {token: token, id_producto: id_producto, nombre: nombre, descripcion: descripcion, precio: precio, cantidad: cantidad, unidad_x_caja: unidad_x_caja, bonificacion: bonificacion, cantidad_bonificacion: cantidad_bonificacion, descuento: descuento, precio_oferta: precio_oferta, precio_x_botella: precio_x_botella, stock: stock, id_marca: id_marca, id_categoria: id_categoria, estado: estado, nombreimagen: nombreimagen}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana

                        //COPIAR IMAGEN                        
                        var formData = new FormData();
                        formData.append('my_uploaded_file', imagen);
                        var xhr = new XMLHttpRequest();
                        xhr.open("POST", DIRECCION_WS + "imagen.producto.php");
                        xhr.send(formData);
                        //COPIAR IMAGEN

                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(id_producto) {

    document.getElementById("cboestado").disabled = false;
    $('#images-to-upload').empty();
    $('#imagen').val("");

    var ruta = DIRECCION_WS + "producto.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_producto: id_producto}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#txtcodigo").val(item.id_producto)
                $("#txtnombre").val(item.nombre)
                $("#txtdescripcion").val(item.descripcion)
                $("#txtprecio").val(item.precio)
                $("#txtcantidad").val(item.cantidad)
                $("#txtunidadxcaja").val(item.unidad_x_caja)
                $("#cbobonificacionmodal").val(item.bonificacion)
                $("#txtdescuento").val(item.descuento)
                $("#txtpreciooferta").val(item.precio_oferta)
                $("#txtprecioxbotella").val(item.precio_x_botella)
                $("#txtstock").val(item.stock)

                $("#cbomarcamodal").val(item.id_marca)
                $("#cbomarcamodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cbocategoriamodal").val(item.id_categoria)
                });

                $("#cboestado").val(item.estado_producto)
                $("#txtcantidadbonificacion").val(item.cantidad_bonificacion)
                
                var template = '<img src="' + item.foto + '" height="450px" width="550px"> '
                $('#images-to-upload').html(template);

                $("#titulomodal").text("Editar Producto.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}
