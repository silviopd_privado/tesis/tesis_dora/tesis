//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

function cargarComboMarca(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "marca.listar.personal.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una marca</option>';
            } else {
                html += '<option value="0">Todas las marcas</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_marca + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboBonificacion(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "producto.listar.personal.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una bonificacion</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_producto + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboCategoria(p_nombreCombo, p_tipo, p_id_marca) {
    var ruta = DIRECCION_WS + "categoria.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_marca: p_id_marca}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un tipo de categorias</option>';
            } else {
                html += '<option value="0">Todos los tipos de categorias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_categoria + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboTipoComprobante(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "tipocomprobante.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un tipo comprobante</option>';
            } 

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_tipo_comprobante + '">' + item.descripcion + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboDepartamento(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "departamento.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una departamento</option>';
            } else {
                html += '<option value="0">Todas los departamentos</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_departamento + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboProvincia(p_nombreCombo, p_tipo, id_departamento) {
    var ruta = DIRECCION_WS + "provincia.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_departamento: id_departamento}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una provincia</option>';
            } else {
                html += '<option value="0">Todas las provincias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_provincia + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboDistrito(p_nombreCombo, p_tipo, id_departamento, id_provincia) {
    var ruta = DIRECCION_WS + "distrito.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_departamento: id_departamento, id_provincia: id_provincia}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una provincia</option>';
            } else {
                html += '<option value="0">Todas las provincias</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_distrito + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboCargo(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "cargo.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un cargo</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_cargo + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboArea(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "area.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una area</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_area + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

