//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    cargarComboCargo("#cbocargomodal", "seleccione");
    cargarComboArea("#cboareamodal", "seleccione");

    cargarComboDepartamento("#cbodepartamentomodal", "seleccione");

    listar();
});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtdescripcion").focus();
});

$("#cbodepartamentomodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", id_departamento);
});

$("#cboprovinciamodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    var id_provincia = $("#cboprovinciamodal").val();
    cargarComboDistrito("#cbodistritomodal", "seleccione", id_departamento, id_provincia);
});

$("#btnagregar").click(function () {

    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#cbomarcamodal").val("");
    $("#titulomodal").text("Agregar Nuevo Personal.");

    document.getElementById("cboestado").disabled = true;
    document.getElementById("txtcodigo").disabled = false;
});

function listar() {

    var ruta = DIRECCION_WS + "personal.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">DNI</th>';
            html += '<th style="text-align: center">NOMBRE</th>';
            html += '<th style="text-align: center">DIRECCION</th>';
            html += '<th style="text-align: center">TELEFONO</th>';
            html += '<th style="text-align: center">EMAIL</th>';
            html += '<th style="text-align: center">DEPARTAMENTO</th>';
            html += '<th style="text-align: center">PROVINCIA</th>';
            html += '<th style="text-align: center">DISTRITO</th>';
            html += '<th style="text-align: center">AREA</th>';
            html += '<th style="text-align: center">CARGO</th>';
            html += '<th style="text-align: center">ESTADO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.dni_personal + '</td>';
                html += '<td align="center">' + item.nombre + '</td>';
                html += '<td align="center">' + item.direccion + '</td>';
                html += '<td align="center">' + item.telefono + '</td>';
                html += '<td align="center">' + item.email + '</td>';
                html += '<td align="center">' + item.departamento + '</td>';
                html += '<td align="center">' + item.provincia + '</td>';
                html += '<td align="center">' + item.distrito + '</td>';
                html += '<td align="center">' + item.area + '</td>';
                html += '<td align="center">' + item.cargo + '</td>';
                html += '<td align="center">' + item.estado + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.dni_personal + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.dni_personal + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "personal.agregar.php";
                var token = $.cookie('token');

                var dni_personal = $("#txtcodigo").val()
                var apellido_paterno = $("#txtapellidopaterno").val()
                var apellido_materno = $("#txtapellidomaterno").val()
                var nombres = $("#txtnombres").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var email = $("#txtemail").val()
                var clave = $.md5($("#txtclave").val())
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()
                var id_area = $("#cboareamodal").val()
                var id_cargo = $("#cbocargomodal").val()

                $.post(ruta, {token: token, dni_personal: dni_personal, apellido_paterno: apellido_paterno, apellido_materno: apellido_materno, nombres: nombres, direccion: direccion, telefono: telefono, email: email, clave: clave, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito, id_area: id_area, id_cargo: id_cargo}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "personal.editar.php";
                var token = $.cookie('token');

                var dni_personal = $("#txtcodigo").val()
                var apellido_paterno = $("#txtapellidopaterno").val()
                var apellido_materno = $("#txtapellidomaterno").val()
                var nombres = $("#txtnombres").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var email = $("#txtemail").val()
                var clave = $.md5($("#txtclave").val())
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()
                var id_area = $("#cboareamodal").val()
                var id_cargo = $("#cbocargomodal").val()
                var estado = $("#cboestado").val()

                $.post(ruta, {token: token, dni_personal: dni_personal, apellido_paterno: apellido_paterno, apellido_materno: apellido_materno, nombres: nombres, direccion: direccion, telefono: telefono, email: email, clave: clave, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito, id_area: id_area, id_cargo: id_cargo, estado: estado}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(dni_personal) {

    document.getElementById("cboestado").disabled = false;
    document.getElementById("txtcodigo").disabled = true;

    var ruta = DIRECCION_WS + "personal.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, dni_personal: dni_personal}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#txtcodigo").val(item.dni_personal)
                $("#txtapellidopaterno").val(item.apellido_paterno)
                $("#txtapellidomaterno").val(item.apellido_materno)
                $("#txtnombres").val(item.nombres)
                $("#txtdireccion").val(item.direccion)
                $("#txttelefono").val(item.telefono)
                $("#txtemail").val(item.email)
                $("#txtclave").val(item.clave)


                $("#cbodepartamentomodal").val(item.id_departamento)
                $("#cbodepartamentomodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cboprovinciamodal").val(item.id_provincia)
                });

                $("#cboprovinciamodal").val(item.id_provincia)
                $("#cboprovinciamodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cbodistritomodal").val(item.id_distrito)
                });

                $("#cboareamodal").val(item.id_area)
                $("#cbocargomodal").val(item.id_cargo)
                $("#cboestado").val(item.estado)

                $("#titulomodal").text("Editar Personal.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(dni_personal) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "personal.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, dni_personal: dni_personal}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}



