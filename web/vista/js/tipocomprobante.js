//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {
    listar();
});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtdescripcion").focus();
});

$("#btnagregar").click(function () {
    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdescripcion").val("");
    $("#titulomodal").text("Agregar Nueva Tipo Comprobante.");

    document.getElementById("txtcodigo").disabled = false;
});

function listar() {

    var ruta = DIRECCION_WS + "tipocomprobante.listar.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">DESCRIPCION</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td style="text-align: center">' + item.descripcion + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.id_tipo_comprobante + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.id_tipo_comprobante + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[2, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

function eliminar(id_tipo_comprobante) {

    var nuevo;

    if (id_tipo_comprobante < 9) {
        nuevo = "0" + id_tipo_comprobante.toString()
    }else{
        nuevo = id_tipo_comprobante;
    }
    
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "tipocomprobante.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, id_tipo_comprobante: nuevo}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}


$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "tipocomprobante.agregar.php";
                var token = $.cookie('token');
                var id_tipo_comprobante = $("#txtcodigo").val()
                var descripcion = $("#txtdescripcion").val()

                $.post(ruta, {token: token, id_tipo_comprobante: id_tipo_comprobante, descripcion: descripcion}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "tipocomprobante.editar.php";
                var token = $.cookie('token');
                var id_tipo_comprobante = $("#txtcodigo").val()
                var descripcion = $("#txtdescripcion").val()

                $.post(ruta, {token: token, id_tipo_comprobante: id_tipo_comprobante, descripcion: descripcion}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(id_tipo_comprobante) {
    
    var nuevo;

    if (id_tipo_comprobante < 9) {
        nuevo = "0" + id_tipo_comprobante.toString()
    }else{
        nuevo = id_tipo_comprobante;
    }

    document.getElementById("txtcodigo").disabled = true;

    var ruta = DIRECCION_WS + "tipocomprobante.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_tipo_comprobante: nuevo}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {
                $("#txttipooperacion").val("editar");
                $("#txtcodigo").val(item.id_tipo_comprobante);
                $("#txtdescripcion").val(item.descripcion);
                $("#titulomodal").text("Editar Tipo Comprobante.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}





