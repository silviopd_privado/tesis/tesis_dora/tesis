//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {

    cargarComboDepartamento("#cbodepartamentomodal", "seleccione");

    listar();
});


$("#myModal").on("shown.bs.modal", function () {
    $("#txtcodigo").focus();
});

$("#cbodepartamentomodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    cargarComboProvincia("#cboprovinciamodal", "seleccione", id_departamento);
});

$("#cboprovinciamodal").change(function () {
    var id_departamento = $("#cbodepartamentomodal").val();
    var id_provincia = $("#cboprovinciamodal").val();
    cargarComboDistrito("#cbodistritomodal", "seleccione", id_departamento, id_provincia);
});

$("#btnagregar").click(function () {

    $("#txttipooperacion").val("agregar");
    $("#txtcodigo").val("");
    $("#txtdireccion").val("");
    $("#txttelefono").val("");
    $("#txtcelular").val("");
    $("#titulomodal").text("Agregar Nueva Direccion.");

    document.getElementById("txtcodigo").disabled = false;
});

function listar() {

    var ruta = DIRECCION_WS + "direccion.cliente.listar.personal.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            html += '<small>';
            html += '<table id="tabla-listado2" class="table table-bordered table-striped">';
            html += '<thead>';
            html += '<tr style="background-color: #ededed; height:25px;">';
            html += '<th style="text-align: center">DNI/RUC</th>';
            html += '<th style="text-align: center">NOMBRE/RAZON SOCIAL</th>';
            html += '<th style="text-align: center">N° DIRECCION</th>';
            html += '<th style="text-align: center">DIRECCION</th>';
            html += '<th style="text-align: center">TELEFONO</th>';
            html += '<th style="text-align: center">CELULAR</th>';
            html += '<th style="text-align: center">DEPARTAMENTO</th>';
            html += '<th style="text-align: center">PROVINCIA</th>';
            html += '<th style="text-align: center">DISTRITO</th>';
            html += '<th style="text-align: center">OPCIONES</th>';
            html += '</tr>';
            html += '</thead>';
            html += '<tbody>';

            //Detalle
            $.each(datosJSON.datos, function (i, item) {
                html += '<tr>';
                html += '<td align="center">' + item.id_cliente + '</td>';
                html += '<td align="center">' + item.nombre_razonsocial + '</td>';
                html += '<td align="center">' + item.id_direccion + '</td>';
                html += '<td align="center">' + item.direccion + '</td>';
                html += '<td align="center">' + item.telefono + '</td>';
                html += '<td align="center">' + item.celular + '</td>';
                html += '<td align="center">' + item.departamento + '</td>';
                html += '<td align="center">' + item.provincia + '</td>';
                html += '<td align="center">' + item.distrito + '</td>';
                html += '<td align="center">';
                html += '<button type="button" class="btn btn-warning btn-xs" data-toggle="modal" data-target="#myModal" onclick="leerDatos(' + item.id_cliente + ',' + item.id_direccion + ')"><i class="fa fa-pencil"></i></button>';
                html += '&nbsp;&nbsp;';
                html += '<button type="button" class="btn btn-danger btn-xs" onclick="eliminar(' + item.id_cliente + ',' + item.id_direccion + ')"><i class="fa fa-close"></i></button>';

                html += '</td>';
                html += '</tr>';
            });

            html += '</tbody>';
            html += '</table>';
            html += '</small>';

            $("#listado").html(html);

            $('#tabla-listado2').dataTable({
                "aaSorting": [[1, "desc"]]
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })

}

$("#frmgrabar").submit(function (evento) {
    evento.preventDefault();

    swal({
        title: "Confirme",
        text: "¿Esta seguro de grabar los datos ingresados?",
        showCancelButton: true,
        confirmButtonColor: '#3d9205',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/pregunta.png"
    }, function (isConfirm) {
        if (isConfirm) { //el usuario hizo clic en el boton SI     

            if ($("#txttipooperacion").val() == "agregar") {

                var ruta = DIRECCION_WS + "direccion.cliente.agregar.personal.php";
                var token = $.cookie('token');

                var id_cliente = $("#txtcodigo").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var celular = $("#txtcelular").val()
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, direccion: direccion, telefono: telefono, celular: celular, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            } else {

                var ruta = DIRECCION_WS + "direccion.cliente.editar.php";
                var token = $.cookie('token');

                var id_cliente = $("#txtcodigo").val()
                var id_direccion = $("#txtnrodireccion").val()
                var direccion = $("#txtdireccion").val()
                var telefono = $("#txttelefono").val()
                var celular = $("#txtcelular").val()
                var id_departamento = $("#cbodepartamentomodal").val()
                var id_provincia = $("#cboprovinciamodal").val()
                var id_distrito = $("#cbodistritomodal").val()

                $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion, direccion: direccion, telefono: telefono, celular: celular, id_departamento: id_departamento, id_provincia: id_provincia, id_distrito: id_distrito}, function () {
                }).done(function (resultado) {
                    var datosJSON = resultado;
                    if (datosJSON.estado === 200) {
                        swal("Exito", datosJSON.mensaje, "success");
                        $("#btncerrar").click(); //cerrar ventana
                        listar();//refrescar los datos
                    } else {
                        swal("Mensaje del sistema", resultado, "warning");
                    }
                }).fail(function (error) {
                    var datosJSON = $.parseJSON(error.responseText);
                    swal("Error", datosJSON.mensaje, "error");
                })
            }
        }
    });
});

function leerDatos(id_cliente, id_direccion) {

    document.getElementById("txtcodigo").disabled = true;

    var ruta = DIRECCION_WS + "direccion.cliente.leerdatos.php";
    var token = $.cookie('token');

    $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            $.each(datosJSON.datos, function (i, item) {

                $("#txtcodigo").val(item.id_cliente)
                $("#txtnrodireccion").val(item.id_direccion)
                $("#txtdireccion").val(item.direccion)
                $("#txttelefono").val(item.telefono)
                $("#txtcelular").val(item.celular)


                $("#cbodepartamentomodal").val(item.id_departamento)
                $("#cbodepartamentomodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cboprovinciamodal").val(item.id_provincia)
                });

                $("#cboprovinciamodal").val(item.id_provincia)
                $("#cboprovinciamodal").change();
                $("#myModal").on("shown.bs.modal", function () {
                    $("#cbodistritomodal").val(item.id_distrito)
                });

                $("#titulomodal").text("Editar Dirección.");
            });
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function eliminar(id_cliente, id_direccion) {
    swal({
        title: "Confirme",
        text: "¿Esta seguro de eliminar el registro seleccionado?",
        showCancelButton: true,
        confirmButtonColor: '#d93f1f',
        confirmButtonText: 'Si',
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: true,
        imageUrl: "../imagenes/eliminar.png"
    },
            function (isConfirm) {
                if (isConfirm) {

                    var ruta = DIRECCION_WS + "direccion.cliente.eliminar.php";
                    var token = $.cookie('token');

                    $.post(ruta, {token: token, id_cliente: id_cliente, id_direccion: id_direccion}, function () {
                    }).done(function (resultado) {
                        var datosJSON = resultado;
                        if (datosJSON.estado === 200) {
                            listar();
                            swal("Exito", datosJSON.mensaje, "success");
                        } else {
                            swal("Mensaje del sistema", resultado, "warning");
                        }
                    }).fail(function (error) {
                        var datosJSON = $.parseJSON(error.responseText);
                        swal("Error", datosJSON.mensaje, "error");
                    })
                }
            });
}



