$(document).ready(function () {

    if (!$.cookie('nombreUsuario').length > 0) {
        window.location = "./inicio-sesion.php";
    } else {

        var html = "";

        html += '<a href="#" class="dropdown-toggle" data-toggle="dropdown">'
        html += '<img src="../imagenes/1.png" class="user-image" alt="User Image"/>'
        html += '<span class="hidden-xs"> '+$.cookie('nombreUsuario')+'</span>'
        html += '</a>'
        html += '<ul class="dropdown-menu">'
        html += '<li class="user-header">'
        html += '<img src="../imagenes/1.png" class="img-circle" alt="User Image" />'
        html += '<p>'
        html += $.cookie('nombreUsuario')
        html += '<br>'
        html += '<small> '+$.cookie('areaUsuario')+' - '+$.cookie('cargoUsuario')+' </small>'
        html += '</p>'
        html += '</li>'
        html += '<li class="user-footer">'
        html += '<div class="pull-left">'
        html += '<a href="cambiar.contrasena.vista.php" target="myModal" id="btncambiarcontrasena" class="btn btn-default btn-flat"><i class="fa fa-key"></i> Cambiar Contraseña</a>'
        html += '</div>'
        html += '<div class="pull-right">'
        html += '<button type="button" id="btnCerrarSesion" name="btnCerrarSesion" class="btn btn-danger btn-flat"><i class="fa fa-power-off"></i> Salir</a>'
        html += '</div>'
        html += '</li>'
        html += '</ul>'

        $("#frmcabecera").html(html)
    }
});

$(document).on("click", "#btnCerrarSesion", function () {
    $.cookie('nombreUsuario', "");
    $.cookie('cargoUsuario', "");
    $.cookie('token', "");
    window.location = "./inicio-sesion.php";
})

