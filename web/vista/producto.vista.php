<?php
require_once '../util/funciones/definiciones.php';

//if (isset($_FILES['imagen_producto'])) {
//    $file = $_FILES['imagen_producto'];
//
//    //propiedades
//    $file_name = $file['name'];
//    $file_tmp = $file['tmp_name'];
//    $file_size = $file['size'];
//    $file_error = $file['error'];
//
//    //extensiones
//    $file_ext = explode('.', $file_name);
//    $file_ext = strtolower(end($file_name));
//
//    $allowed = array('jpg', 'jpeg', 'png');
//
//    if (in_array($file_namee, $allowed)) {
//        if ($file_error === 0) {
//            if ($file_size <= 2097152) {
//                $file_name_new = uniqid('', true).'.'.$file_ext;
//                $file_destination = 
//            }
//        }
//    }
//
//    2097152
//            }
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title><?php echo C_NOMBRE_SOFTWARE; ?></title>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>

        <?php
        include 'estilos.vista.php';
        ?>

    </head>
    <body class="skin-red layout-top-nav">
        <!-- Site wrapper -->
        <div class="wrapper">

            <?php
            include 'cabecera.vista.php';
            ?>

            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1 class="text-bold text-black" style="font-size: 20px;">Mantenimiento De Producto</h1>
                </section>

                <section class="content">

                    <!-- INICIO del formulario modal -->
                    <small>
                        <form id="frmgrabar" enctype="multipart/form-data">
                            <div class="modal fade" id="myModal" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header"> 
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="titulomodal">Título de la ventana</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="hidden" name="txttipooperacion" id="txttipooperacion" class="form-control">
                                            <div class="row">
                                                <div class="col-xs-3">
                                                    <p>Código Producto<input type="text" name="txtcodigo" id="txtcodigo" class="form-control input-sm text-center text-bold" placeholder="" readonly=""></p>
                                                </div>
                                            </div>
                                            <p>Nombre <font color = "red">*</font>
                                                <input type="text" name="txtnombre" id="txtnombre" class="form-control input-sm" placeholder="" required="">
                                            </p>
                                            <p>Descripcion <font color = "red">*</font>
                                                <textarea name="txtdescripcion" id="txtdescripcion" class="form-control input-sm" placeholder="" required="" rows="5"> </textarea>
                                            </p>
                                            <p>Precio <font color = "red">*</font>
                                                <input type="text" name="txtprecio" id="txtprecio" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosDecimales(event, this)">
                                            </p>
                                            <p>Cantidad ml<font color = "red">*</font>
                                                <input type="text" name="txtcantidad" id="txtcantidad" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosDecimales(event, this)">
                                            </p>
                                            <p>Unidad por caja<font color = "red">*</font>
                                                <input type="text" name="txtunidadxcaja" id="txtunidadxcaja" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosEnteros(event, this)">
                                            </p>
                                            <p>
                                                Bonificacion <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbobonificacionmodal" id="cbobonificacionmodal" required="" >
                                                </select>
                                            </p>
                                            <p>Cantidad bonificacion<font color = "red">*</font>
                                                <input type="text" name="txtcantidadbonificacion" id="txtcantidadbonificacion" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosEnteros(event, this)">
                                            </p>
                                            <p>Descuento<font color = "red">*</font>
                                                <input type="text" name="txtdescuento" id="txtdescuento" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosDecimales(event, this)">
                                            </p>
                                            <p>Precio Oferta<font color = "red">*</font>
                                                <input type="text" name="txtpreciooferta" id="txtpreciooferta" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosDecimales(event, this)">
                                            </p>
                                            <p>Precio por botella<font color = "red">*</font>
                                                <input type="text" name="txtprecioxbotella" id="txtprecioxbotella" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosDecimales(event, this)">
                                            </p>
                                            <p>Stock<font color = "red">*</font>
                                                <input type="text" name="txtstock" id="txtstock" class="form-control input-sm" placeholder="" required="" onkeypress="return ValidarNumerosEnteros(event, this)">
                                            </p>
                                            <p>
                                                Marca <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbomarcamodal" id="cbomarcamodal" required="" >
                                                </select>
                                            </p>
                                            <p>
                                                Categoria <font color = "red">*</font>
                                                <select class="form-control input-sm" name="cbocategoriamodal" id="cbocategoriamodal" required="" >
                                                </select>
                                            </p>
                                            <p>
                                                Subir Imagen
                                                <input type="file" name="imagen" id="imagen">
                                            <div id="images-to-upload">

                                            </div><!-- end #images-to-upload -->
                                            </p>
                                            <p>
                                                Estado <font color = "red">*</font>
                                                <select class="form-control input-sm" id="cboestado" name="cboestado" required="" >
                                                    <option value="" disabled selected>Seleccione un estado</option>
                                                    <option value="A" selected="">Activo</option>
                                                    <option value="I" >Inactivo</option>
                                                </select>
                                            </p>
                                            <p>
                                                <font color = "red">* Campos obligatorios</font>
                                            </p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="submit" class="btn btn-success" aria-hidden="true"><i class="fa fa-save"></i> Grabar</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal" id="btncerrar"><i class="fa fa-close"></i> Cerrar</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </small>
                    <!-- FIN del formulario modal -->

                    <div class="row">
                        <div class="col-xs-3">
                            <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal" id="btnagregar"><i class="fa fa-copy"></i> Agregar Nuevo Producto</button>
                        </div>
                    </div>
                    <p>
                    <div class="box box-success">
                        <div class="box-body">
                            <div id="listado">
                            </div>
                        </div>
                    </div>
                    </p>
                </section>
            </div>
        </div><!-- ./wrapper -->
        <?php
        include 'scripts.vista.php';
        ?>
        <!--JS-->
        <script src="js/util.js" type="text/javascript"></script>
        <script src="js/cargar-combos.js" type="text/javascript"></script>
        <script src="js/producto.js" type="text/javascript"></script>

    </body>
</html>



