-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2017-04-15 18:22:55.657

-- tables
-- Table: area
CREATE TABLE area (
    id_area int  NOT NULL,
    nombre int  NOT NULL,
    CONSTRAINT area_pk PRIMARY KEY (id_area)
);

-- Table: cargo
CREATE TABLE cargo (
    id_cargo int  NOT NULL,
    nombre varchar(50)  NULL,
    CONSTRAINT cargo_pk PRIMARY KEY (id_cargo)
);

-- Table: categoria
CREATE TABLE categoria (
    id_categoria int  NOT NULL,
    nombre varchar(100)  NULL,
    CONSTRAINT categoria_pk PRIMARY KEY (id_categoria)
);

-- Table: cliente
CREATE TABLE cliente (
    id_cliente varchar(11)  NOT NULL,
    nombre_razonsocial varchar(100)  NOT NULL,
    direccion varchar(100)  NOT NULL,
    telefono int  NOT NULL,
    celular int  NOT NULL,
    email char(32)  NOT NULL,
    clave int  NOT NULL,
    id_departamento char(2)  NOT NULL,
    id_provincia char(2)  NOT NULL,
    id_distrito char(2)  NOT NULL,
    CONSTRAINT cliente_pk PRIMARY KEY (id_cliente)
);

-- Table: correlativo
CREATE TABLE correlativo (
    tabla varchar(100)  NOT NULL,
    numero int  NULL,
    CONSTRAINT correlativo_pk PRIMARY KEY (tabla)
);

-- Table: departamento
CREATE TABLE departamento (
    id_departamento char(2)  NOT NULL,
    nombre varchar(50)  NOT NULL,
    CONSTRAINT departamento_pk PRIMARY KEY (id_departamento)
);

-- Table: distrito
CREATE TABLE distrito (
    id_departamento char(2)  NOT NULL,
    id_provincia char(2)  NOT NULL,
    id_distrito char(2)  NOT NULL,
    nombre varchar(50)  NOT NULL,
    CONSTRAINT distrito_pk PRIMARY KEY (id_departamento,id_provincia,id_distrito)
);

-- Table: marca
CREATE TABLE marca (
    id_marca int  NOT NULL,
    nombre varchar(50)  NULL,
    CONSTRAINT marca_pk PRIMARY KEY (id_marca)
);

-- Table: personal
CREATE TABLE personal (
    dni_personal char(8)  NOT NULL,
    apellido_paterno varchar(50)  NOT NULL,
    apellido_materno varchar(50)  NOT NULL,
    nombres varchar(50)  NOT NULL,
    direccion int  NOT NULL,
    telefono int  NOT NULL,
    email varchar(70)  NOT NULL,
    clave char(32)  NOT NULL,
    id_departamento char(2)  NOT NULL,
    id_provincia char(2)  NOT NULL,
    id_distrito char(2)  NOT NULL,
    id_area int  NOT NULL,
    id_cargo int  NOT NULL,
    CONSTRAINT personal_pk PRIMARY KEY (dni_personal)
);

-- Table: producto
CREATE TABLE producto (
    id_producto int  NOT NULL,
    nombre varchar(100)  NOT NULL,
    precio decimal(14,2)  NOT NULL,
    cantidad decimal(14,3)  NOT NULL,
    unidad_x_caja int  NOT NULL,
    bonificacion int  NULL,
    descuento decimal(5,2)  NOT NULL,
    precio_oferta decimal(14,2)  NOT NULL,
    precio_x_botella decimal(14,6)  NOT NULL,
    stock int  NOT NULL,
    id_categoria int  NOT NULL,
    marca_id_marca int  NOT NULL,
    CONSTRAINT producto_pk PRIMARY KEY (id_producto)
);

-- Table: provincia
CREATE TABLE provincia (
    id_departamento char(2)  NOT NULL,
    id_provincia char(2)  NOT NULL,
    nombre varchar(50)  NOT NULL,
    CONSTRAINT provincia_pk PRIMARY KEY (id_departamento,id_provincia)
);

-- Table: serie_comprobante
CREATE TABLE serie_comprobante (
    id_tipo_comprobante char(1)  NOT NULL,
    numero_serie int  NOT NULL,
    numero_documento int  NOT NULL,
    CONSTRAINT serie_comprobante_pk PRIMARY KEY (id_tipo_comprobante,numero_serie)
);

-- Table: tipo_comprobante
CREATE TABLE tipo_comprobante (
    id_tipo_comprobante char(1)  NOT NULL,
    descripcion varchar(50)  NOT NULL,
    CONSTRAINT tipo_comprobante_pk PRIMARY KEY (id_tipo_comprobante)
);

-- Table: venta
CREATE TABLE venta (
    numero_venta int  NOT NULL,
    id_tipo_comprobante char(1)  NOT NULL,
    numero_serie int  NOT NULL,
    numero_docuemento int  NOT NULL,
    id_cliente varchar(11)  NOT NULL,
    fecha_venta date  NULL,
    porcentaje_igv int  NULL,
    sub_total int  NULL,
    igv int  NULL,
    total int  NULL,
    fecha_registro date  NULL,
    hora_registro time  NULL,
    dni_personal char(8)  NOT NULL,
    estado char(1)  NOT NULL,
    CONSTRAINT venta_pk PRIMARY KEY (numero_venta)
);

-- Table: venta_detalle
CREATE TABLE venta_detalle (
    numero_venta int  NOT NULL,
    item int  NOT NULL,
    id_producto int  NOT NULL,
    cantidad int  NOT NULL,
    precio numeric(10,2)  NOT NULL,
    importe numeric(14,2)  NOT NULL,
    CONSTRAINT venta_detalle_pk PRIMARY KEY (numero_venta,item,id_producto)
);

-- foreign keys
-- Reference: cliente_distrito (table: cliente)
ALTER TABLE cliente ADD CONSTRAINT cliente_distrito
    FOREIGN KEY (id_departamento, id_provincia, id_distrito)
    REFERENCES distrito (id_departamento, id_provincia, id_distrito)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: distrito_provincia (table: distrito)
ALTER TABLE distrito ADD CONSTRAINT distrito_provincia
    FOREIGN KEY (id_departamento, id_provincia)
    REFERENCES provincia (id_departamento, id_provincia)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: personal_area (table: personal)
ALTER TABLE personal ADD CONSTRAINT personal_area
    FOREIGN KEY (id_area)
    REFERENCES area (id_area)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: personal_cargo (table: personal)
ALTER TABLE personal ADD CONSTRAINT personal_cargo
    FOREIGN KEY (id_cargo)
    REFERENCES cargo (id_cargo)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: personal_distrito (table: personal)
ALTER TABLE personal ADD CONSTRAINT personal_distrito
    FOREIGN KEY (id_departamento, id_provincia, id_distrito)
    REFERENCES distrito (id_departamento, id_provincia, id_distrito)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: producto_categoria (table: producto)
ALTER TABLE producto ADD CONSTRAINT producto_categoria
    FOREIGN KEY (id_categoria)
    REFERENCES categoria (id_categoria)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: producto_marca (table: producto)
ALTER TABLE producto ADD CONSTRAINT producto_marca
    FOREIGN KEY (marca_id_marca)
    REFERENCES marca (id_marca)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: producto_producto (table: producto)
ALTER TABLE producto ADD CONSTRAINT producto_producto
    FOREIGN KEY (bonificacion)
    REFERENCES producto (id_producto)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: provincia_departamento (table: provincia)
ALTER TABLE provincia ADD CONSTRAINT provincia_departamento
    FOREIGN KEY (id_departamento)
    REFERENCES departamento (id_departamento)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: serie_comprobante_tipo_comprobante (table: serie_comprobante)
ALTER TABLE serie_comprobante ADD CONSTRAINT serie_comprobante_tipo_comprobante
    FOREIGN KEY (id_tipo_comprobante)
    REFERENCES tipo_comprobante (id_tipo_comprobante)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: venta_cliente (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_cliente
    FOREIGN KEY (id_cliente)
    REFERENCES cliente (id_cliente)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: venta_detalle_producto (table: venta_detalle)
ALTER TABLE venta_detalle ADD CONSTRAINT venta_detalle_producto
    FOREIGN KEY (id_producto)
    REFERENCES producto (id_producto)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: venta_detalle_venta (table: venta_detalle)
ALTER TABLE venta_detalle ADD CONSTRAINT venta_detalle_venta
    FOREIGN KEY (numero_venta)
    REFERENCES venta (numero_venta)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: venta_personal (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_personal
    FOREIGN KEY (dni_personal)
    REFERENCES personal (dni_personal)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: venta_tipo_comprobante (table: venta)
ALTER TABLE venta ADD CONSTRAINT venta_tipo_comprobante
    FOREIGN KEY (id_tipo_comprobante)
    REFERENCES tipo_comprobante (id_tipo_comprobante)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

