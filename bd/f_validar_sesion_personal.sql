﻿CREATE OR REPLACE FUNCTION public.f_validar_sesion_personal(
    IN p_dni character varying,
    IN p_clave character varying)
  RETURNS TABLE(estado integer, dato character varying, usuario character varying, codigo_personal character varying, cargo character varying, area character varying) AS
$BODY$

declare
v_registro record; --almacenar un registro(solo uno, si se requieren mas se usa: refcursor)
v_respuesta character varying;
v_estado integer;
begin
	begin
	
		SELECT 
		  personal.dni_personal, 
		  personal.apellido_paterno ||' '|| personal.apellido_materno ||' , '|| personal.nombres as nombre, 
		  personal.clave, 
		  area.nombre as area, 
		  cargo.nombre as cargo, 
		  personal.estado
		INTO
		  v_registro
		FROM 
		  public.personal, 
		  public.area, 
		  public.cargo
		WHERE 
		  personal.id_cargo = cargo.id_cargo AND
		  area.id_area = personal.id_area AND
		  personal.dni_personal = p_dni;

		v_estado = 500; --Error


		if FOUND then
			--if v_registro.clave = md5(p_clave) then
			if v_registro.clave = p_clave then
				if v_registro.estado = 'I' then
				v_respuesta = 'Usuario Inactivo';
				else
				v_estado = 200;
				v_respuesta = v_registro.dni_personal;
				end if;
			else
			v_respuesta = 'Contraseña Incorrecta';
			end if;
		else
		v_respuesta = 'El usuario no existe';
		end if;
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

if v_estado = 200 then
return query select v_estado, v_respuesta, initcap(v_registro.nombre)::varchar, (v_registro.dni_personal)::varchar,initcap(v_registro.cargo)::varchar,initcap(v_registro.area)::varchar;
else
return query select v_estado, v_respuesta, '-'::varchar,'-'::varchar,'-'::varchar,'-'::varchar;
end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE