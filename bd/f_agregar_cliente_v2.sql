﻿select * from f_agregar_cliente('111111111','silvio','dsfsdf','24535','345345','sdfsdf','202cb962ac59075b964b07152d234b70','1','1','1')

CREATE OR REPLACE FUNCTION public.f_agregar_cliente(
    IN p_id_cliente character varying,
    IN p_nombre_razonsocial character varying,
    IN p_direccion character varying,
    IN p_telefono character varying,
    IN p_celular character varying,
    IN p_email character varying,
    IN p_clave character varying,
    IN p_id_departamento character varying,
    IN p_id_provincia character varying,
    IN p_id_distrito character varying)
  RETURNS TABLE(cliente character varying, nombre character varying) AS
$BODY$
	DECLARE

		v_productoid integer;
		v_codigoproducto varchar;
		v_cliente varchar;
		v_id_direccion integer;
	begin	
		begin
			select id_cliente into v_cliente from cliente where id_cliente @@ p_id_cliente;

			if v_cliente is null then

				update correlativo set numero = 0 where tabla = 'direccion';

				select numero + 1 into v_id_direccion from correlativo where tabla = 'direccion';

				INSERT INTO public.cliente(id_cliente, nombre_razonsocial, email, clave)
				VALUES (p_id_cliente, p_nombre_razonsocial, p_email, p_clave);

				INSERT INTO public.direccion(id_cliente, id_direccion, direccion, telefono, celular, id_departamento,id_provincia, id_distrito)
				VALUES (p_id_cliente, v_id_direccion, p_direccion, p_telefono, p_celular, p_id_departamento, p_id_provincia, p_id_distrito);
			
			else
				RAISE EXCEPTION 'El cliente con (dni o ruc) (%) ya existe ', p_id_cliente;			
			
			end if;
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_id_cliente,p_nombre_razonsocial;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE