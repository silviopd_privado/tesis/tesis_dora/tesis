﻿select * from f_agregar_personal('46705012','a','a','a','asdsd','asdsd','asdsd','202cb962ac59075b964b07152d234b70',
				'1','1','1',1,1)

CREATE OR REPLACE FUNCTION public.f_agregar_personal(
    IN p_dni_personal character varying,
    IN p_apellido_paterno character varying,
    IN p_apellido_materno character varying,
    IN p_nombres character varying,
    IN p_direccion character varying,
    IN p_telefono character varying,
    IN p_email character varying,
    IN p_clave character varying,
    IN p_id_departamento character varying,
    IN p_id_provincia character varying,
    IN p_id_distrito character varying,
    IN p_id_area integer,
    IN p_id_cargo integer)
  RETURNS TABLE(cliente character varying, nombre character varying) AS
$BODY$		
	begin	
		begin

			INSERT INTO public.personal(
				    dni_personal, apellido_paterno, apellido_materno, nombres, direccion, 
				    telefono, email, clave, id_departamento, id_provincia, id_distrito, 
				    id_area, id_cargo)
			    VALUES (p_dni_personal, p_apellido_paterno, p_apellido_materno, p_nombres, p_direccion, 
				    p_telefono, p_email, p_clave, p_id_departamento, p_id_provincia, p_id_distrito, 
				    p_id_area, p_id_cargo);
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_dni_personal,(p_apellido_paterno ||' '|| p_apellido_materno ||' , '||p_nombres)::varchar;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE