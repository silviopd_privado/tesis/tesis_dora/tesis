﻿CREATE OR REPLACE FUNCTION public.f_agregar_categoria(
    IN p_id_marca integer,
    IN p_nombre character varying)
  RETURNS TABLE(marca integer, categoria integer) AS
$BODY$
	DECLARE

		v_id_categoria integer;
		
	begin	

		begin

			select id_categoria+1 INTO v_id_categoria from categoria where id_marca = p_id_marca order by 1 desc limit 1;

			INSERT INTO public.categoria(id_categoria, nombre, id_marca)
			VALUES (v_id_categoria, p_nombre, p_id_marca);
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_id_marca,v_id_categoria;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
