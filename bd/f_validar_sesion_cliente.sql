﻿CREATE OR REPLACE FUNCTION public.f_validar_sesion_cliente(
    IN p_dni character varying,
    IN p_clave character varying)
  RETURNS TABLE(estad integer, dato character varying, usuario character varying, codigo_usuario character varying) AS
$BODY$

declare
v_registro record; --almacenar un registro(solo uno, si se requieren mas se usa: refcursor)
v_respuesta character varying;
v_estado integer;
begin
	begin
	
		SELECT 
		  id_cliente,
		  nombre_razonsocial,
		  clave, 
		  estado
		INTO
		  v_registro
		FROM 
		  public.cliente
		WHERE 
		  id_cliente = p_dni;

		v_estado = 500; --Error


		if FOUND then
			--if v_registro.clave = md5(p_clave) then
			if v_registro.clave = p_clave then
				if v_registro.estado = 'I' then
				v_respuesta = 'Usuario Inactivo';
				else
				v_estado = 200;
				v_respuesta = 'Usuario Encontrado';
				end if;
			else
			v_respuesta = 'Contraseña Incorrecta';
			end if;
		else
		v_respuesta = 'El usuario no existe';
		end if;
		
		EXCEPTION
		when others then
		RAISE EXCEPTION '%', SQLERRM;
	end;

if v_estado = 200 then
return query select v_estado, v_respuesta, initcap(v_registro.nombre_razonsocial)::varchar, (v_registro.id_cliente)::varchar;
else
return query select v_estado, v_respuesta, '-'::varchar,'-'::varchar;
end if;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE