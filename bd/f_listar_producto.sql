﻿CREATE OR REPLACE FUNCTION public.f_listar_producto()
  RETURNS TABLE(id_producto integer, nombre character varying,descripcion varchar, precio numeric,cantidad numeric,unidad_x_caja integer,cantidad_bonificacion integer,bonificacion character varying, descuento numeric,precio_oferta numeric,precio_x_botella numeric,stock integer,marca varchar,categoria varchar) AS
$BODY$
	
	
	begin
		return query
		SELECT 
			p.id_producto, 
			p.nombre,
			p.descripcion, 
			p.precio, 
			p.cantidad, 
			p.unidad_x_caja,
			p.cantidad_bonificacion, 
			p2.nombre as bonificacion, 
			p.descuento, 
			p.precio_oferta, 
			p.precio_x_botella, 
			p.stock, 
			m.nombre as marca, 
			c.nombre as categoria
		FROM 
			producto p inner join categoria c on p.id_categoria = c.id_categoria AND p.id_marca = c.id_marca
				   inner join marca m on p.id_marca = m.id_marca
				   inner join producto p2 on p2.id_producto = p.bonificacion
		WHERE
			p.estado_producto like 'A'
		ORDER BY
			2;															
	end
$BODY$
  LANGUAGE plpgsql VOLATILE


  select * from f_listar_producto()