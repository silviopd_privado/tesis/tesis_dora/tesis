﻿
select * from f_comparacion('03')


CREATE OR REPLACE FUNCTION public.f_comparacion(
    IN p_tipo_comprobante character)
  RETURNS TABLE(marca integer) AS
$BODY$
	DECLARE

		v_numero_documento integer;
		v_ultimo_numero integer;

		v_detalle_serie_comprobante_registro record;
		v_numero integer;
		
	begin	

		begin

			FOR v_detalle_serie_comprobante_registro IN select * from serie_comprobante where id_tipo_comprobante like p_tipo_comprobante order by 2 
			LOOP
				if v_detalle_serie_comprobante_registro.numero_documento < v_detalle_serie_comprobante_registro.ultimo_numero then

					v_numero = v_detalle_serie_comprobante_registro.numero_documento;
					exit;
				end if;
			END LOOP;
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select v_numero;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE;