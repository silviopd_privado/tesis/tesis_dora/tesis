﻿CREATE OR REPLACE FUNCTION public.f_registrar_venta(
		IN p_id_cliente character,
		IN p_id_tipo_comprobante character,
		IN p_fecha_emitida date,
		IN p_id_forma_pago character,
		IIN p_numero_cuota integer,
		IN p_detalle_venta json)
	RETURNS TABLE(nv integer) AS
$BODY$
	declare

		v_numero_venta integer;
		v_porcentaje_igv numeric;

	begin
		begin

			--Inicio: generar el nuevo numero de venta
			select numero + 1 into v_numero_venta 
			from correlativo where tabla = 'venta';

			RAISE NOTICE 'Nuevo número de venta: %', v_numero_venta;
			--Fin: generar el nuevo numero de venta



			--Inicio: obtener numero_serie

			select * from serie_comprobante where id_tipo_comprobante = '01'
			
			
			--Inicio: obtener numero_serie

			
			--Inicio: obtener el porcentaje de IGV			
			select valor::numeric(7,2) into v_porcentaje_igv 
			from configuracion where codigo = 1;
			
			RAISE NOTICE 'Porcentaje de IGV: %', v_porcentaje_igv;
			--Fin: obtener el porcentaje de IGV


			--Inicio: Insertar en la tabla venta
			INSERT INTO public.venta(
						numero_venta, 
						id_tipo_comprobante,
						numero_serie, 
						numero_docuemento, 
						id_cliente,
						fecha_venta, 
						porcentaje_igv, 
						sub_total, 
						igv, 
						total, 
						fecha_registro, 
						hora_registro, 
						dni_personal, 
						id_forma_pago, 
						numero_cuota
						)
					VALUES (
						v_numero_venta,
						p_id_tipo_comprobante,

					);
			--Fin: Insertar en la tabla venta
		
		end;

		--return 1; --Cuando la transacción terminó con exito
		return query select v_numero_venta;
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE