﻿/*select * from departamento
order by 2;
select * from provincia;
select * from distrito;

delete from departamento;
delete from provincia;
delete from distrito;

alter table distrito rename column nombre_distrito to nombre

select d.nombre,p.nombre,di.nombre
from departamento d inner join provincia p on (d.id_departamento=p.id_departamento) inner join distrito di on (p.id_provincia=di.id_provincia)

select d.nombre,p.nombre,di.nombre
from departamento d, provincia p, distrito di
where d.id_departamento=p.id_departamento and p.id_provincia=di.id_provincia;*/

/*AMAZONAS*/
insert into departamento(id_departamento,nombre) values (1,'Amazonas');

insert into provincia(id_departamento,id_provincia,nombre) values (1,1,'Chachapoyas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,01, 'Chachapoyas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,02, 'Asuncion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,03, 'Balsas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,04, 'Cheto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,05, 'Chiliquin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,06, 'Chuquibamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,07, 'Granada');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,08, 'Huancas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,09, 'La Jalca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,10, 'Leimebamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,11, 'Levanto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,12, 'Magdalena');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,13, 'Mariscal Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,14, 'Molinopampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,15, 'Montevideo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,16, 'Olleros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,17, 'Quinjalca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,18, 'San Francisco De Daguas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,19, 'San Isidro De Maino');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,20, 'Soloco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,1,21, 'Sonche');

insert into provincia(id_departamento,id_provincia,nombre) values (1,2,'Bagua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,2,01, 'La Peca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,2,02, 'Aramango');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,2,03, 'Copallin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,2,04, 'El Parco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,2,05, 'Imaza');

insert into provincia(id_departamento,id_provincia,nombre) values (1,3,'Bongara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,01, 'Jumbilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,02, 'Chisquilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,03, 'Churuja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,04, 'Corosha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,05, 'Cuispes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,06, 'Florida');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,07, 'Jazan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,08, 'Recta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,09, 'San Carlos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,10, 'Shipasbamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,11, 'Valera');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,3,12, 'Yambrasbamba');

insert into provincia(id_departamento,id_provincia,nombre) values (1,4,'Condorcanqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,4,01, 'Nieva');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,4,02, 'El Cenepa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,4,03, 'Rio Santiago');

insert into provincia(id_departamento,id_provincia,nombre) values (1,5,'Luya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,01, 'Lamud');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,02, 'Camporredondo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,03, 'Cocabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,04, 'Colcamar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,05, 'Conila');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,06, 'Inguilpata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,07, 'Longuita');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,08, 'Lonya Chico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,09, 'Luya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,10, 'Luya Viejo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,11, 'Maria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,12, 'Ocalliv');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,13, 'Ocumal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,14, 'Pisuquia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,15, 'Providencia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,16, 'San Cristobal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,17, 'San Francisco Del Yeso');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,18, 'San Jeronimo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,19, 'San Juan De Lopecancha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,20, 'Santa Catalina');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,21, 'Santo Tomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,22, 'Tingo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,5,23, 'Trita');

insert into provincia(id_departamento,id_provincia,nombre) values (1,6,'Rodriguez De Mendo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,01, 'San Nicolas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,02, 'Chirimoto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,03, 'Cochamal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,04, 'Huambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,05, 'Limabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,06, 'Longar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,07, 'Mariscal Benavides');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,08, 'Milpuc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,09, 'Omia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,10, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,11, 'Totora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,6,12, 'Vista Alegre');

insert into provincia(id_departamento,id_provincia,nombre) values (1,7,'Utcubamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,01, 'Bagua Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,02, 'Cajaruro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,03, 'Cumba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,04, 'El Milagro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,05, 'Jamalca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,06, 'Lonya Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(1,7,07, 'Yamon');

/*Ancash*/
insert into departamento(id_departamento,nombre) values (2,'Ancash');

insert into provincia(id_departamento,id_provincia,nombre) values (2,1,'Huaraz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,01, 'Huaraz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,02, 'Cochabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,03, 'Colcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,04, 'Huanchay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,05, 'Independencia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,06, 'Jangas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,07, 'La Libertad');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,08, 'Olleros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,09, 'Pampas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,10, 'Pariacoto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,11, 'Pira');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,1,12, 'Tarica');

insert into provincia(id_departamento,id_provincia,nombre) values (2,2,'Aija');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,2,01, 'Aija');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,2,02, 'Coris');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,2,03, 'Huacllan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,2,04, 'La Merced');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,2,05, 'Succha');

insert into provincia(id_departamento,id_provincia,nombre) values (2,3,'Antonio Raymondi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,01, 'Llamellin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,02, 'Aczo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,03, 'Chaccho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,04, 'Chingas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,05, 'Mirgas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,3,06, 'San Juan De Rontoy');

insert into provincia(id_departamento,id_provincia,nombre) values (2,4,'Asuncion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,4,01, 'Chacas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,4,02, 'Acochaca');

insert into provincia(id_departamento,id_provincia,nombre) values (2,5,'Bolognesi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,01, 'Chiquian');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,02, 'Abelardo Pardo Lezameta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,03, 'Antonio Raymondi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,04, 'Aquia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,05, 'Cajacay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,06, 'Canis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,07, 'Colquioc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,08, 'Huallanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,09, 'Huasta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,10, 'Huayllacayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,11, 'La Primavera');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,12, 'Mangas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,13, 'Pacllon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,14, 'San Miguel De Corpanqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,5,15, 'Ticllos');

insert into provincia(id_departamento,id_provincia,nombre) values (2,6,'Carhuaz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,01, 'Carhuaz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,02, 'Acopampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,03, 'Amashca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,04, 'Anta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,05, 'Ataquero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,06, 'Marcara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,07, 'Pariahuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,08, 'San Miguel De Aco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,09, 'Shilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,10, 'Tinco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,6,11, 'Yungar');

insert into provincia(id_departamento,id_provincia,nombre) values (2,7,'Carlos F. Fitzcarrald');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,7,01, 'San Luis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,7,02, 'San Nicolas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,7,03, 'Yauya');

insert into provincia(id_departamento,id_provincia,nombre) values (2,8,'Casma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,8,01, 'Casma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,8,02, 'Buena Vista Alta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,8,03, 'Comandante Noel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,8,04, 'Yautan');

insert into provincia(id_departamento,id_provincia,nombre) values (2,9,'Corongo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,01, 'Corongo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,02, 'Aco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,03, 'Bambas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,04, 'Cusca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,05, 'La Pampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,06, 'Yanac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,9,07, 'Yupan');

insert into provincia(id_departamento,id_provincia,nombre) values (2,10,'Huari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,01, 'Huari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,02, 'Anra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,03, 'Cajay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,04, 'Chavin De Huantar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,05, 'Huacachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,06, 'Huacchis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,07, 'Huachis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,08, 'Huantar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,09, 'Masin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,10, 'Paucas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,11, 'Ponto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,12, 'Rahuapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,13, 'Rapayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,14, 'San Marcos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,15, 'San Pedro De Chana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,10,16, 'Uco');

insert into provincia(id_departamento,id_provincia,nombre) values (2,11,'Huarmey');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,11,01, 'Huarmey');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,11,02, 'Cochapeti');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,11,03, 'Culebras');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,11,04, 'Huayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,11,05, 'Malvas');

insert into provincia(id_departamento,id_provincia,nombre) values (2,12,'Huaylas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,01, 'Caraz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,02, 'Huallanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,03, 'Huata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,04, 'Huaylas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,05, 'Mato');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,06, 'Pamparomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,07, 'Pueblo Libre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,08, 'Santa Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,09, 'Santo Toribio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,12,10, 'Yuracmarca');

insert into provincia(id_departamento,id_provincia,nombre) values (2,13,'Mariscal Luzuriaga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,01, 'Piscobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,02, 'Casca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,03, 'Eleazar Guzman Barron');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,04, 'Fidel Olivas Escudero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,05, 'Llama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,06, 'Llumpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,07, 'Lucma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,13,08, 'Musga');

insert into provincia(id_departamento,id_provincia,nombre) values (2,14,'Ocros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,01, 'Ocros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,02, 'Acas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,03, 'Cajamarquilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,04, 'Carhuapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,05, 'Cochas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,06, 'Congas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,07, 'Llipa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,08, 'San Cristobal De Rajan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,09, 'San Pedro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,14,10, 'Santiago De Chilcas');

insert into provincia(id_departamento,id_provincia,nombre) values (2,15,'Pallasca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,01, 'Cabana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,02, 'Bolognesi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,03, 'Conchucos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,04, 'Huacaschuque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,05, 'Huandoval');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,06, 'Lacabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,07, 'Llapo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,08, 'Pallasca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,09, 'Pampas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,10, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,15,11, 'Tauca');

insert into provincia(id_departamento,id_provincia,nombre) values (2,16,'Pomabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,16,01, 'Pomabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,16,02, 'Huayllan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,16,03, 'Parobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,16,04, 'Quinuabamba');

insert into provincia(id_departamento,id_provincia,nombre) values (2,17,'Recuay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,01, 'Recuay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,02, 'Catac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,03, 'Cotaparaco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,04, 'Huayllapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,05, 'Llacllin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,06, 'Marca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,07, 'Pampas Chico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,08, 'Pararin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,09, 'Tapacocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,17,10, 'Ticapampa');

insert into provincia(id_departamento,id_provincia,nombre) values (2,18,'Santa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,01, 'Chimbote');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,02, 'Caceres Del Peru');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,03, 'Coishco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,04, 'Macate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,05, 'Moro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,06, 'Nepeña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,07, 'Samanco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,08, 'Santa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,18,09, 'Nuevo Chimbote');

insert into provincia(id_departamento,id_provincia,nombre) values (2,19,'Sihuas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,01, 'Sihuas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,02, 'Acobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,03, 'Alfonso Ugarte');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,04, 'Cashapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,05, 'Chingalpo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,06, 'Huayllabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,07, 'Quiches');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,08, 'Ragash');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,09, 'San Juan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,19,10, 'Sicsibamba');

insert into provincia(id_departamento,id_provincia,nombre) values (2,20,'Yungay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,01, 'Yungay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,02, 'Cascapara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,03, 'Mancos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,04, 'Matacoto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,05, 'Quillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,06, 'Ranrahirca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,07, 'Shupluy');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(2,20,08, 'Yanama');

/*Apurimac*/
insert into departamento(id_departamento,nombre) values (3,'Apurimac');

insert into provincia(id_departamento,id_provincia,nombre) values (3,1,'Abancay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,01, 'Abancay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,02, 'Chacoche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,03, 'Circa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,04, 'Curahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,05, 'Huanipaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,06, 'Lambrama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,07, 'Pichirhua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,08, 'San Pedro De Cachora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,1,09, 'Tamburco');

insert into provincia(id_departamento,id_provincia,nombre) values (3,2,'Andahuaylas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,01, 'Andahuaylas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,02, 'Andarapa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,03, 'Chiara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,04, 'Huancarama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,05, 'Huancaray');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,06, 'Huayana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,07, 'Kishuara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,08, 'Pacobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,09, 'Pacucha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,10, 'Pampachiri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,11, 'Pomacocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,12, 'San Antonio De Cachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,13, 'San Jeronimo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,14, 'San Miguel De Chaccrampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,15, 'Santa Maria De Chicmo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,16, 'Talavera');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,17, 'Tumay Huaraca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,18, 'Turpo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,2,19, 'Kaquiabamba');

insert into provincia(id_departamento,id_provincia,nombre) values (3,3,'Antabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,01, 'Antabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,02, 'El Oro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,03, 'Huaquirca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,04, 'Juan Espinoza Medrano');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,05, 'Oropesa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,06, 'Pachaconas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,3,07, 'Sabaino');

insert into provincia(id_departamento,id_provincia,nombre) values (3,4,'Aymaraes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,01, 'Chalhuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,02, 'Capaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,03, 'Caraybamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,04, 'Chapimarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,05, 'Colcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,06, 'Cotaruse');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,07, 'Huayllo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,08, 'Justo Apu Sahuaraura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,09, 'Lucre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,10, 'Pocohuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,11, 'San Juan De Chacña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,12, 'Sañayca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,13, 'Soraya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,14, 'Tapairihua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,15, 'Tintay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,16, 'Toraya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,4,17, 'Yanaca');

insert into provincia(id_departamento,id_provincia,nombre) values (3,5,'Cotabambas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,01, 'Tambobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,02, 'Cotabambas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,03, 'Coyllurqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,04, 'Haquira');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,05, 'Mara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,5,06, 'Challhuahuacho');

insert into provincia(id_departamento,id_provincia,nombre) values (3,6,'Chincheros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,01, 'Chincheros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,02, 'Anco-Huallo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,03, 'Cocharcas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,04, 'Huaccana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,05, 'Ocobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,06, 'Ongoy');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,07, 'Uranmarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,6,08, 'Ranracancha');

insert into provincia(id_departamento,id_provincia,nombre) values (3,7,'Grau');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,01, 'Chuquibambilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,02, 'Curpahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,03, 'Gamarra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,04, 'Huayllati');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,05, 'Mamara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,06, 'Micaela Bastidas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,07, 'Pataypampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,08, 'Progreso');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,09, 'San Antonio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,10, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,11, 'Turpay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,12, 'Vilcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,13, 'Virundo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(3,7,14, 'Curasco');

/*Arequipa*/
insert into departamento(id_departamento,nombre) values (4,'Arequipa');

insert into provincia(id_departamento,id_provincia,nombre) values (4,1,'Arequipa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,01, 'Arequipa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,02, 'Alto Selva Alegre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,03, 'Cayma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,04, 'Cerro Colorado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,05, 'Characato');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,06, 'Chiguata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,07, 'Jacobo Hunter');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,08, 'La Joya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,09, 'Mariano Melgar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,10, 'Miraflores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,11, 'Mollebaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,12, 'Paucarpata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,13, 'Pocsi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,14, 'Polobaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,15, 'Quequeña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,16, 'Sabandia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,17, 'Sachaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,18, 'San Juan De Siguas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,19, 'San Juan De Tarucani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,20, 'Santa Isabel De Siguas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,21, 'Santa Rita De Siguas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,22, 'Socabaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,23, 'Tiabaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,24, 'Uchumayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,25, 'Vitor');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,26, 'Yanahuara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,27, 'Yarabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,28, 'Yura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,1,29, 'Jose Luis Bustamante Y Rivero');

insert into provincia(id_departamento,id_provincia,nombre) values (4,2,'Camana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,01, 'Camana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,02, 'Jose Maria Quimper');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,03, 'Mariano Nicolas Valcarcel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,04, 'Mariscal Caceres');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,05, 'Nicolas De Pierola');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,06, 'Ocoña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,07, 'Quilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,2,08, 'Samuel Pastor');

insert into provincia(id_departamento,id_provincia,nombre) values (4,3,'Caraveli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,01, 'Caraveli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,02, 'Acari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,03, 'Atico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,04, 'Atiquipa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,05, 'Bella Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,06, 'Cahuacho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,07, 'Chala');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,08, 'Chaparra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,09, 'Huanuhuanu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,10, 'Jaqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,11, 'Lomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,12, 'Quicacha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,3,13, 'Yauca');

insert into provincia(id_departamento,id_provincia,nombre) values (4,4,'Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,01, 'Aplao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,02, 'Andagua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,03, 'Ayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,04, 'Chachas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,05, 'Chilcaymarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,06, 'Choco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,07, 'Huancarqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,08, 'Machaguay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,09, 'Orcopampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,10, 'Pampacolca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,11, 'Tipan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,12, 'Uñon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,13, 'Uraca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,4,14, 'Viraco');

insert into provincia(id_departamento,id_provincia,nombre) values (4,5,'Caylloma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,01, 'Chivay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,02, 'Achoma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,03, 'Cabanaconde');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,04, 'Callalli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,05, 'Caylloma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,06, 'Coporaque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,07, 'Huambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,08, 'Huanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,09, 'Ichupampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,10, 'Lari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,11, 'Lluta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,12, 'Maca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,13, 'Madrigal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,14, 'San Antonio De Chuca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,15, 'Sibayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,16, 'Tapay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,17, 'Tisco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,18, 'Tuti');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,19, 'Yanque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,5,20, 'Majes');

insert into provincia(id_departamento,id_provincia,nombre) values (4,6,'Condesuyos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,01, 'Chuquibamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,02, 'Andaray');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,03, 'Cayarani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,04, 'Chichas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,05, 'Iray');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,06, 'Rio Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,07, 'Salamanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,6,08, 'Yanaquihua');

insert into provincia(id_departamento,id_provincia,nombre) values (4,7,'Islay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,01, 'Mollendo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,02, 'Cocachacra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,03, 'Dean Valdivia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,04, 'Islay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,05, 'Mejia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,7,06, 'Punta De Bombon');

insert into provincia(id_departamento,id_provincia,nombre) values (4,8,'La Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,01, 'Cotahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,02, 'Alca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,03, 'Charcana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,04, 'Huaynacotas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,05, 'Pampamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,06, 'Puyca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,07, 'Quechualla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,08, 'Sayla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,09, 'Tauria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,10, 'Tomepampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(4,8,11, 'Toro');

/*Ayacucho*/
insert into departamento(id_departamento,nombre) values (5,'Ayacucho');

insert into provincia(id_departamento,id_provincia,nombre) values (5,1,'Huamanga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,01, 'Ayacucho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,02, 'Acocro ');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,03, 'Acos Vinchos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,04, 'Carmen Alto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,05, 'Chiara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,06, 'Ocros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,07, 'Pacaycasa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,08, 'Quinua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,09, 'San Jose De Ticllas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,10, 'San Juan Bautista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,11, 'Santiago De Pischa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,12, 'Socos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,13, 'Tambillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,14, 'Vinchos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,1,15, 'Jesus Nazareno');
    
insert into provincia(id_departamento,id_provincia,nombre) values (5,2,'Cangallo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,01, 'Cangallo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,02, 'Chuschi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,03, 'Los Morochucos'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,04, 'Maria Parado De Bellido');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,05, 'Paras');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,2,06, 'Totos');
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,3,'Huanca Sancos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,3,01, 'Sancos');  
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,3,02, 'Carapo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,3,03, 'Sacsamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,3,04, 'Santiago De Lucanamarca'); 
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,4,'Huanta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,01, 'Huanta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,02, 'Ayahuanco'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,03, 'Huamanguilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,04, 'Iguain');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,05, 'Luricocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,06, 'Santillana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,07, 'Sivia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,4,08, 'Llochegua');
 
insert into provincia(id_departamento,id_provincia,nombre) values (5,5,'La Mar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,01, 'San Miguel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,02, 'Anco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,03, 'Ayna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,04, 'Chilcas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,05, 'Chungui'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,06, 'Luis Carranza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,07, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,5,08, 'Tambo');

insert into provincia(id_departamento,id_provincia,nombre) values (5,6,'Lucanas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,01, 'Puquio'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,02, 'Aucara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,03, 'Cabana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,04, 'Carmen Salcedo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,05, 'Chaviña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,06, 'Chipao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,07, 'Huac-Huas'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,08, 'Laramate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,09, 'Leoncio Prado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,10, 'Llauta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,11, 'Lucanas'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,12, 'Ocaña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,13, 'Otoca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,14, 'Saisa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,15, 'San Cristobal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,16, 'San Juan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,17, 'San Pedro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,18, 'San Pedro De Palco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,19, 'Sancos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,20, 'Santa Ana De Huaycahuacho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,6,21, 'Santa Lucia');
  
insert into provincia(id_departamento,id_provincia,nombre) values (5,7,'Parinacochas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,01, 'Coracora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,02, 'Chumpi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,03, 'Coronel Castaðeda'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,04, 'Pacapausa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,05, 'Pullo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,06, 'Puyusca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,07, 'San Francisco De Ravacayco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,7,08, 'Upahuacho');
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,8,'Paucar Del Sara Sara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,01, 'Pausa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,02, 'Colta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,03, 'Corculla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,04, 'Lampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,05, 'Marcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,06, 'Oyolo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,07, 'Pararca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,08, 'San Javier De Alpabamba'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,09, 'San Jose De Ushua'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,8,10, 'Sara Sara');
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,9,'Sucre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,01, 'Querobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,02, 'Belen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,03, 'Chalcos'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,04, 'Chilcayoc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,05, 'Huacaña'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,06, 'Morcolla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,07, 'Paico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,08, 'San Pedro De Larcay'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,09, 'San Salvador De Quije'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,10, 'Santiago De Paucaray');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,9,11, 'Soras'); 
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,10,'Victor Fajardo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,01, 'Huancapi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,02, 'Alcamenca'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,03, 'Apongo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,04, 'Asquipata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,05, 'Canaria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,06, 'Cayara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,07, 'Colca'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,08, 'Huamanquiquia'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,09, 'Huancaraylla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,10, 'Huaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,11, 'Sarhua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,10,12, 'Vilcanchos');  
   
insert into provincia(id_departamento,id_provincia,nombre) values (5,11,'Vilcas Huaman');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,01, 'Vilcas Huaman');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,02, 'Accomarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,03, 'Carhuanca'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,04, 'Concepcion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,05, 'Huambalpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,06, 'Independencia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,07, 'Saurama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(5,11,08, 'Vischongo');

/*Ayacucho*/
insert into departamento(id_departamento,nombre) values (6,'Cajamarca');
 
insert into provincia(id_departamento,id_provincia,nombre) values (6,1,'Cajamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,01, 'Cajamarca'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,02, 'Asuncion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,03, 'Chetilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,04, 'Cospan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,05, 'Encañada'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,06, 'Jesus'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,07, 'Llacanora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,08, 'Los Baños Del Inca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,09, 'Magdalena'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,10, 'Matara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,11, 'Namora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,1,12, 'San Juan');
 
insert into provincia(id_departamento,id_provincia,nombre) values (6,2,'Cajabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,2,01, 'Cajabamba'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,2,02, 'Cachachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,2,03, 'Condebamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,2,04, 'Sitacocha'); 

insert into provincia(id_departamento,id_provincia,nombre) values (6,3,'Celendin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,01, 'Celendin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,02, 'Chumuch');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,03, 'Cortegana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,04, 'Huasmin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,05, 'Jorge Chavez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,06, 'Jose Galvez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,07, 'Miguel Iglesias'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,08, 'Oxamarca'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,09, 'Sorochuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,10, 'Sucre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,11, 'Utco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,3,12, 'La Libertad De Pallan');

insert into provincia(id_departamento,id_provincia,nombre) values (6,4,'Chota');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,01, 'Chota'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,02, 'Anguia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,03, 'Chadin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,04, 'Chiguirip');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,05, 'Chimban');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,06, 'Choropampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,07, 'Cochabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,08, 'Conchan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,09, 'Huambos');  
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,10, 'Lajas'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,11, 'Llama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,12, 'Miracosta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,13, 'Paccha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,14, 'Pion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,15, 'Querocoto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,16, 'San Juan De Licupis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,17, 'Tacabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,18, 'Tocmoche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,4,19, 'Chalamarca');

insert into provincia(id_departamento,id_provincia,nombre) values (6,5,'Contumaza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,01, 'Contumaza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,02, 'Chilete');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,03, 'Cupisnique');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,04, 'Guzmango');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,05, 'San Benito');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,06, 'Santa Cruz De Toledo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,07, 'Tantarica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,5,08, 'Yonan');

insert into provincia(id_departamento,id_provincia,nombre) values (6,6,'Cutervo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,01, 'Cutervo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,02, 'Callayuc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,03, 'Choros');  
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,04, 'Cujillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,05, 'La Ramada');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,06, 'Pimpingos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,07, 'Querocotillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,08, 'San Andres De Cutervo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,09, 'San Juan De Cutervo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,10, 'San Luis De Lucma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,11, 'Santa Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,12, 'Santo Domingo De La Capilla'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,16, 'Santo Tomas'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,17, 'Socota');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,6,18, 'Toribio Casanova'); 

insert into provincia(id_departamento,id_provincia,nombre) values (6,7,'Hualgayoc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,7,01, 'Bambamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,7,02, 'Chugur');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,7,03, 'Hualgayoc'); 

insert into provincia(id_departamento,id_provincia,nombre) values (6,8,'Jaen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,01, 'Jaen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,02, 'Bellavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,03, 'Chontali'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,04, 'Colasay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,05, 'Huabal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,06, 'Las Pirias'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,07, 'Pomahuaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,08, 'Pucara'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,09, 'Sallique');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,10, 'San Felipe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,11, 'San Jose Del Alto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,8,12, 'Santa Rosa');
 

insert into provincia(id_departamento,id_provincia,nombre) values (6,9,'San Ignacio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,01, 'San Ignacio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,02, 'Chirinos'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,03, 'Huarango'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,04, 'La Coipa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,05, 'Namballe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,06, 'San Jose De Lourdes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,9,07, 'Tabaconas'); 
 
insert into provincia(id_departamento,id_provincia,nombre) values (6,10,'San Marcos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,01, 'Pedro Galvez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,02, 'Chancay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,03, 'Eduardo Villanueva');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,04, 'Gregorio Pita'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,05, 'Ichocan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,06, 'Jose Manuel Quiroz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,10,07, 'Jose Sabogal');

insert into provincia(id_departamento,id_provincia,nombre) values (6,11,'San Miguel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,01, 'San Miguel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,02, 'Bolivar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,03, 'Calquis'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,04, 'Catilluc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,05, 'El Prado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,06, 'La Florida');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,07, 'Llapa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,08, 'Nanchoc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,09, 'Niepos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,10, 'San Gregorio'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,11, 'San Silvestre De Cochan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,12, 'Tongod');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,11,13, 'Union Agua Blanca');
 
insert into provincia(id_departamento,id_provincia,nombre) values (6,12,'San Pablo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,12,01, 'San Pablo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,12,02, 'San Bernardino');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,12,03, 'San Luis'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,12,04, 'Tumbaden'); 

 
insert into provincia(id_departamento,id_provincia,nombre) values (6,13,'Santa Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,01, 'Santa Cruz'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,02, 'Andabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,03, 'Catache');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,04, 'Chancaybaños');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,05, 'La Esperanza'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,06, 'Ninabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,07, 'Pulan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,08, 'Saucepampa'); 
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,09, 'Sexi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,10, 'Uticyacu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(6,13,11, 'Yauyucan');

/*Cuzco*/
insert into departamento(id_departamento,nombre) values (7,'Cuzco');

insert into provincia(id_departamento,id_provincia,nombre) values (7,1,'Cuzco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,01, 'Cuzco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,02, 'Ccorca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,03, 'Poroy');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,04, 'San Jeronimo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,05, 'San Sebastian');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,06, 'Santiago');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,07, 'Saylla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,1,08, 'Wanchaq');

insert into provincia(id_departamento,id_provincia,nombre) values (7,2,'Acomayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,01, 'Acomayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,02, 'Acopia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,03, 'Acos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,04, 'Mosoc Llacta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,05, 'Pomacanchi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,06, 'Rondocan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,2,07, 'Sangarara');

insert into provincia(id_departamento,id_provincia,nombre) values (7,3,'Anta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,01, 'Anta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,02, 'Ancahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,03, 'Cachimayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,04, 'Chinchaypujio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,05, 'Huarocondo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,06, 'Limatambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,07, 'Mollepata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,08, 'Pucyura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,3,09, 'Zurite');

insert into provincia(id_departamento,id_provincia,nombre) values (7,4,'Calca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,01, 'Calca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,02, 'Coya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,03, 'Lamay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,04, 'Lares');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,05, 'Pisac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,06, 'San Salvador');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,07, 'Taray');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,4,08, 'Yanatile');

insert into provincia(id_departamento,id_provincia,nombre) values (7,5,'Canas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,01, 'Yanaoca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,02, 'Checca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,03, 'Kunturkanki');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,04, 'Langui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,05, 'Layo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,06, 'Pampamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,07, 'Quehue');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,5,08, 'Tupac Amaru');

insert into provincia(id_departamento,id_provincia,nombre) values (7,6,'Canchis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,01, 'Sicuani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,02, 'Checacupe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,03, 'Combapata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,04, 'Marangani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,05, 'Pitumarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,06, 'San Pablo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,07, 'San Pedro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,6,08, 'Tinta');

insert into provincia(id_departamento,id_provincia,nombre) values (7,7,'Chumbivilcas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,01, 'Santo Tomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,02, 'Capacmarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,03, 'Chamaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,04, 'Colquemarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,05, 'Livitaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,06, 'Llusco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,07, 'Quiñota');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,7,08, 'Velille');

insert into provincia(id_departamento,id_provincia,nombre) values (7,8,'Espinar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,01, 'Espinar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,02, 'Condoroma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,03, 'Coporaque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,04, 'Ocoruro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,05, 'Pallpata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,06, 'Pichigua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,07, 'Suyckutambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,8,08, 'Alto Pichigua');

insert into provincia(id_departamento,id_provincia,nombre) values (7,9,'La Convencion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,01, 'Santa Ana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,02, 'Echarate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,03, 'Huayopata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,04, 'Maranura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,05, 'Ocobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,06, 'Quellouno');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,07, 'Kimbiri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,08, 'Santa Teresa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,09, 'Vilcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,9,10, 'Pichari');

insert into provincia(id_departamento,id_provincia,nombre) values (7,10,'Paruro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,01, 'Paruro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,02, 'Accha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,03, 'Ccapi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,04, 'Colcha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,05, 'Huanoquite');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,06, 'Omacha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,07, 'Paccaritambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,08, 'Pillpinto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,10,09, 'Yaurisque');

insert into provincia(id_departamento,id_provincia,nombre) values (7,11,'Paucartambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,01, 'Paucartambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,02, 'Caicay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,03, 'Challabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,04, 'Colquepata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,05, 'Huancarani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,11,06, 'Kosñipata');

insert into provincia(id_departamento,id_provincia,nombre) values (7,12,'Quispicanchi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,01, 'Urcos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,02, 'Andahuaylillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,03, 'Camanti');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,04, 'Ccarhuayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,05, 'Ccatca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,06, 'Cusipata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,07, 'Huaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,08, 'Lucre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,09, 'Marcapata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,10, 'Ocongate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,11, 'Oropesa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,12,12, 'Quiquijana');

insert into provincia(id_departamento,id_provincia,nombre) values (7,13,'Urubamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,01, 'Urubamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,02, 'Chinchero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,03, 'Huayllabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,04, 'Machupicchu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,05, 'Maras');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,06, 'Ollantaytambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(7,13,07, 'Yucay');

/*Huancavelica*/
insert into departamento(id_departamento,nombre) values (8,'Huancavelica');

insert into provincia(id_departamento,id_provincia,nombre) values (8,1,'Huancavelica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,01, 'Huancavelica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,02, 'Acobambilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,03, 'Acoria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,04, 'Conayca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,05, 'Cuenca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,06, 'Huachocolpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,07, 'Huayllahuara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,08, 'Izcuchaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,09, 'Laria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,10, 'Manta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,11, 'Mariscal Caceres');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,12, 'Moya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,13, 'Nuevo Occoro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,14, 'Palca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,15, 'Pilchaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,16, 'Vilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,17, 'Yauli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,1,18, 'Ascension');

insert into provincia(id_departamento,id_provincia,nombre) values (8,2,'Acobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,01, 'Acobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,02, 'Andabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,03, 'Anta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,04, 'Caja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,05, 'Marcas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,06, 'Paucara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,07, 'Pomacocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,2,08, 'Rosario');

insert into provincia(id_departamento,id_provincia,nombre) values (8,3,'Angaraes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,01, 'Lircay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,02, 'Anchonga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,03, 'Callanmarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,04, 'Ccochaccasa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,05, 'Chincho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,06, 'Congalla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,07, 'Huanca-Huanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,08, 'Huayllay Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,09, 'Julcamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,10, 'San Antonio De Antaparco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,11, 'Santo Tomas De Pata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,3,12, 'Secclla');

insert into provincia(id_departamento,id_provincia,nombre) values (8,4,'Castrovirreyna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,01, 'Castrovirreyna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,02, 'Arma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,03, 'Aurahua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,04, 'Capillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,05, 'Chupamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,06, 'Cocas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,07, 'Huachos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,08, 'Huamatambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,09, 'Mollepampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,10, 'San Juan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,11, 'Santa Ana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,12, 'Tantara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,4,13, 'Ticrapo');

insert into provincia(id_departamento,id_provincia,nombre) values (8,5,'Churcampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,01, 'Churcampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,02, 'Anco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,03, 'Chinchihuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,04, 'El Carmen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,05, 'La Merced');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,06, 'Locroja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,07, 'Paucarbamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,08, 'San Miguel De Mayocc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,09, 'San Pedro De Coris');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,5,10, 'Pachamarca');

insert into provincia(id_departamento,id_provincia,nombre) values (8,6,'Huaytara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,01, 'Huaytara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,02, 'Ayavi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,03, 'Cordova');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,04, 'Huayacundo Arma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,05, 'Laramarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,06, 'Ocoyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,07, 'Pilpichaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,08, 'Querco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,09, 'Quito-Arma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,10, 'San Antonio De Cusicancha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,11, 'San Francisco De Sangayaico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,12, 'San Isidro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,13, 'Santiago De Chocorvos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,14, 'Santiago De Quirahuara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,15, 'Santo Domingo De Capillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,6,16, 'Tambo');

insert into provincia(id_departamento,id_provincia,nombre) values (8,7,'Tayacaja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,01, 'Pampas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,02, 'Acostambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,03, 'Acraquia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,04, 'Ahuaycha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,05, 'Colcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,06, 'Daniel Hernandez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,07, 'Huachocolpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,08, 'Huando');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,09, 'Huaribamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,10, 'Ñahuimpuquio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,11, 'Pazos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,13, 'Quishuar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,14, 'Salcabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,15, 'Salcahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,16, 'San Marcos De Rocchac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,17, 'Surcubamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(8,7,18, 'Tintay Puncu');

/*Huanuco*/
insert into departamento(id_departamento,nombre) values (9,'Huanuco');

insert into provincia(id_departamento,id_provincia,nombre) values (9,1,'Huanuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,01, 'Huanuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,02, 'Amarilis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,03, 'Chinchao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,04, 'Churubamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,05, 'Margos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,06, 'Quisqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,07, 'San Francisco De Cayran');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,08, 'San Pedro De Chaulan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,09, 'Santa Maria Del Valle');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,10, 'Yarumayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,1,11, 'Pillco Marca');

insert into provincia(id_departamento,id_provincia,nombre) values (9,2,'Ambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,01, 'Ambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,02, 'Cayna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,03, 'Colpas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,04, 'Conchamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,05, 'Huacar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,06, 'San Francisco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,07, 'San Rafael');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,2,08, 'Tomay Kichwa');

insert into provincia(id_departamento,id_provincia,nombre) values (9,3,'Dos De Mayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,01, 'La Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,02, 'Chuquis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,03, 'Marias');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,04, 'Pachas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,05, 'Quivilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,06, 'Ripan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,07, 'Shunqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,08, 'Sillapata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,3,09, 'Yanas');

insert into provincia(id_departamento,id_provincia,nombre) values (9,4,'Huacaybamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,4,01, 'Huacaybamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,4,02, 'Canchabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,4,03, 'Cochabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,4,04, 'Pinra');

insert into provincia(id_departamento,id_provincia,nombre) values (9,5,'Huamalies');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,01, 'Llata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,02, 'Arancay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,03, 'Chavin De Pariarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,04, 'Jacas Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,05, 'Jircan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,06, 'Miraflores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,07, 'Monzon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,08, 'Punchao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,09, 'Puños');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,10, 'Singa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,5,11, 'Tantamayo');

insert into provincia(id_departamento,id_provincia,nombre) values (9,6,'Leoncio Prado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,01, 'Rupa-Rupa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,02, 'Daniel Alomia Robles');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,03, 'Hermilio Valdizan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,04, 'Jose Crespo Y Castillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,05, 'Luyando');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,6,06, 'Mariano Damaso Beraun');

insert into provincia(id_departamento,id_provincia,nombre) values (9,7,'Marañon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,7,01, 'Huacrachuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,7,02, 'Cholon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,7,03, 'San Buenaventura');

insert into provincia(id_departamento,id_provincia,nombre) values (9,8,'Pachitea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,8,01, 'Panao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,8,02, 'Chaglla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,8,03, 'Molino');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,8,04, 'Umari');

insert into provincia(id_departamento,id_provincia,nombre) values (9,9,'Puerto Inca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,9,01, 'Puerto Inca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,9,02, 'Codo Del Pozuzo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,9,03, 'Honoria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,9,04, 'Tournavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,9,05, 'Yuyapichis');

insert into provincia(id_departamento,id_provincia,nombre) values (9,10,'Lauricocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,01, 'Jesus');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,02, 'Baños');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,03, 'Jivia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,04, 'Queropalca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,05, 'Rondos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,06, 'San Francisco De Asis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,10,07, 'San Miguel De Cauri');

insert into provincia(id_departamento,id_provincia,nombre) values (9,11,'Yarowilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,01, 'Chavinillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,02, 'Cahuac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,03, 'Chacabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,04, 'Aparicio Pomares');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,05, 'Jacas Chico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,06, 'Obas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,07, 'Pampamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(9,11,08, 'Choras');

/*Ica*/
insert into departamento(id_departamento,nombre) values (10,'Ica');

insert into provincia(id_departamento,id_provincia,nombre) values (10,1,'Ica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,01, 'Ica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,02, 'La Tinguiða');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,03, 'Los Aquijes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,04, 'Ocucaje');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,05, 'Pachacutec');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,06, 'Parcona');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,07, 'Pueblo Nuevo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,08, 'Salas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,09, 'San Jose De Los Molinos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,10, 'San Juan Bautista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,11, 'Santiago');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,12, 'Subtanjalla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,13, 'Tate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,1,14, 'Yauca Del Rosario');

insert into provincia(id_departamento,id_provincia,nombre) values (10,2,'Chincha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,01, 'Chincha Alta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,02, 'Alto Laran');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,03, 'Chavin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,04, 'Chincha Baja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,05, 'El Carmen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,06, 'Grocio Prado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,07, 'Pueblo Nuevo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,08, 'San Juan De Yanac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,09, 'San Pedro De Huacarpana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,10, 'Sunampe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,2,11, 'Tambo De Mora');

insert into provincia(id_departamento,id_provincia,nombre) values (10,3,'Nazca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,3,01, 'Nazca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,3,02, 'Changuillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,3,03, 'El Ingenio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,3,04, 'Marcona');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,3,05, 'Vista Alegre');

insert into provincia(id_departamento,id_provincia,nombre) values (10,4,'Palpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,4,01, 'Palpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,4,02, 'Llipata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,4,03, 'Rio Grande');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,4,04, 'Santa Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,4,05, 'Tibillo');

insert into provincia(id_departamento,id_provincia,nombre) values (10,5,'Pisco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,01, 'Pisco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,02, 'Huancano');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,03, 'Humay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,04, 'Independencia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,05, 'Paracas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,06, 'San Andres');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,07, 'San Clemente');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(10,5,08, 'Tupac Amaru Inca');

/*Junin*/
insert into departamento(id_departamento,nombre) values (11,'Junin');

insert into provincia(id_departamento,id_provincia,nombre) values (11,1,'Huancayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,01, 'Huancayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,02, 'Carhuacallanga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,03, 'Chacapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,04, 'Chicche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,05, 'Chilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,06, 'Chongos Alto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,07, 'Chupuro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,08, 'Colca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,09, 'Cullhuas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,10, 'El Tambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,11, 'Huacrapuquio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,12, 'Hualhuas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,13, 'Huancan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,14, 'Huasicancha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,15, 'Huayucachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,16, 'Ingenio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,17, 'Pariahuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,18, 'Pilcomayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,19, 'Pucara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,20, 'Quichuay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,21, 'Quilcas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,22, 'San Agustin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,23, 'San Jeronimo De Tunan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,24, 'Saño');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,25, 'Sapallanga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,26, 'Sicaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,27, 'Santo Domingo De Acobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,1,28, 'Viques');

insert into provincia(id_departamento,id_provincia,nombre) values (11,2,'Concepcion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,01, 'Concepcion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,02, 'Aco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,03, 'Andamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,04, 'Chambara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,05, 'Cochas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,06, 'Comas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,07, 'Heroinas Toledo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,08, 'Manzanares');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,09, 'Mariscal Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,10, 'Matahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,11, 'Mito');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,12, 'Nueve De Julio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,13, 'Orcotuna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,14, 'San Jose De Quero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,2,15, 'Santa Rosa De Ocopa');

insert into provincia(id_departamento,id_provincia,nombre) values (11,3,'Chanchamayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,01, 'Chanchamayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,02, 'Perene');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,03, 'Pichanaqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,04, 'San Luis De Shuaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,05, 'San Ramon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,3,06, 'Vitoc');

insert into provincia(id_departamento,id_provincia,nombre) values (11,4,'Jauja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,01, 'Jauja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,02, 'Acolla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,03, 'Apata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,04, 'Ataura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,05, 'Canchayllo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,06, 'Curicaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,07, 'El Mantaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,08, 'Huamali');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,09, 'Huaripampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,10, 'Huertas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,11, 'Janjaillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,12, 'Julcan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,13, 'Leonor Ordoñez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,14, 'Llocllapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,15, 'Marco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,16, 'Masma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,17, 'Masma Chicche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,18, 'Molinos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,19, 'Monobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,20, 'Muqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,21, 'Muquiyauyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,22, 'Paca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,23, 'Paccha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,24, 'Pancan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,25, 'Parco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,26, 'Pomacancha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,27, 'Ricran');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,28, 'San Lorenzo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,29, 'San Pedro De Chunan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,30, 'Sausa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,31, 'Sincos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,32, 'Tunan Marca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,33, 'Yauli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,4,34, 'Yauyos');

insert into provincia(id_departamento,id_provincia,nombre) values (11,5,'Junin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,5,01, 'Junin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,5,02, 'Carhuamayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,5,03, 'Ondores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,5,04, 'Ulcumayo');

insert into provincia(id_departamento,id_provincia,nombre) values (11,6,'Satipo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,01, 'Satipo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,02, 'Coviriali');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,03, 'Llaylla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,04, 'Mazamari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,05, 'Pampa Hermosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,06, 'Pangoa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,07, 'Rio Negro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,6,08, 'Rio Tambo');

insert into provincia(id_departamento,id_provincia,nombre) values (11,7,'Tarma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,01, 'Tarma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,02, 'Acobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,03, 'Huaricolca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,04, 'Huasahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,05, 'La Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,06, 'Palca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,07, 'Palcamayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,08, 'San Pedro De Cajas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,7,09, 'Tapo');

insert into provincia(id_departamento,id_provincia,nombre) values (11,8,'Yauli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,01, 'La Oroya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,02, 'Chacapalpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,03, 'Huay-Huay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,04, 'Marcapomacocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,05, 'Morococha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,06, 'Paccha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,07, 'Santa Barbara De Carhuacaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,08, 'Santa Rosa De Sacco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,09, 'Suitucancha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,8,10, 'Yauli');

insert into provincia(id_departamento,id_provincia,nombre) values (11,9,'Chupaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,01, 'Chupaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,02, 'Ahuac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,03, 'Chongos Bajo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,04, 'Huachac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,05, 'Huamancaca Chico');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,06, 'San Juan De Iscos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,07, 'San Juan De Jarpa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,08, 'Tres De Diciembre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(11,9,09, 'Yanacancha');

/*La Libertad*/
insert into departamento(id_departamento,nombre) values (12,'La Libertad');

insert into provincia(id_departamento,id_provincia,nombre) values (12,1,'Trujillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,01, 'Trujillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,02, 'El Porvenir');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,03, 'Florencia De Mora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,04, 'Huanchaco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,05, 'La Esperanza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,06, 'Laredo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,07, 'Moche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,08, 'Poroto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,09, 'Salaverry');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,10, 'Simbal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,1,11, 'Victor Larco Herrera');

insert into provincia(id_departamento,id_provincia,nombre) values (12,2,'Ascope');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,01, 'Ascope');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,02, 'Chicama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,03, 'Chocope');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,04, 'Magdalena De Cao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,05, 'Paijan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,06, 'Razuri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,07, 'Santiago De Cao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,2,08, 'Casa Grande');

insert into provincia(id_departamento,id_provincia,nombre) values (12,3,'Bolivar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,01, 'Bolivar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,02, 'Bambamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,03, 'Condormarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,04, 'Longotea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,05, 'Uchumarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,3,06, 'Ucuncha');

insert into provincia(id_departamento,id_provincia,nombre) values (12,4,'Chepen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,4,01, 'Chepen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,4,02, 'Pacanga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,4,03, 'Pueblo Nuevo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,4,04, 'Pacanguilla');

insert into provincia(id_departamento,id_provincia,nombre) values (12,5,'Julcan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,5,01, 'Julcan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,5,02, 'Calamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,5,03, 'Carabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,5,04, 'Huaso');

insert into provincia(id_departamento,id_provincia,nombre) values (12,6,'Otuzco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,01, 'Otuzco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,02, 'Agallpampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,03, 'Charat');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,04, 'Huaranchal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,05, 'La Cuesta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,06, 'Mache');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,07, 'Paranday');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,08, 'Salpo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,09, 'Sinsicap');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,6,10, 'Usquil');

insert into provincia(id_departamento,id_provincia,nombre) values (12,7,'Pacasmayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,7,01, 'San Pedro De Lloc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,7,02, 'Guadalupe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,7,03, 'Jequetepeque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,7,04, 'Pacasmayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,7,05, 'San Jose');

insert into provincia(id_departamento,id_provincia,nombre) values (12,8,'Pataz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,01, 'Tayabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,02, 'Buldibuyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,03, 'Chillia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,04, 'Huancaspata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,05, 'Huaylillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,06, 'Huayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,07, 'Ongon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,08, 'Parcoy');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,09, 'Pataz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,10, 'Pias');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,11, 'Santiago De Challas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,12, 'Taurija');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,8,13, 'Urpay');

insert into provincia(id_departamento,id_provincia,nombre) values (12,9,'Sanchez Carrion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,01, 'Huamachuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,02, 'Chugay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,03, 'Cochorco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,04, 'Curgos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,05, 'Marcabal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,06, 'Sanagoran');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,07, 'Sarin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,9,08, 'Sartimbamba');

insert into provincia(id_departamento,id_provincia,nombre) values (12,10,'Santiago De Chuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,01, 'Santiago De Chuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,02, 'Angasmarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,03, 'Cachicadan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,04, 'Mollebamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,05, 'Mollepata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,06, 'Quiruvilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,07, 'Santa Cruz De Chuca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,10,08, 'Sitabamba');

insert into provincia(id_departamento,id_provincia,nombre) values (12,11,'Gran Chimu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,11,01, 'Cascas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,11,02, 'Lucma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,11,03, 'Marmot');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,11,04, 'Sayapullo');

insert into provincia(id_departamento,id_provincia,nombre) values (12,12,'Viru');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,12,01, 'Viru');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,12,02, 'Chao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(12,12,03, 'Guadalupito');

/*Lambayeque*/
insert into departamento(id_departamento,nombre) values (13,'Lambayeque');

insert into provincia(id_departamento,id_provincia,nombre) values (13,1,'Chiclayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,01, 'Chiclayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,02, 'Chongoyape');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,03, 'Eten');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,04, 'Puerto Eten');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,05, 'Jose Leonardo Ortiz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,06, 'La Victoria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,07, 'Lagunas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,08, 'Monsefu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,09, 'Nueva Arica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,10, 'Oyotun');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,11, 'Picsi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,12, 'Pimentel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,13, 'Reque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,14, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,15, 'Saña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,16, 'Cayalti');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,17, 'Patapo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,18, 'Pomalca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,19, 'Pucala');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,1,20, 'Tuman');

insert into provincia(id_departamento,id_provincia,nombre) values (13,2,'Ferreñafe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,01, 'Ferreñafe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,02, 'Cañaris');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,03, 'Incahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,04, 'Manuel Antonio Mesones Muro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,05, 'Pitipo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,2,06, 'Pueblo Nuevo');

insert into provincia(id_departamento,id_provincia,nombre) values (13,3,'Lambayeque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,01, 'Lambayeque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,02, 'Chochope');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,03, 'Illimo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,04, 'Jayanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,05, 'Mochumi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,06, 'Morrope');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,07, 'Motupe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,08, 'Olmos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,09, 'Pacora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,10, 'Salas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,11, 'San Jose');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(13,3,12, 'Tucume');

/*Lima*/
insert into departamento(id_departamento,nombre) values (14,'Lima');

insert into provincia(id_departamento,id_provincia,nombre) values (14,1,'Lima (1');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,01, 'Lima');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,02, 'Ancon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,03, 'Ate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,04, 'Barranco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,05, 'Breña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,06, 'Carabayllo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,07, 'Chaclacayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,08, 'Chorrillos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,09, 'Cieneguilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,10, 'Comas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,11, 'El Agustino');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,12, 'Independencia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,13, 'Jesus Maria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,14, 'La Molina');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,15, 'La Victoria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,16, 'Lince');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,17, 'Los Olivos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,18, 'Lurigancho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,19, 'Lurin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,20, 'Magdalena Del Mar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,21, 'Magdalena Vieja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,22, 'Miraflores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,23, 'Pachacamac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,24, 'Pucusana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,25, 'Puente Piedra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,26, 'Punta Hermosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,27, 'Punta Negra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,28, 'Rimac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,29, 'San Bartolo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,30, 'San Borja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,31, 'San Isidro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,32, 'San Juan De Lurigancho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,33, 'San Juan De Miraflores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,34, 'San Luis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,35, 'San Martin De Porres');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,36, 'San Miguel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,37, 'Santa Anita');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,38, 'Santa Maria Del Mar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,39, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,40, 'Santiago De Surco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,41, 'Surquillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,42, 'Villa El Salvador');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,1,43, 'Villa Maria Del Triunfo');

insert into provincia(id_departamento,id_provincia,nombre) values (14,2,'Barranca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,2,01, 'Barranca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,2,02, 'Paramonga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,2,03, 'Pativilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,2,04, 'Supe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,2,05, 'Supe Puerto');

insert into provincia(id_departamento,id_provincia,nombre) values (14,3,'Cajatambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,3,01, 'Cajatambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,3,02, 'Copa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,3,03, 'Gorgor');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,3,04, 'Huancapon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,3,05, 'Manas');

insert into provincia(id_departamento,id_provincia,nombre) values (14,4,'Callao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,01, 'Callao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,02, 'Bellavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,03, 'Carmen De La Legua Reynoso');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,04, 'La Perla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,05, 'La Punta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,4,06, 'Ventanilla');

insert into provincia(id_departamento,id_provincia,nombre) values (14,5,'Canta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,01, 'Canta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,02, 'Arahuay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,03, 'Huamantanga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,04, 'Huaros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,05, 'Lachaqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,06, 'San Buenaventura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,5,07, 'Santa Rosa De Quives');

insert into provincia(id_departamento,id_provincia,nombre) values (14,6,'Cañete');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,01, 'San Vicente De Cañete');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,02, 'Asia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,03, 'Calango');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,04, 'Cerro Azul');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,05, 'Chilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,06, 'Coayllo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,07, 'Imperial');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,08, 'Lunahuana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,09, 'Mala');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,10, 'Nuevo Imperial');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,11, 'Pacaran');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,12, 'Quilmana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,13, 'San Antonio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,14, 'San Luis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,15, 'Santa Cruz De Flores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,6,16, 'Zuðiga');

insert into provincia(id_departamento,id_provincia,nombre) values (14,7,'Huaral');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,01, 'Huaral');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,02, 'Atavillos Alto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,03, 'Atavillos Bajo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,04, 'Aucallama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,05, 'Chancay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,06, 'Ihuari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,07, 'Lampian');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,08, 'Pacaraos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,09, 'San Miguel De Acos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,10, 'Santa Cruz De Andamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,11, 'Sumbilca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,7,12, 'Veintisiete De Noviembre');

insert into provincia(id_departamento,id_provincia,nombre) values (14,8,'Huarochiri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,01, 'Matucana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,02, 'Antioquia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,03, 'Callahuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,04, 'Carampoma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,05, 'Chicla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,06, 'Cuenca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,07, 'Huachupampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,08, 'Huanza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,09, 'Huarochiri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,10, 'Lahuaytambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,11, 'Langa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,12, 'Laraos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,13, 'Mariatana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,14, 'Ricardo Palma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,15, 'San Andres De Tupicocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,16, 'San Antonio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,17, 'San Bartolome');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,18, 'San Damian');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,19, 'San Juan De Iris');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,20, 'San Juan De Tantaranche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,21, 'San Lorenzo De Quinti');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,22, 'San Mateo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,23, 'San Mateo De Otao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,24, 'San Pedro De Casta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,25, 'San Pedro De Huancayre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,26, 'Sangallaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,27, 'Santa Cruz De Cocachacra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,28, 'Santa Eulalia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,29, 'Santiago De Anchucaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,30, 'Santiago De Tuna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,31, 'Santo Domingo De Los Ollero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,8,32, 'Surco');

insert into provincia(id_departamento,id_provincia,nombre) values (14,9,'Huaura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,01, 'Huacho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,02, 'Ambar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,03, 'Caleta De Carquin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,04, 'Checras');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,05, 'Hualmay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,06, 'Huaura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,07, 'Leoncio Prado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,08, 'Paccho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,09, 'Santa Leonor');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,10, 'Santa Maria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,11, 'Sayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,9,12, 'Vegueta');

insert into provincia(id_departamento,id_provincia,nombre) values (14,10,'Oyon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,01, 'Oyon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,02, 'Andajes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,03, 'Caujul');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,04, 'Cochamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,05, 'Navan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,10,06, 'Pachangara');

insert into provincia(id_departamento,id_provincia,nombre) values (14,11,'Yauyos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,01, 'Yauyos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,02, 'Alis');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,03, 'Ayauca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,04, 'Ayaviri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,05, 'Azangaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,06, 'Cacra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,07, 'Carania');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,08, 'Catahuasi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,09, 'Chocos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,10, 'Cochas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,11, 'Colonia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,12, 'Hongos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,13, 'Huampara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,14, 'Huancaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,15, 'Huangascar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,16, 'Huantan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,17, 'Huañec');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,18, 'Laraos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,19, 'Lincha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,20, 'Madean');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,21, 'Miraflores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,22, 'Omas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,23, 'Putinza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,24, 'Quinches');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,25, 'Quinocay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,26, 'San Joaquin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,27, 'San Pedro De Pilas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,28, 'Tanta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,29, 'Tauripampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,30, 'Tomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,31, 'Tupe');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,32, 'Viñac');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(14,11,33, 'Vitis');
------------------------------------------------------------------------------
/*Loreto*/
insert into departamento(id_departamento,nombre) values (15,'Loreto');

insert into provincia(id_departamento,id_provincia,nombre) values (15,1,'Maynas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,01, 'Iquitos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,02, 'Alto Nanay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,03, 'Fernando Lores');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,04, 'Indiana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,05, 'Las Amazonas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,06, 'Mazan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,07, 'Napo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,08, 'Punchana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,09, 'Putumayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,10, 'Torres Causana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,12, 'Belen');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,1,13, 'San Juan Bautista');

insert into provincia(id_departamento,id_provincia,nombre) values (15,2,'Alto Amazonas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,01, 'Yurimaguas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,02, 'Balsapuerto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,03, 'Barranca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,04, 'Cahuapanas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,05, 'Jeberos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,06, 'Lagunas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,07, 'Manseriche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,08, 'Morona');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,09, 'Pastaza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,10, 'Santa Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,2,11, 'Teniente Cesar Lopez Rojas');

insert into provincia(id_departamento,id_provincia,nombre) values (15,3,'Loreto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,3,01, 'Nauta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,3,02, 'Parinari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,3,03, 'Tigre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,3,04, 'Trompeteros');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,3,05, 'Urarinas');

insert into provincia(id_departamento,id_provincia,nombre) values (15,4,'Mariscal Ramon Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,4,01, 'Ramon Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,4,02, 'Pebas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,4,03, 'Yavari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,4,04, 'San Pablo');

insert into provincia(id_departamento,id_provincia,nombre) values (15,5,'Requena');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,01, 'Requena');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,02, 'Alto Tapiche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,03, 'Capelo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,04, 'Emilio San Martin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,05, 'Maquia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,06, 'Puinahua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,07, 'Saquena');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,08, 'Soplin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,09, 'Tapiche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,10, 'Jenaro Herrera');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,5,11, 'Yaquerana');

insert into provincia(id_departamento,id_provincia,nombre) values (15,6,'Ucayali');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,01, 'Contamana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,02, 'Inahuaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,03, 'Padre Marquez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,04, 'Pampa Hermosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,05, 'Sarayacu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(15,6,06, 'Vargas Guerra');
---------------------------
/*Madre de Dios*/
insert into departamento(id_departamento,nombre) values (16,'Madre de Dios');

insert into provincia(id_departamento,id_provincia,nombre) values (16,1,'Tambopata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,1,01, 'Tambopata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,1,02, 'Inambari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,1,03, 'Las Piedras');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,1,04, 'Laberinto');

insert into provincia(id_departamento,id_provincia,nombre) values (16,2,'Manu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,2,01, 'Manu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,2,02, 'Fitzcarrald');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,2,03, 'Madre De Dios');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,2,04, 'Huepetuhe');

insert into provincia(id_departamento,id_provincia,nombre) values (16,3,'Tahuamanu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,3,01, 'Iñapari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,3,02, 'Iberia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(16,3,03, 'Tahuamanu');
----------------------
/*Moquegua*/
insert into departamento(id_departamento,nombre) values (17,'Moquegua');

insert into provincia(id_departamento,id_provincia,nombre) values (17,1,'Mariscal Nieto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,01, 'Moquegua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,02, 'Carumas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,03, 'Cuchumbaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,04, 'Samegua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,05, 'San Cristobal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,1,06, 'Torata');

insert into provincia(id_departamento,id_provincia,nombre) values (17,2,'General Sanchez Cerro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,01, 'Omate');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,02, 'Chojata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,03, 'Coalaque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,04, 'Ichuña');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,05, 'La Capilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,06, 'Lloque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,07, 'Matalaque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,08, 'Puquina');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,09, 'Quinistaquillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,10, 'Ubinas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,2,11, 'Yunga');

insert into provincia(id_departamento,id_provincia,nombre) values (17,3,'Ilo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,3,01, 'Ilo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,3,02, 'El Algarrobal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(17,3,03, 'Pacocha');
----------------
/*Pasco*/
insert into departamento(id_departamento,nombre) values (18,'Pasco');

insert into provincia(id_departamento,id_provincia,nombre) values (18,1,'Pasco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,01, 'Chaupimarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,02, 'Huachon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,03, 'Huariaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,04, 'Huayllay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,05, 'Ninacaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,06, 'Pallanchacra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,07, 'Paucartambo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,08, 'San Fco.De Asis De Yarusyacan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,09, 'Simon Bolivar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,10, 'Ticlacayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,11, 'Tinyahuarco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,12, 'Vicco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,1,13, 'Yanacancha');

insert into provincia(id_departamento,id_provincia,nombre) values (18,2,'Daniel Alcides Carrion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,01, 'Yanahuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,02, 'Chacayan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,03, 'Goyllarisquizga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,04, 'Paucar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,05, 'San Pedro De Pillao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,06, 'Santa Ana De Tusi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,07, 'Tapuc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,2,08, 'Vilcabamba');

insert into provincia(id_departamento,id_provincia,nombre) values (18,3,'Oxapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,01, 'Oxapampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,02, 'Chontabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,03, 'Huancabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,04, 'Palcazu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,05, 'Pozuzo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,06, 'Puerto Bermudez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(18,3,07, 'Villa Rica');
--------------------
/*Piura*/
insert into departamento(id_departamento,nombre) values (19,'Piura');

insert into provincia(id_departamento,id_provincia,nombre) values (19,1,'Piura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,01, 'Piura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,04, 'Castilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,05, 'Catacaos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,07, 'Cura Mori');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,08, 'El Tallan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,09, 'La Arena');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,10, 'La Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,11, 'Las Lomas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,1,14, 'Tambo Grande');

insert into provincia(id_departamento,id_provincia,nombre) values (19,2,'Ayabaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,01, 'Ayabaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,02, 'Frias');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,03, 'Jilili');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,04, 'Lagunas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,05, 'Montero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,06, 'Pacaipampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,07, 'Paimas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,08, 'Sapillica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,09, 'Sicchez');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,2,10, 'Suyo');

insert into provincia(id_departamento,id_provincia,nombre) values (19,3,'Huancabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,01, 'Huancabamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,02, 'Canchaque');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,03, 'El Carmen De La Frontera');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,04, 'Huarmaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,05, 'Lalaquiz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,06, 'San Miguel De El Faique');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,07, 'Sondor');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,3,08, 'Sondorillo');

insert into provincia(id_departamento,id_provincia,nombre) values (19,4,'Morropon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,01, 'Chulucanas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,02, 'Buenos Aires');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,03, 'Chalaco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,04, 'La Matanza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,05, 'Morropon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,06, 'Salitral');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,07, 'San Juan De Bigote');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,08, 'Santa Catalina De Mossa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,09, 'Santo Domingo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,4,10, 'Yamango');

insert into provincia(id_departamento,id_provincia,nombre) values (19,5,'Paita');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,01, 'Paita');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,02, 'Amotape');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,03, 'Arenal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,04, 'Colan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,05, 'La Huaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,06, 'Tamarindo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,5,07, 'Vichayal');

insert into provincia(id_departamento,id_provincia,nombre) values (19,6,'Sullana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,01, 'Sullana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,02, 'Bellavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,03, 'Ignacio Escudero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,04, 'Lancones');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,05, 'Marcavelica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,06, 'Miguel Checa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,07, 'Querecotillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,6,08, 'Salitral');

insert into provincia(id_departamento,id_provincia,nombre) values (19,7,'Talara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,01, 'Pariñas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,02, 'El Alto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,03, 'La Brea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,04, 'Lobitos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,05, 'Los Organos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,7,06, 'Mancora');

insert into provincia(id_departamento,id_provincia,nombre) values (19,8,'Sechura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,01, 'Sechura');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,02, 'Bellavista De La Union');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,03, 'Bernal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,04, 'Cristo Nos Valga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,05, 'Vice');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(19,8,06, 'Rinconada Llicuar');
-------------
/*Puno*/
insert into departamento(id_departamento,nombre) values (20,'Puno');

insert into provincia(id_departamento,id_provincia,nombre) values (20,1,'Puno');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,01, 'Puno');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,02, 'Acora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,03, 'Amantani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,04, 'Atuncolla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,05, 'Capachica');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,06, 'Chucuito');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,07, 'Coata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,08, 'Huata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,09, 'Mañazo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,10, 'Paucarcolla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,11, 'Pichacani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,12, 'Plateria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,13, 'San Antonio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,14, 'Tiquillaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,1,15, 'Vilque');

insert into provincia(id_departamento,id_provincia,nombre) values (20,2,'Azangaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,01, 'Azangaro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,02, 'Achaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,03, 'Arapa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,04, 'Asillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,05, 'Caminaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,06, 'Chupa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,07, 'Jose Domingo Choquehuanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,08, 'Muñani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,09, 'Potoni');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,10, 'Saman');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,11, 'San Anton');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,12, 'San Jose');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,13, 'San Juan De Salinas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,14, 'Santiago De Pupuja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,2,15, 'Tirapata');

insert into provincia(id_departamento,id_provincia,nombre) values (20,3,'Carabaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,01, 'Macusani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,02, 'Ajoyani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,03, 'Ayapata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,04, 'Coasa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,05, 'Corani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,06, 'Crucero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,07, 'Ituata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,08, 'Ollachea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,09, 'San Gaban');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,3,10, 'Usicayos');

insert into provincia(id_departamento,id_provincia,nombre) values (20,4,'Chucuito');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,01, 'Juli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,02, 'Desaguadero');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,03, 'Huacullani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,04, 'Kelluyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,05, 'Pisacoma');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,06, 'Pomata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,4,07, 'Zepita');

insert into provincia(id_departamento,id_provincia,nombre) values (20,5,'El Collao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,5,01, 'Ilave');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,5,02, 'Capazo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,5,03, 'Pilcuyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,5,04, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,5,05, 'Conduriri');

insert into provincia(id_departamento,id_provincia,nombre) values (20,6,'Huancane');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,01, 'Huancane');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,02, 'Cojata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,03, 'Huatasani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,04, 'Inchupalla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,05, 'Pusi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,06, 'Rosaspata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,07, 'Taraco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,6,08, 'Vilque Chico');

insert into provincia(id_departamento,id_provincia,nombre) values (20,7,'Lampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,01, 'Lampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,02, 'Cabanilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,03, 'Calapuja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,04, 'Nicasio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,05, 'Ocuviri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,06, 'Palca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,07, 'Paratia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,08, 'Pucara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,09, 'Santa Lucia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,7,10, 'Vilavila');

insert into provincia(id_departamento,id_provincia,nombre) values (20,8,'Melgar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,01, 'Ayaviri');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,02, 'Antauta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,03, 'Cupi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,04, 'Llalli');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,05, 'Macari');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,06, 'Nuñoa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,07, 'Orurillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,08, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,8,09, 'Umachiri');

insert into provincia(id_departamento,id_provincia,nombre) values (20,9,'Moho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,9,01, 'Moho');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,9,02, 'Conima');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,9,03, 'Huayrapata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,9,04, 'Tilali');

insert into provincia(id_departamento,id_provincia,nombre) values (20,10,'San Antonio De Putina');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,10,01, 'Putina');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,10,02, 'Ananea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,10,03, 'Pedro Vilca Apaza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,10,04, 'Quilcapuncu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,10,05, 'Sina');

insert into provincia(id_departamento,id_provincia,nombre) values (20,11,'San Roman');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,11,01, 'Juliaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,11,02, 'Cabana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,11,03, 'Cabanillas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,11,04, 'Caracoto');

insert into provincia(id_departamento,id_provincia,nombre) values (20,12,'Sandia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,01, 'Sandia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,02, 'Cuyocuyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,03, 'Limbani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,04, 'Patambuco');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,05, 'Phara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,06, 'Quiaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,07, 'San Juan Del Oro');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,08, 'Yanahuaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,12,09, 'Alto Inambari');

insert into provincia(id_departamento,id_provincia,nombre) values (20,13,'Yunguyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,01, 'Yunguyo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,02, 'Anapia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,03, 'Copani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,04, 'Cuturapi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,05, 'Ollaraya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,06, 'Tinicachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(20,13,07, 'Unicachi');
-----------------------
/*San Martin*/
insert into departamento(id_departamento,nombre) values (21,'San Martin');

insert into provincia(id_departamento,id_provincia,nombre) values (21,1,'Moyobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,01, 'Moyobamba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,02, 'Calzada');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,03, 'Habana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,04, 'Jepelacio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,05, 'Soritor');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,1,06, 'Yantalo');

insert into provincia(id_departamento,id_provincia,nombre) values (21,2,'Bellavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,01, 'Bellavista');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,02, 'Alto Biavo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,03, 'Bajo Biavo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,04, 'Huallaga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,05, 'San Pablo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,2,06, 'San Rafael');

insert into provincia(id_departamento,id_provincia,nombre) values (21,3,'El Dorado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,3,01, 'San Jose De Sisa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,3,02, 'Agua Blanca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,3,03, 'San Martin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,3,04, 'Santa Rosa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,3,05, 'Shatoja');

insert into provincia(id_departamento,id_provincia,nombre) values (21,4,'Huallaga');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,01, 'Saposoa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,02, 'Alto Saposoa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,03, 'El Eslabon');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,04, 'Piscoyacu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,05, 'Sacanche');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,4,06, 'Tingo De Saposoa');

insert into provincia(id_departamento,id_provincia,nombre) values (21,5,'Lamas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,01, 'Lamas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,02, 'Alonso De Alvarado');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,03, 'Barranquita');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,04, 'Caynarachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,05, 'Cuñumbuqui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,06, 'Pinto Recodo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,07, 'Rumisapa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,08, 'San Roque De Cumbaza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,09, 'Shanao');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,10, 'Tabalosos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,5,11, 'Zapatero');

insert into provincia(id_departamento,id_provincia,nombre) values (21,6,'Mariscal Caceres');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,6,01, 'Juanjui');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,6,02, 'Campanilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,6,03, 'Huicungo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,6,04, 'Pachiza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,6,05, 'Pajarillo');

insert into provincia(id_departamento,id_provincia,nombre) values (21,7,'Picota');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,01, 'Picota');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,02, 'Buenos Aires');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,03, 'Caspisapa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,04, 'Pilluana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,05, 'Pucacaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,06, 'San Cristobal');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,07, 'San Hilarion');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,08, 'Shamboyacu');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,09, 'Tingo De Ponasa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,7,10, 'Tres Unidos');

insert into provincia(id_departamento,id_provincia,nombre) values (21,8,'Rioja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,01, 'Rioja');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,02, 'Awajun');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,03, 'Elias Soplin Vargas');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,04, 'Nueva Cajamarca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,05, 'Pardo Miguel');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,06, 'Posic');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,07, 'San Fernando');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,08, 'Yorongos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,8,09, 'Yuracyacu');

insert into provincia(id_departamento,id_provincia,nombre) values (21,9,'San Martin');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,01, 'Tarapoto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,02, 'Alberto Leveau');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,03, 'Cacatachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,04, 'Chazuta');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,05, 'Chipurana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,06, 'El Porvenir');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,07, 'Huimbayoc');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,08, 'Juan Guerra');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,09, 'La Banda De Shilcayo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,10, 'Morales');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,11, 'Papaplaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,12, 'San Antonio');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,13, 'Sauce');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,9,14, 'Shapaja');

insert into provincia(id_departamento,id_provincia,nombre) values (21,10,'Tocache');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,10,01, 'Tocache');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,10,02, 'Nuevo Progreso');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,10,03, 'Polvora');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,10,04, 'Shunte');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(21,10,05, 'Uchiza');
--------------------------
/*Taca*/
insert into departamento(id_departamento,nombre) values (22,'Taca');

insert into provincia(id_departamento,id_provincia,nombre) values (22,1,'Tacna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,01, 'Tacna');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,02, 'Alto De La Alianza');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,03, 'Calana');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,04, 'Ciudad Nueva');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,05, 'Inclan');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,06, 'Pachia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,07, 'Palca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,08, 'Pocollay');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,09, 'Sama');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,1,10, 'Coronel Gregorio Albarracín Lanchipa');

insert into provincia(id_departamento,id_provincia,nombre) values (22,2,'Candarave');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,01, 'Candarave');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,02, 'Cairani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,03, 'Camilaca');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,04, 'Curibaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,05, 'Huanuara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,2,06, 'Quilahuani');

insert into provincia(id_departamento,id_provincia,nombre) values (22,3,'Jorge Basadre');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,3,01, 'Locumba');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,3,02, 'Ilabaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,3,03, 'Ite');

insert into provincia(id_departamento,id_provincia,nombre) values (22,4,'Tarata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,01, 'Tarata');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,02, 'Chucatamani');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,03, 'Estique');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,04, 'Estique-Pampa');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,05, 'Sitajara');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,06, 'Susapaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,07, 'Tarucachi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(22,4,08, 'Ticaco');
--------------
/*Tumbes*/
insert into departamento(id_departamento,nombre) values (23,'Tumbes');

insert into provincia(id_departamento,id_provincia,nombre) values (23,1,'Tumbes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,01, 'Tumbes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,02, 'Corrales');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,03, 'La Cruz');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,04, 'Pampas De Hospital');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,05, 'San Jacinto');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,1,06, 'San Juan De La Virgen');

insert into provincia(id_departamento,id_provincia,nombre) values (23,2,'Contralmirante Villar');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,2,01, 'Zorritos');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,2,02, 'Casitas');

insert into provincia(id_departamento,id_provincia,nombre) values (23,3,'Zarumilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,3,01, 'Zarumilla');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,3,02, 'Aguas Verdes');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,3,03, 'Matapalo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(23,3,04, 'Papayal');
----------------
/*Ucayali*/
insert into departamento(id_departamento,nombre) values (24,'Ucayali');

insert into provincia(id_departamento,id_provincia,nombre) values (24,1,'Coronel Portillo');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,01, 'Calleria');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,02, 'Campoverde');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,03, 'Iparia');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,04, 'Masisea');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,05, 'Yarinacocha');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,1,06, 'Nueva Requena');

insert into provincia(id_departamento,id_provincia,nombre) values (24,2,'Atalaya');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,2,01, 'Raymondi');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,2,02, 'Sepahua');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,2,03, 'Tahuania');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,2,04, 'Yurua');

insert into provincia(id_departamento,id_provincia,nombre) values (24,3,'Padre Abad');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,3,01, 'Padre Abad');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,3,02, 'Irazola');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,3,03, 'Curimana');

insert into provincia(id_departamento,id_provincia,nombre) values (24,4,'Purus');
insert into distrito(id_departamento,id_provincia,id_distrito,nombre) values(24,4,01, 'Puru');



