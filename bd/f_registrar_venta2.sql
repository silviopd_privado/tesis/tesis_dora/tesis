﻿select * from f_registrar_venta('01','46705013',2,'2',10,'[{"id_producto":1,"cantidad":2},{"id_producto":2,"cantidad":4}]')

-- DROP FUNCTION public.f_registrar_venta(character, character varying, character, integer, json);

CREATE OR REPLACE FUNCTION public.f_registrar_venta(
    IN p_tipo_comprobante character,
    IN p_id_cliente character varying,
    IN p_cliente_direccion integer,
    IN p_forma_pago character,
    IN p_numero_cuota integer,
    IN p_detalle_venta json)
  RETURNS TABLE(nv integer) AS
$BODY$
	declare
		v_numero_venta integer;
		v_numero_serie integer;
		v_numero_documento integer;

		v_detalle_venta_cursor refcursor;
		v_detalle_venta_registro record;

		v_precio_producto integer;
		
		v_cuotas1 integer;
		contador integer;
		v_fecha date;
		v_monto numeric;
		dia int;
		fecha date;		
		
		v_porcentaje_igv numeric;
		v_credito integer;
		v_saldoactual numeric;
		v_saldofavor numeric;
		v_estado char(2);
		v_saldo numeric;

		v_importe_bruto numeric;
		v_descuento1 numeric;
		v_descuento2 numeric;
		v_importe numeric;

		v_item integer;

		v_sub_total numeric;
		v_igv numeric;
		v_total numeric;
		v_total2 numeric;

		v_stock_actual integer;

		v_detalle_serie_comprobante_registro record;

	begin
		begin
	
			--Inicio: generar el nuevo numero de venta
			select numero + 1 into v_numero_venta 
			from correlativo where tabla = 'venta';

			RAISE NOTICE 'Nuevo número de venta: %', v_numero_venta;
			--Fin: generar el nuevo numero de venta

						
			--Inicio: obtener el porcentaje de IGV			
			select 
				valor::numeric(7,2) into v_porcentaje_igv 
			from 
				configuracion 
			where 
				codigo = 1;
			
			RAISE NOTICE 'Porcentaje de IGV: %', v_porcentaje_igv;
			--Fin: obtener el porcentaje de IGV


			--Inicio: obtener numero_serie
			FOR v_detalle_serie_comprobante_registro IN select * from serie_comprobante where id_tipo_comprobante like p_tipo_comprobante order by 2 
			LOOP
				if v_detalle_serie_comprobante_registro.numero_documento < v_detalle_serie_comprobante_registro.ultimo_numero then

					v_numero_documento = v_detalle_serie_comprobante_registro.numero_documento;
					v_numero_serie = v_detalle_serie_comprobante_registro.numero_serie;
					exit;
				end if;
			END LOOP;
			--Inicio: obtener numero_serie

			
			--Inicio: Insertar en la tabla venta
			INSERT INTO public.venta(
						numero_venta, 
						id_tipo_comprobante, 
						numero_serie, 
						numero_docuemento, 
						id_cliente, 
						porcentaje_igv, 
						sub_total, 
						igv, 
						total, 
						id_forma_pago, 
						numero_cuota,
						direccion_cliente
						)
					VALUES (
						v_numero_venta,
						p_tipo_comprobante,
						v_numero_serie,
						v_numero_documento,
						p_id_cliente,
						v_porcentaje_igv,
						0,
						0,
						0,
						p_forma_pago,
						p_numero_cuota,
						p_cliente_direccion
					);
			--Fin: Insertar en la tabla venta



			--Inicio: Insertar en venta_detalle
			open v_detalle_venta_cursor for
				select
					id_producto,
					cantidad
				from
					json_populate_recordset
					(
						null:: venta_detalle,
						p_detalle_venta
					);

			v_item = 0; --inicializando la variable en cero
			v_total = 0; --inicializando la variable en cero

			loop --Bucle para recorrer todos los registros almacenados en la vaiable: v_detalle_venta_cursor
				fetch v_detalle_venta_cursor into v_detalle_venta_registro; --captura registro por registro
				if found then --Si aun encuentra registros
					
					
					if v_detalle_venta_registro.cantidad <= 0 then
						RAISE EXCEPTION 'La cantidad del artículo con código (%) debe ser mayor que cero', v_detalle_venta_registro.codigo_articulo;
					end if;

					select precio into v_precio_producto from producto where id_producto = v_detalle_venta_registro.id_producto;
					
	
					v_importe_bruto = v_detalle_venta_registro.cantidad  * v_precio_producto;
					v_importe = v_importe_bruto;

					v_total = v_total + v_importe;

					v_item = v_item + 1;

					INSERT INTO public.venta_detalle(
							numero_venta, 
							item, 
							id_producto, 
							cantidad, 
							precio)
					VALUES (
							v_numero_venta,
							v_item, 
							v_detalle_venta_registro.id_producto, 
							v_detalle_venta_registro.cantidad, 
							v_precio_producto);

				else
					exit; --salir del bucle
				end if;
			end loop;
			--Fin: Insertar en venta_detalle

			--Inicio: Calcular y actualizar el sub total y el igv
			v_igv = v_total*(v_porcentaje_igv/100);			
			v_sub_total = v_total;
			v_total = v_sub_total+v_igv;
			v_saldofavor = v_total;

			
			update 
				venta
			set
				sub_total = v_sub_total,
				igv = v_igv,
				total = v_total
			where
				numero_venta = v_numero_venta;
			--Fin: Calcular y actualizar el sub total y el igv


			--Inicio: Actualizar los correlativos
			update correlativo set numero = v_numero_venta where tabla = 'venta';		
			--Fin: Actualizar los correlativos


			--Inicio: Actualizar serie_comprobante
			update serie_comprobante set numero_documento = v_numero_documento+1 where id_tipo_comprobante = p_tipo_comprobante and numero_serie = v_numero_serie;		
			--Fin: Actualizar serie_comprobante


			--Inicio: Ingresar Pago Mensual
			
			if p_numero_cuota > 0 then 
			
				v_cuotas1=1;
				contador=1;
				
				select fecha_registro into v_fecha from venta where numero_venta = v_numero_venta;
				select total into v_total2 from venta where numero_venta = v_numero_venta;

				v_monto=(v_total2/p_numero_cuota);

				LOOP
    
					IF v_cuotas1!=p_numero_cuota+1 THEN
					
					select date_part('dow',(v_fecha+CONCAT(v_cuotas1,' month')::interval)::date) into dia;
				
					select (v_fecha+CONCAT(v_cuotas1,' month')::interval)::date into fecha;
				
					--validar que no caiga domingo=0

					if(dia=0) then
					select (fecha-'2 day'::interval)::date into fecha;
					end if;


					INSERT INTO pago_mensual(numero_letra,numero_venta,fecha, monto)
					VALUES (contador,v_numero_venta, fecha,v_monto);

					v_cuotas1=v_cuotas1+1;
					contador=contador+1;
				
					Else
					EXIT;  
					
					END IF;
					
				END LOOP;
			
			end if;
			--Fin: Ingresar Pago Mensual


		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
		end;

		--return 1; --Cuando la transacción terminó con exito
		return query select v_numero_venta;
	end;
$BODY$
  LANGUAGE plpgsql VOLATILE