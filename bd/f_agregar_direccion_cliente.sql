﻿select * from f_agregar_direccion_cliente('111111111','234234','234234','234234','1','1','1')

CREATE OR REPLACE FUNCTION public.f_agregar_direccion_cliente(
    IN p_id_cliente character varying,
    IN p_direccion character varying,
    IN p_telefono character varying,
    IN p_celular character varying,
    IN p_id_departamento character varying,
    IN p_id_provincia character varying,
    IN p_id_distrito character varying)
  RETURNS TABLE(cliente character varying, direccion character varying) AS
$BODY$
	DECLARE
		
		v_cliente varchar;
		v_id_direccion integer;
		
	begin	
		begin
			select id_cliente into v_cliente from cliente where id_cliente @@ p_id_cliente;

			if v_cliente is null then

				RAISE EXCEPTION 'El cliente con (dni o ruc) (%) no existe ', p_id_cliente;
			else
				select id_direccion  into v_id_direccion from direccion where id_cliente @@ p_id_cliente order by 1 desc limit 1;

				INSERT INTO public.direccion(id_cliente, id_direccion, direccion, telefono, celular, id_departamento,id_provincia, id_distrito)
				VALUES (p_id_cliente, v_id_direccion+1, p_direccion, p_telefono, p_celular, p_id_departamento, p_id_provincia, p_id_distrito);			
			
			end if;
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_id_cliente,p_direccion;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE