﻿select f_agregar_cliente('46705014','silvio peña','las viñas',
			'546456','4670504','dsfdsf','123','1','1','1')

CREATE OR REPLACE FUNCTION public.f_agregar_cliente(
    IN p_id_cliente character varying,
    IN p_nombre_razonsocial character varying,
    IN p_direccion character varying,
    IN p_telefono character varying,
    IN p_celular character varying,
    IN p_email character varying,
    IN p_clave character varying,
    IN p_id_departamento character varying,
    IN p_id_provincia character varying,
    IN p_id_distrito character varying)
  RETURNS TABLE(cliente character varying, nombre character varying) AS
$BODY$
	DECLARE

		v_productoid integer;
		v_codigoproducto varchar;
		
	begin	
		begin

			INSERT INTO public.cliente(
				    id_cliente, nombre_razonsocial, direccion, telefono, celular, 
				    email, clave, id_departamento, id_provincia, id_distrito)
			    VALUES (p_id_cliente, p_nombre_razonsocial, p_direccion, p_telefono, p_celular, 
				    p_email, md5(p_clave), p_id_departamento, p_id_provincia, p_id_distrito);
			
		EXCEPTION
			when others then --Cuando ocurra algun error, muestra un mensaje
				RAISE EXCEPTION '%', SQLERRM;
			
		end;

		return query select p_id_cliente,p_nombre_razonsocial;
	end;
	
$BODY$
  LANGUAGE plpgsql VOLATILE
  