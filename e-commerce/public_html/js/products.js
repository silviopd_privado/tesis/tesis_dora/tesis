var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

$(document).ready(function () {

    /*Categoria*/
    $(".tab1 .single-bottom").hide();
    $(".tab2 .single-bottom").hide();
    $(".tab3 .single-bottom").hide();
    $(".tab4 .single-bottom").hide();
    $(".tab5 .single-bottom").hide();

    $(".tab1 ul").click(function () {
        $(".tab1 .single-bottom").slideToggle(300);
        $(".tab2 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
    })
    $(".tab2 ul").click(function () {
        $(".tab2 .single-bottom").slideToggle(300);
        $(".tab1 .single-bottom").hide();
        $(".tab3 .single-bottom").hide();
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
    })
    $(".tab3 ul").click(function () {
        $(".tab3 .single-bottom").slideToggle(300);
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
    $(".tab4 ul").click(function () {
        $(".tab3 .single-bottom").slideToggle(300);
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
    $(".tab5 ul").click(function () {
        $(".tab3 .single-bottom").slideToggle(300);
        $(".tab4 .single-bottom").hide();
        $(".tab5 .single-bottom").hide();
        $(".tab2 .single-bottom").hide();
        $(".tab1 .single-bottom").hide();
    })
    /*Categoria*/

    listarProductos();
    listarMarca();
    listarMarcaCategoria();
    listarMarcaDerecha();
});

function listarProductos() {

    var ruta = DIRECCION_WS + "producto.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {

                html += '<div class="col-md-4 grid-stn simpleCart_shelfItem">';
                html += '<div class="ih-item square effect3 bottom_to_top">';
                html += '<div class="bottom-2-top">';
                html += '<div class="img"><img src="images/grid4.jpg" alt="/" class="img-responsive gri-wid"></div>';
                html += '<div class="info">';
                html += '<div class="pull-left styl-hdn">';
                html += '<h3>' + item.nombre + '</h3>';
                html += '</div>';
                html += '<div class="pull-right styl-price">';
                html += '<p><a  href="#" class="item_add"><span class="glyphicon glyphicon-shopping-cart grid-cart" aria-hidden="true"></span> <span class=" item_price">$' + item.precio + '</span></a></p>';
                html += '</div>';
                html += '<div class="clearfix"></div>';
                html += '</div></div>';
                html += '</div>';
                html += '<div class="quick-view">';
                html += '<a href="single.html">Quick view</a>';
                html += '</div>';
                html += '</div>';

            });

            $("#producto-lista").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function listarMarcaDerecha() {
    var ruta = DIRECCION_WS + "marca.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<label class="checkbox"><input type="checkbox" name="checkbox" onclick="categoriaDerecha(' + item.id_marca + ')"><i></i>' + item.nombre + '</label>';
            });

            $("#marca-lista-derecha").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function listarMarca() {
    var ruta = DIRECCION_WS + "marca.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<li><a href="#">' + item.nombre + '</a></li>';
            });

            $("#lista-marca").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function listarMarcaCategoria() {
    var ruta = DIRECCION_WS + "marca.listar.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<li class="dropdown">';
                html += '<a href="#" class="dropdown-toggle" data-toggle="dropdown" onclick="categoria(' + item.id_marca+')">' + item.nombre + '<b class="caret"></b></a>';
                html += '<ul class="dropdown-menu multi-column columns-2" >';
                html += '<div class="row" >';
                html += '<div class="col-sm-6">';
                html += '<ul class="multi-column-dropdown" id="lista-categoria-' + item.id_marca + '">';
                html += '</ul>';
                html += '</div>';
                html += '<div class="col-sm-6">';
                html += '<a href="#"><img src="images/menu3.jpg" alt="/" class="img-rsponsive men-img-wid" /></a>';
                html += '</div>';
                html += '</div>';
                html += '</ul>';
                html += '</li>';
            });

            $("#lista-marca-categoria").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function categoria(id_marca) {
    var ruta = DIRECCION_WS + "categoria.listar.php";

    $.post(ruta, {id_marca: id_marca}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";

            $.each(datosJSON.datos, function (i, item) {
                html += '<li><a href="products.html">' + item.nombre + '</a></li>';
            });

            $("#lista-categoria-" + id_marca).html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function categoriaDerecha(id_marca, nombre) {
    var ruta = DIRECCION_WS + "categoria.listar.php";

    $.post(ruta, {id_marca: id_marca}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {

            var html = "";
            html += '<h4><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>'+nombre+'</h4>';
            html += '<div class="row row1 scroll-pane">';
            html += '<div class="col col-4">';

            $.each(datosJSON.datos, function (i, item) {
                html += '<label class="checkbox"><input type="checkbox" name="checkbox"><i></i>' + item.nombre + '</label>';
            });

            html += '</div>';
            html += '</div>';
            $("#lado-derecho").html(html);

        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}