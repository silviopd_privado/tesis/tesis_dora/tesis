//var ruta = "https://webservicespropio.herokuapp.com/webservice/";
var DIRECCION_WS = "http://localhost:8080/tesis/e-commerce-ws/webservice/";

function cargarComboDepartamento(p_nombreCombo, p_tipo) {
    var ruta = DIRECCION_WS + "departamento.listar.cliente.php";

    $.post(ruta, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un departamento</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_departamento + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboProvincia(p_nombreCombo, p_tipo, id_departamento) {
    var ruta = DIRECCION_WS + "provincia.listar.cliente.php";

    $.post(ruta, {id_departamento: id_departamento}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione una provincia</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_provincia + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}

function cargarComboDistrito(p_nombreCombo, p_tipo, id_departamento, id_provincia) {
    var ruta = DIRECCION_WS + "distrito.listar.cliente.php";

    $.post(ruta, {id_departamento: id_departamento, id_provincia: id_provincia}, function () {
    }).done(function (resultado) {
        var datosJSON = resultado;
        if (datosJSON.estado === 200) {
            var html = "";
            if (p_tipo === "seleccione") {
                html += '<option value="">Seleccione un distrito</option>';
            }

            $.each(datosJSON.datos, function (i, item) {
                html += '<option value="' + item.id_distrito + '">' + item.nombre + '</option>';
            });

            $(p_nombreCombo).html(html);
        } else {
            swal("Mensaje del sistema", resultado, "warning");
        }
    }).fail(function (error) {
        var datosJSON = $.parseJSON(error.responseText);
        swal("Error", datosJSON.mensaje, "error");
    })
}
