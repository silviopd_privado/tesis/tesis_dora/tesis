<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Cargo.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cargo"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_cargo = $_POST["id_cargo"];

try {
    if (validarToken($token)) {
        
        $obj = new Cargo();
        $resultado = $obj->eliminar($id_cargo);

        Funciones::imprimeJSON(200, "Se Elimino Correctamente", "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}