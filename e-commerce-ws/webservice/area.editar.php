<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Area.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_area"]) ||  !isset($_POST["nombre"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_area"];
$nombre = $_POST["nombre"];

try {
    if (validarToken($token)) {

        $obj = new Area();
        $obj->setId_area($id_marca);
        $obj->setNombre($nombre);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}