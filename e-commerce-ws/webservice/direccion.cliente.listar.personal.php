<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Direccion.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Direccion();
        $resultado = $obj->listarDireccionClientesPersonal();

        $listaProductos = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_cliente" => $resultado[$i]["id_cliente"],
                "nombre_razonsocial" => $resultado[$i]["nombre_razonsocial"],
                "id_direccion" => $resultado[$i]["id_direccion"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "celular" => $resultado[$i]["celular"],
                "distrito" => $resultado[$i]["distrito"],
                "provincia" => $resultado[$i]["provincia"],
                "departamento" => $resultado[$i]["departamento"]
            );

            $listaProductos[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaProductos);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}