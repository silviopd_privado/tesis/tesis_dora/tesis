<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Personal();
        $resultado = $obj->listarPersonal();

        $listaProductos = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "dni_personal" => $resultado[$i]["dni_personal"],
                "nombre" => $resultado[$i]["nombre"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "email" => $resultado[$i]["email"],
                "departamento" => $resultado[$i]["departamento"],
                "provincia" => $resultado[$i]["provincia"],
                "distrito" => $resultado[$i]["distrito"],
                "area" => $resultado[$i]["area"],
                "cargo" => $resultado[$i]["cargo"],
                "estado" => $resultado[$i]["estado"]
            );

            $listaProductos[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaProductos);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}