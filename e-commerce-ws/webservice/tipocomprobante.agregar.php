<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/TipoComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_tipo_comprobante"]) || !isset($_POST["descripcion"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$p_id_tipo_comprobante = $_POST["id_tipo_comprobante"];
$p_descripcion = $_POST["descripcion"];

try {
    if (validarToken($token)) {

        $obj = new TipoComprobante();
        $obj->setId_tipo_comprobante($p_id_tipo_comprobante);
        $obj->setDescripcion($p_descripcion);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
