<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Cliente.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Cliente();
        $resultado = $obj->listarClientesPersonal();

        $listaProductos = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_cliente" => $resultado[$i]["id_cliente"],
                "nombre_razonsocial" => $resultado[$i]["nombre_razonsocial"],
                "email" => $resultado[$i]["email"],
                "estado" => $resultado[$i]["estado"]
            );

            $listaProductos[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaProductos);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}