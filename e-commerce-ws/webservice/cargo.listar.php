<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Cargo.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Cargo();
        $resultado = $obj->cargarCargo();

        $listaDepartamento = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_cargo" => $resultado[$i]["id_cargo"],
                "nombre" => $resultado[$i]["nombre"]
            );

            $listaDepartamento[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaDepartamento);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}