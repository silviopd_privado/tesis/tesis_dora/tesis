<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Distrito.clase.php';
require_once '../util/funciones/Funciones.clase.php';

if (!isset($_POST["id_departamento"]) || !isset($_POST["id_provincia"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$p_marca = $_POST["id_departamento"];
$p_provincia = $_POST["id_provincia"];

try {
    $obj = new Distrito();
    $resultado = $obj->cargarDistrito($p_marca, $p_provincia);

    $listaDistrito = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_departamento" => $resultado[$i]["id_departamento"],
            "id_provincia" => $resultado[$i]["id_provincia"],
            "id_distrito" => $resultado[$i]["id_distrito"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaDistrito[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaDistrito);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}