<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';


try {

    $obj = new Producto();
    $resultado = $obj->listarProductosTotales();

    $listaProductos = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
           "id_producto" => $resultado[$i]["id_producto"],
                "nombre" => $resultado[$i]["nombre"],
                "descripcion" => $resultado[$i]["descripcion"],
                "precio" => $resultado[$i]["precio"],
                "cantidad" => $resultado[$i]["cantidad"],
                "unidad_x_caja" => $resultado[$i]["unidad_x_caja"],
                "cantidad_bonificacion" => $resultado[$i]["cantidad_bonificacion"],
                "id_bonificacion" => $resultado[$i]["id_bonificacion"],
                "bonificacion" => $resultado[$i]["bonificacion"],
                "descuento" => $resultado[$i]["descuento"],
                "precio_oferta" => $resultado[$i]["precio_oferta"],
                "precio_x_botella" => $resultado[$i]["precio_x_botella"],
                "stock" => $resultado[$i]["stock"],
                "id_marca" => $resultado[$i]["id_marca"],
                "marca" => $resultado[$i]["marca"],
                "id_categoria" => $resultado[$i]["id_categoria"],
                "categoria" => $resultado[$i]["categoria"],
                "foto" => $obj->obtenerFoto($resultado[$i]["nombre_imagen"])
        );

        $listaProductos[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaProductos);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}