<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Direccion.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cliente"]) || !isset($_POST["id_direccion"]) || !isset($_POST["direccion"]) || !isset($_POST["telefono"]) || !isset($_POST["celular"]) || !isset($_POST["id_departamento"]) || !isset($_POST["id_provincia"]) || !isset($_POST["id_distrito"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$id_cliente = $_POST["id_cliente"];
$id_direccion = $_POST["id_direccion"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];
$celular = $_POST["celular"];
$departamento = $_POST["id_departamento"];
$provincia = $_POST["id_provincia"];
$distrito = $_POST["id_distrito"];


try {
    if (validarToken($token)) {
        $obj = new Direccion();
        $obj->setId_cliente($id_cliente);
        $obj->setId_direccion($id_direccion);
        $obj->setDireccion($direccion);
        $obj->setTelefono($telefono);
        $obj->setCelular($celular);
        $obj->setId_departamento($departamento);
        $obj->setId_provincia($provincia);
        $obj->setId_distrito($distrito);;

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Edito Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
