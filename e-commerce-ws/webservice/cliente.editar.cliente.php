<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Cliente.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cliente"]) || !isset($_POST["nombre_razonsocial"]) || !isset($_POST["email"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$p_cliente = $_POST["id_cliente"];
$p_nombre_razonsocial = $_POST["nombre_razonsocial"];
$p_email = $_POST["email"];


try {
    if (validarToken($token)) {
        $obj = new Cliente();
        $obj->setId_cliente($p_cliente);
        $obj->setNombre_razonsocial($p_nombre_razonsocial);       
        $obj->setEmail($p_email);

        $resultado = $obj->editarCliente();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
