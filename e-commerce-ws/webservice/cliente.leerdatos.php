<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Cliente.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cliente"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$dni_personal = $_POST["id_cliente"];

try {
    if (validarToken($token)) {
        $obj = new Cliente();
        $resultado = $obj->leerDatos($dni_personal);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_cliente" => $resultado[$i]["id_cliente"],
                "nombre_razonsocial" => $resultado[$i]["nombre_razonsocial"],
                "email" => $resultado[$i]["email"],
                "clave" => $resultado[$i]["clave"],
                "estado" => $resultado[$i]["estado"]
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}