<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Direccion.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cliente"]) || !isset($_POST["id_direccion"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_cliente = $_POST["id_cliente"];
$id_direccion = $_POST["id_direccion"];

try {
    if (validarToken($token)) {

        $obj = new Direccion();
        $resultado = $obj->eliminar($id_cliente,$id_direccion);

        Funciones::imprimeJSON(200, "Se Elimino Correctamente", "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}