<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/TipoComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_tipo_comprobante"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_area = $_POST["id_tipo_comprobante"];

try {
    if (validarToken($token)) {
        
        $obj = new TipoComprobante();
        $resultado = $obj->eliminar($id_area);

        Funciones::imprimeJSON(200, "Se Elimino Correctamente", "");
    }
} catch (Exception $exc) {
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}