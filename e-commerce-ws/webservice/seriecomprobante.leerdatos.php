<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/SerieComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_tipo_comprobante"]) || !isset($_POST["numero_serie"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_tipo_comprobante"];
$id_categoria = $_POST["numero_serie"];

try {
    if (validarToken($token)) {
        $obj = new SerieComprobante();
        $resultado = $obj->leerDatos($id_marca, $id_categoria);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_tipo_comprobante" => $resultado[$i]["id_tipo_comprobante"],
                "numero_serie" => $resultado[$i]["numero_serie"],
                "numero_documento" => $resultado[$i]["numero_documento"],
                "ultimo_numero" => $resultado[$i]["ultimo_numero"],
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}