<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/SerieComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_tipo_comprobante"]) ||  !isset($_POST["numero_serie"]) || !isset($_POST["numero_documento"])|| !isset($_POST["ultimo_numero"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_tipo_comprobante"];
$id_categoria = $_POST["numero_serie"];
$nombre = $_POST["numero_documento"];
$ultimo_numero = $_POST["ultimo_numero"];

try {
    if (validarToken($token)) {

        $obj = new SerieComprobante();
        $obj->setId_tipo_comprobante($id_marca);
        $obj->setNumero_serie($id_categoria);
        $obj->setNumero_documento($nombre);
        $obj->setUltimo_numero_documento($ultimo_numero);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}