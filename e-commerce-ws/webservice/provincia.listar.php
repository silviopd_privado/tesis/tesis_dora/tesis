<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Provincia.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_departamento"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$p_marca = $_POST["id_departamento"];

try {
    if (validarToken($token)) {
        $obj = new Provincia();
        $resultado = $obj->cargarProvincia($p_marca);

        $listaProvincia = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_departamento" => $resultado[$i]["id_departamento"],
                "id_provincia" => $resultado[$i]["id_provincia"],
                "nombre" => $resultado[$i]["nombre"]
            );

            $listaProvincia[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaProvincia);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}