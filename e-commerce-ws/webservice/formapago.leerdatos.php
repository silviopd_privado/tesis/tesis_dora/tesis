<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/FormasPago.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_forma_pago"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_forma_pago"];

try {
    if (validarToken($token)) {
        $obj = new FormaPago();
        $resultado = $obj->leerDatos($id_marca);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_forma_pago" => $resultado[$i]["id_forma_pago"],
                "tipo_pago" => $resultado[$i]["tipo_pago"]
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}