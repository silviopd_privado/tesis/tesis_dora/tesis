<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/TipoComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new TipoComprobante();
        $resultado = $obj->cargarTipoComprobante();

        $listaDepartamento = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_tipo_comprobante" => $resultado[$i]["id_tipo_comprobante"],
                "descripcion" => $resultado[$i]["descripcion"]
            );

            $listaDepartamento[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaDepartamento);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}