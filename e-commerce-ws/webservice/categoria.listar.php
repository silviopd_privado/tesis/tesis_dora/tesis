<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Categoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';

if (! isset($_POST["id_marca"]) ){
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$p_marca = $_POST["id_marca"];

try {

    $obj = new Categoria();
    $resultado = $obj->cargarCategoria($p_marca);

    $listaCategoria = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_marca" => $resultado[$i]["id_marca"],
            "id_categoria" => $resultado[$i]["id_categoria"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaCategoria[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaCategoria);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}