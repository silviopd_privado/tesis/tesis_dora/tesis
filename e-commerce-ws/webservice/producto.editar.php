<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["nombre"]) || !isset($_POST["descripcion"]) || !isset($_POST["precio"]) || !isset($_POST["cantidad"]) || !isset($_POST["unidad_x_caja"]) || !isset($_POST["bonificacion"]) || !isset($_POST["descuento"]) || !isset($_POST["precio_oferta"]) || !isset($_POST["precio_x_botella"]) || !isset($_POST["stock"]) || !isset($_POST["id_categoria"]) || !isset($_POST["id_marca"]) || !isset($_POST["cantidad_bonificacion"]) || !isset($_POST["estado"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$id_producto = $_POST["id_producto"];
$nombre = $_POST["nombre"];
$descripcion = $_POST["descripcion"];
$precio = $_POST["precio"];
$cantidad = $_POST["cantidad"];
$unidad_x_caja = $_POST["unidad_x_caja"];
$bonificacion = $_POST["bonificacion"];
$descuento = $_POST["descuento"];
$precio_oferta = $_POST["precio_oferta"];
$precio_x_botella = $_POST["precio_x_botella"];
$stock = $_POST["stock"];
$id_categoria = $_POST["id_categoria"];
$id_marca = $_POST["id_marca"];
$cantidad_bonificacion = $_POST["cantidad_bonificacion"];
$estado = $_POST["estado"];
$nombreimagen = $_POST["nombreimagen"];

try {
    if (validarToken($token)) {

        $obj = new Producto();
        $obj->setId_producto($id_producto);
        $obj->setNombre($nombre);
        $obj->setDescripcion($descripcion);
        $obj->setPrecio($precio);
        $obj->setCantidad($cantidad);
        $obj->setUnidad_x_caja($unidad_x_caja);
        $obj->setBonificacion($bonificacion);
        $obj->setDescuento($descuento);
        $obj->setPrecio_oferta($precio_oferta);
        $obj->setPrecio_x_botella($precio_x_botella);
        $obj->setStock($stock);
        $obj->setId_categoria($id_categoria);
        $obj->setId_marca($id_marca);
        $obj->setCantidad_bonificacion($cantidad_bonificacion);
        $obj->setEstado_producto($estado);
        $obj->setNombreimagen($nombreimagen);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}