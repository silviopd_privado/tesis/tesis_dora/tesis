<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Marca.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {


    $obj = new Marca();
    $resultado = $obj->cargarMarca();

    $listaMarca = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_marca" => $resultado[$i]["id_marca"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaMarca[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaMarca);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}