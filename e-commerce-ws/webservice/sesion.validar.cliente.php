<?php

header('Access-Control-Allow-Origin: *');  
//header('Access-Control-Allow-Origin: http://localhost:8080');  

require_once '../negocio/Sesion.clase.php';
require_once '../util/funciones/Funciones.clase.php';

if (! isset($_POST["dni"]) || ! isset($_POST["clave"])){
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$dni = $_POST["dni"];
$clave = $_POST["clave"];

try {
    $objSesion = new Sesion();
    $objSesion->setDni($dni);
    $objSesion->setClave($clave);
    $resultado = $objSesion->validarSesionCliente();   
            
    if ($resultado["estad"]==200) {
        unset($resultado["estado"]);
        
        /*Generar un token de seguridad*/
        require_once 'token.generar.php';
        $token = generarToken(null,3600);
        $resultado["token"] = $token;
        /*Generar un token de seguridad*/
        
        Funciones::imprimeJSON(200, "Bienvenido a la aplicacion", $resultado);
    } else {
        Funciones::imprimeJSON(500, $resultado["dato"], "");
    }
} catch (Exception $exc) {    
    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}