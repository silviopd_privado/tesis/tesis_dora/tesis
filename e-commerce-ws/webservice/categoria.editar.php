<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Categoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_marca"]) ||  !isset($_POST["nombre"]) || !isset($_POST["id_categoria"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_marca"];
$id_categoria = $_POST["id_categoria"];
$nombre = $_POST["nombre"];

try {
    if (validarToken($token)) {

        $obj = new Categoria();
        $obj->setId_marca($id_marca);
        $obj->setId_categoria($id_categoria);
        $obj->setNombre($nombre);

        $resultado = $obj->editar();

        Funciones::imprimeJSON(200, "Se Modifico Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}