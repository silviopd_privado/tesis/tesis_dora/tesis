<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Categoria.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new Categoria();
        $resultado = $obj->listarCategoria();

        $listaCategoria = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_marca" => $resultado[$i]["id_marca"],
                "id_categoria" => $resultado[$i]["id_categoria"],
                "categoria" => $resultado[$i]["categoria"],
                "marca" => $resultado[$i]["marca"],
            );

            $listaCategoria[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaCategoria);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}