<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/FormasPago.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new FormaPago();
        $resultado = $obj->cargarFormaPago();

        $listaMarca = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_forma_pago" => $resultado[$i]["id_forma_pago"],
                "tipo_pago" => $resultado[$i]["tipo_pago"]
            );

            $listaMarca[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarca);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}