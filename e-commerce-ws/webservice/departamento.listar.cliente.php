<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Departamento.clase.php';
require_once '../util/funciones/Funciones.clase.php';

try {
    $obj = new Departamento();
    $resultado = $obj->cargarDepartamento();

    $listaDepartamento = array();
    for ($i = 0; $i < count($resultado); $i++) {

        $datos = array(
            "id_departamento" => $resultado[$i]["id_departamento"],
            "nombre" => $resultado[$i]["nombre"]
        );

        $listaDepartamento[$i] = $datos;
    }
    Funciones::imprimeJSON(200, "", $listaDepartamento);
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}