<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Marca.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_marca"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_marca = $_POST["id_marca"];

try {
    if (validarToken($token)) {
        $obj = new Marca();
        $resultado = $obj->leerDatos($id_marca);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_marca" => $resultado[$i]["id_marca"],
                "nombre" => $resultado[$i]["nombre"]
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}