<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Producto.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_producto"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_producto = $_POST["id_producto"];

try {
    if (validarToken($token)) {
        $obj = new Producto();
        $resultado = $obj->leerDatos($id_producto);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_producto" => $resultado[$i]["id_producto"],
                "nombre" => $resultado[$i]["nombre"],
                "precio" => $resultado[$i]["precio"],
                "cantidad" => $resultado[$i]["cantidad"],
                "unidad_x_caja" => $resultado[$i]["unidad_x_caja"],
                "bonificacion" => $resultado[$i]["bonificacion"],
                "descuento" => $resultado[$i]["descuento"],
                "precio_oferta" => $resultado[$i]["precio_oferta"],
                "precio_x_botella" => $resultado[$i]["precio_x_botella"],
                "stock" => $resultado[$i]["stock"],                
                "id_categoria" => $resultado[$i]["id_categoria"],
                "id_marca" => $resultado[$i]["id_marca"],
                "cantidad_bonificacion" => $resultado[$i]["cantidad_bonificacion"],
                "estado_producto" => $resultado[$i]["estado_producto"],
                "descripcion" => $resultado[$i]["descripcion"],
                "foto" => $obj->obtenerFoto($resultado[$i]["nombre_imagen"])
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}