<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Cliente.clase.php';
require_once '../util/funciones/Funciones.clase.php';


$p_cliente = $_POST["id_cliente"];
$p_nombre_razonsocial = $_POST["nombre_razonsocial"];
$p_direccion = $_POST["direccion"];
$p_telefono = $_POST["telefono"];
$p_celular = $_POST["celular"];
$p_email = $_POST["email"];
$p_clave = $_POST["clave"];
$p_marca = $_POST["id_departamento"];
$p_provincia = $_POST["id_provincia"];
$p_distrito = $_POST["id_distrito"];

try {

    $obj = new Cliente();
    $obj->setId_cliente($p_cliente);
    $obj->setNombre_razonsocial($p_nombre_razonsocial);
    $obj->setDireccion($p_direccion);
    $obj->setTelefono($p_telefono);
    $obj->setCelular($p_celular);
    $obj->setEmail($p_email);
    $obj->setClave($p_clave);
    $obj->setId_departamento($p_marca);
    $obj->setId_provincia($p_provincia);
    $obj->setId_distrito($p_distrito);

    $resultado = $obj->agregar();

    Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
