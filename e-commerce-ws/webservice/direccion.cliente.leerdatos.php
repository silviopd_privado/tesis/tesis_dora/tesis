<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Direccion.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["id_cliente"]) || !isset($_POST["id_direccion"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$id_cliente = $_POST["id_cliente"];
$id_direccion = $_POST["id_direccion"];

try {
    if (validarToken($token)) {
        $obj = new Direccion();
        $resultado = $obj->leerDatos($id_cliente, $id_direccion);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_cliente" => $resultado[$i]["id_cliente"],
                "id_direccion" => $resultado[$i]["id_direccion"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "celular" => $resultado[$i]["celular"],
                "id_departamento" => $resultado[$i]["id_departamento"],
                "id_provincia" => $resultado[$i]["id_provincia"],
                "id_distrito" => $resultado[$i]["id_distrito"]
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}