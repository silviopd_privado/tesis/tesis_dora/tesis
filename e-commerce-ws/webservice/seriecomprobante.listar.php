<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/SerieComprobante.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

$token = $_POST["token"];

try {
    if (validarToken($token)) {
        $obj = new SerieComprobante();
        $resultado = $obj->listarSerieComprobante();

        $listaCategoria = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "id_tipo_comprobante" => $resultado[$i]["id_tipo_comprobante"],
                "tipo_comprobante" => $resultado[$i]["tipo_comprobante"],
                "numero_serie" => $resultado[$i]["numero_serie"],
                "numero_documento" => $resultado[$i]["numero_documento"],
                "ultimo_numero" => $resultado[$i]["ultimo_numero"],
            );

            $listaCategoria[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaCategoria);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}