<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/FormasPago.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["tipo_pago"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$p_nombre = $_POST["tipo_pago"];

try {
    if (validarToken($token)) {

        $obj = new FormaPago();
        $obj->setTipo_pago($p_nombre);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
