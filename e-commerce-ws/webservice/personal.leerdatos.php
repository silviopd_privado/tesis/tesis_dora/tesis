<?php

header('Access-Control-Allow-Origin: *');  

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

//if (!isset($_POST["token"])) {
//    Funciones::imprimeJSON(500, "Debe especificar un token", "");
//    exit();
//}

if (!isset($_POST["dni_personal"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$dni_personal = $_POST["dni_personal"];

try {
//    if (validarToken($token)) {
        $obj = new Personal();
        $resultado = $obj->leerDatos($dni_personal);

        $listaMarcas = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "dni_personal" => $resultado[$i]["dni_personal"],
                "apellido_paterno" => $resultado[$i]["apellido_paterno"],
                "apellido_materno" => $resultado[$i]["apellido_materno"],
                "nombres" => $resultado[$i]["nombres"],
                "direccion" => $resultado[$i]["direccion"],
                "telefono" => $resultado[$i]["telefono"],
                "email" => $resultado[$i]["email"],
                "clave" => $resultado[$i]["clave"],
                "id_departamento" => $resultado[$i]["id_departamento"],
                "id_provincia" => $resultado[$i]["id_provincia"],                
                "id_distrito" => $resultado[$i]["id_distrito"],
                "id_area" => $resultado[$i]["id_area"],
                "id_cargo" => $resultado[$i]["id_cargo"],
                "estado" => $resultado[$i]["estado"]
            );

            $listaMarcas[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaMarcas);
//    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}