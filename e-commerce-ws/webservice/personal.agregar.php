<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Personal.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["dni_personal"]) || !isset($_POST["apellido_paterno"]) || !isset($_POST["apellido_materno"]) || !isset($_POST["nombres"]) || !isset($_POST["direccion"]) || !isset($_POST["telefono"]) || !isset($_POST["email"]) || !isset($_POST["clave"]) || !isset($_POST["id_departamento"]) || !isset($_POST["id_provincia"]) || !isset($_POST["id_distrito"]) || !isset($_POST["id_area"]) || !isset($_POST["id_cargo"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];

$dni_personal = $_POST["dni_personal"];
$apellido_paterno = $_POST["apellido_paterno"];
$apellido_materno = $_POST["apellido_materno"];
$nombres = $_POST["nombres"];
$direccion = $_POST["direccion"];
$telefono = $_POST["telefono"];
$email = $_POST["email"];
$clave = $_POST["clave"];
$id_departamento = $_POST["id_departamento"];
$id_provincia = $_POST["id_provincia"];
$id_distrito = $_POST["id_distrito"];
$id_area = $_POST["id_area"];
$id_cargo = $_POST["id_cargo"];


try {
    if (validarToken($token)) {

        $obj = new Personal();
        $obj->setDni_personal($dni_personal);
        $obj->setApellido_paterno($apellido_paterno);
        $obj->setApellido_materno($apellido_materno);
        $obj->setNombres($nombres);
        $obj->setDireccion($direccion);
        $obj->setTelefono($telefono);
        $obj->setEmail($email);
        $obj->setClave($clave);
        $obj->setId_departamento($id_departamento);
        $obj->setId_provincia($id_provincia);
        $obj->setId_distrito($id_distrito);
        $obj->setId_area($id_area);
        $obj->setId_cargo($id_cargo);

        $resultado = $obj->agregar();

        Funciones::imprimeJSON(200, "Se Agrego Correctamente", "");
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}
