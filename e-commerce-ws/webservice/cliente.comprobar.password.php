<?php

header('Access-Control-Allow-Origin: *');

require_once '../negocio/Cliente.clase.php';
require_once '../util/funciones/Funciones.clase.php';
require_once 'token.validar.php';

if (!isset($_POST["token"])) {
    Funciones::imprimeJSON(500, "Debe especificar un token", "");
    exit();
}

if (!isset($_POST["password"]) || !isset($_POST["id_cliente"])) {
    Funciones::imprimeJSON(500, "Falta completar los datos requeridos", "");
    exit();
}

$token = $_POST["token"];
$password = $_POST["password"];
$id_cliente = $_POST["id_cliente"];

try {
    if (validarToken($token)) {
        $obj = new Cliente();
        $resultado = $obj->comprobarPassword($id_cliente, $password);

        $listaProductos = array();
        for ($i = 0; $i < count($resultado); $i++) {

            $datos = array(
                "estado" => $resultado[$i]["estado"],
                "dato" => $resultado[$i]["dato"]
            );

            $listaProductos[$i] = $datos;
        }
        Funciones::imprimeJSON(200, "", $listaProductos);
    }
} catch (Exception $exc) {

    Funciones::imprimeJSON(500, $exc->getMessage(), "");
}