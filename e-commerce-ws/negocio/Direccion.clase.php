<?php

require_once '../datos/Conexion.clase.php';

class Direccion extends Conexion {

    private $id_cliente;
    private $id_direccion;
    private $direccion;
    private $telefono;
    private $celular;
    private $id_departamento;
    private $id_provincia;
    private $id_distrito;

    function getId_cliente() {
        return $this->id_cliente;
    }

    function getId_direccion() {
        return $this->id_direccion;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCelular() {
        return $this->celular;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function setId_cliente($id_cliente) {
        $this->id_cliente = $id_cliente;
    }

    function setId_direccion($id_direccion) {
        $this->id_direccion = $id_direccion;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }

    public function agregarDireccionClientePersonal() {
        $this->dblink->beginTransaction();

        try {
            $sql = "select * from f_agregar_direccion_cliente(:p_id_cliente, :p_direccion, :p_telefono, :p_celular, :p_id_departamento, :p_id_provincia, :p_id_distrito);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $this->getId_cliente());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_celular", $this->getCelular());
            $sentencia->bindParam(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindParam(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindParam(":p_id_distrito", $this->getId_distrito());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function listarDireccionClientesPersonal() {
        try {
            $sql = "
                    SELECT 
                      direccion.id_cliente, 
                      cliente.nombre_razonsocial, 
                      direccion.id_direccion, 
                      direccion.direccion, 
                      direccion.telefono, 
                      direccion.celular, 
                      distrito.nombre as distrito, 
                      provincia.nombre as provincia, 
                      departamento.nombre as departamento
                    FROM 
                      public.cliente, 
                      public.direccion, 
                      public.distrito, 
                      public.provincia, 
                      public.departamento
                    WHERE 
                      cliente.id_cliente = direccion.id_cliente AND
                      direccion.id_departamento = distrito.id_departamento AND
                      direccion.id_provincia = distrito.id_provincia AND
                      direccion.id_distrito = distrito.id_distrito AND
                      distrito.id_departamento = provincia.id_departamento AND
                      distrito.id_provincia = provincia.id_provincia AND
                      provincia.id_departamento = departamento.id_departamento;
            ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function listarDireccionClientesCliente($p_id_cliente) {
        try {
            $sql = "
                    SELECT 
                      direccion.id_cliente, 
                      cliente.nombre_razonsocial, 
                      direccion.id_direccion, 
                      direccion.direccion, 
                      direccion.telefono, 
                      direccion.celular, 
                      distrito.nombre as distrito, 
                      provincia.nombre as provincia, 
                      departamento.nombre as departamento
                    FROM 
                      public.cliente, 
                      public.direccion, 
                      public.distrito, 
                      public.provincia, 
                      public.departamento
                    WHERE 
                      cliente.id_cliente = direccion.id_cliente AND
                      direccion.id_departamento = distrito.id_departamento AND
                      direccion.id_provincia = distrito.id_provincia AND
                      direccion.id_distrito = distrito.id_distrito AND
                      distrito.id_departamento = provincia.id_departamento AND
                      distrito.id_provincia = provincia.id_provincia AND
                      provincia.id_departamento = departamento.id_departamento AND
                      direccion.id_cliente = :p_id_cliente;
            ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $p_id_cliente);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function leerDatos($p_id_cliente, $p_id_direccion) {
        try {
            $sql = "select * from direccion where id_cliente @@ :p_id_cliente and id_direccion = :p_id_direccion";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $p_id_cliente);
            $sentencia->bindParam(":p_id_direccion", $p_id_direccion);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.direccion
                      SET  direccion=:p_direccion, telefono=:p_telefono, celular=:p_celular, 
                          id_departamento=:p_id_departamento, id_provincia=:p_id_provincia, id_distrito=:p_id_distrito
                    WHERE id_cliente=:p_id_cliente and id_direccion=:p_id_direccion;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_celular", $this->getCelular());
            $sentencia->bindParam(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindParam(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindParam(":p_id_distrito", $this->getId_distrito());
            $sentencia->bindParam(":p_id_cliente", $this->getId_cliente());
            $sentencia->bindParam(":p_id_direccion", $this->getId_direccion());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla cliente.");
        }
    }

    public function eliminar($id_cliente, $id_direccion) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM direccion where id_cliente @@ :p_id_cliente and id_direccion = :p_id_direccion";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $id_cliente);
            $sentencia->bindParam(":p_id_direccion", $id_direccion);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

}
