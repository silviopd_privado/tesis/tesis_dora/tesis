<?php

require_once '../datos/Conexion.clase.php';

class Producto extends Conexion {

    private $id_producto;
    private $nombre;
    private $descripcion;
    private $precio;
    private $cantidad;
    private $unidad_x_caja;
    private $bonificacion;
    private $descuento;
    private $precio_oferta;
    private $precio_x_botella;
    private $stock;
    private $id_categoria;
    private $id_marca;
    private $cantidad_bonificacion;
    private $estado_producto;
    private $nombreimagen;

    function getId_producto() {
        return $this->id_producto;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getPrecio() {
        return $this->precio;
    }

    function getCantidad() {
        return $this->cantidad;
    }

    function getUnidad_x_caja() {
        return $this->unidad_x_caja;
    }

    function getBonificacion() {
        return $this->bonificacion;
    }

    function getDescuento() {
        return $this->descuento;
    }

    function getPrecio_oferta() {
        return $this->precio_oferta;
    }

    function getPrecio_x_botella() {
        return $this->precio_x_botella;
    }

    function getStock() {
        return $this->stock;
    }

    function getId_categoria() {
        return $this->id_categoria;
    }

    function getId_marca() {
        return $this->id_marca;
    }

    function getCantidad_bonificacion() {
        return $this->cantidad_bonificacion;
    }

    function setId_producto($id_producto) {
        $this->id_producto = $id_producto;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setPrecio($precio) {
        $this->precio = $precio;
    }

    function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }

    function setUnidad_x_caja($unidad_x_caja) {
        $this->unidad_x_caja = $unidad_x_caja;
    }

    function setBonificacion($bonificacion) {
        $this->bonificacion = $bonificacion;
    }

    function setDescuento($descuento) {
        $this->descuento = $descuento;
    }

    function setPrecio_oferta($precio_oferta) {
        $this->precio_oferta = $precio_oferta;
    }

    function setPrecio_x_botella($precio_x_botella) {
        $this->precio_x_botella = $precio_x_botella;
    }

    function setStock($stock) {
        $this->stock = $stock;
    }

    function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    function setCantidad_bonificacion($cantidad_bonificacion) {
        $this->cantidad_bonificacion = $cantidad_bonificacion;
    }

    function getEstado_producto() {
        return $this->estado_producto;
    }

    function setEstado_producto($estado_producto) {
        $this->estado_producto = $estado_producto;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    public function listarProductosTotales() {
        try {
            $sql = "SELECT 
                            p.id_producto, 
                            p.nombre,
                            p.descripcion, 
                            p.precio, 
                            p.cantidad, 
                            p.unidad_x_caja,
                            p.cantidad_bonificacion, 
                            p.bonificacion as id_bonificacion,
                            p2.nombre as bonificacion, 
                            p.descuento, 
                            p.precio_oferta, 
                            p.precio_x_botella, 
                            p.stock, 
                            m.id_marca,
                            m.nombre as marca,
                            c.id_categoria, 
                            c.nombre as categoria,
                            p.nombre_imagen
                    FROM 
                            producto p inner join categoria c on p.id_categoria = c.id_categoria AND p.id_marca = c.id_marca
                                       inner join marca m on c.id_marca = m.id_marca
                                       inner join producto p2 on p2.id_producto = p.bonificacion
                    WHERE
                            p.estado_producto like 'A'
                    ORDER BY
                            2;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    function getNombreimagen() {
        return $this->nombreimagen;
    }

    function setNombreimagen($nombreimagen) {
        $this->nombreimagen = $nombreimagen;
    }

    public function listarProductos() {
        try {
            $sql = "
                    SELECT 
                            p.id_producto, 
                            p.nombre,
                            p.descripcion, 
                            p.precio, 
                            p.cantidad, 
                            p.unidad_x_caja,
                            p.cantidad_bonificacion, 
                            p.bonificacion as id_bonificacion,
                            p2.nombre as bonificacion, 
                            p.descuento, 
                            p.precio_oferta, 
                            p.precio_x_botella, 
                            p.stock, 
                            m.id_marca,
                            m.nombre as marca,
                            c.id_categoria, 
                            c.nombre as categoria
                    FROM 
                            producto p inner join categoria c on p.id_categoria = c.id_categoria AND p.id_marca = c.id_marca
                                       inner join marca m on c.id_marca = m.id_marca
                                       inner join producto p2 on p2.id_producto = p.bonificacion
                    ORDER BY
                            2;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_codigoProducto) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM producto where id_producto = :p_codigoProducto;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoProducto", $p_codigoProducto);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.producto
                      SET nombre=:p_nombre, precio=:p_precio, cantidad=:p_cantidad, unidad_x_caja=:p_unidad_x_caja, 
                          bonificacion=:p_bonificacion, descuento=:p_descuento, precio_oferta=:p_precio_oferta, precio_x_botella=:p_precio_x_botella, 
                          stock=:p_stock, id_categoria=:p_id_categoria, id_marca=:p_id_marca, cantidad_bonificacion=:p_cantidad_bonificacion, 
                          estado_producto=:p_estado_producto, descripcion=:p_descripcion,nombre_imagen=:p_nombreimagen
                    WHERE id_producto=:p_id_producto;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_precio", $this->getPrecio());
            $sentencia->bindParam(":p_cantidad", $this->getCantidad());
            $sentencia->bindParam(":p_unidad_x_caja", $this->getUnidad_x_caja());
            $sentencia->bindParam(":p_bonificacion", $this->getBonificacion());
            $sentencia->bindParam(":p_descuento", $this->getDescuento());
            $sentencia->bindParam(":p_precio_oferta", $this->getPrecio_oferta());
            $sentencia->bindParam(":p_precio_x_botella", $this->getPrecio_x_botella());
            $sentencia->bindParam(":p_stock", $this->getStock());
            $sentencia->bindParam(":p_id_categoria", $this->getId_categoria());
            $sentencia->bindParam(":p_id_marca", $this->getId_marca());
            $sentencia->bindParam(":p_cantidad_bonificacion", $this->getCantidad_bonificacion());
            $sentencia->bindParam(":p_estado_producto", $this->getEstado_producto());
            $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
            $sentencia->bindParam(":p_nombreimagen", $this->getNombreimagen());
            $sentencia->bindParam(":p_id_producto", $this->getId_producto());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla marca.");
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('producto') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $categoriaid = $resultado["nc"];
                $this->setId_producto($categoriaid);
                $sql = "
                        INSERT INTO public.producto(
                                id_producto, nombre, precio, cantidad, unidad_x_caja, bonificacion, 
                                descuento, precio_oferta, precio_x_botella, stock, id_categoria, 
                                id_marca, cantidad_bonificacion, descripcion,nombre_imagen)
                        VALUES (:p_id_producto, :p_nombre, :p_precio, :p_cantidad, :p_unidad_x_caja, :p_bonificacion, 
                                :p_descuento, :p_precio_oferta, :p_precio_x_botella, :p_stock, :p_id_categoria, 
                                :p_id_marca, :p_cantidad_bonificacion, :p_descripcion,:p_nombreimagen);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_id_producto", $this->getId_producto());
                $sentencia->bindParam(":p_nombre", $this->getNombre());
                $sentencia->bindParam(":p_precio", $this->getPrecio());
                $sentencia->bindParam(":p_cantidad", $this->getCantidad());
                $sentencia->bindParam(":p_unidad_x_caja", $this->getUnidad_x_caja());
                $sentencia->bindParam(":p_bonificacion", $this->getBonificacion());
                $sentencia->bindParam(":p_descuento", $this->getDescuento());
                $sentencia->bindParam(":p_precio_oferta", $this->getPrecio_oferta());
                $sentencia->bindParam(":p_precio_x_botella", $this->getPrecio_x_botella());
                $sentencia->bindParam(":p_stock", $this->getStock());
                $sentencia->bindParam(":p_id_categoria", $this->getId_categoria());
                $sentencia->bindParam(":p_id_marca", $this->getId_marca());
                $sentencia->bindParam(":p_cantidad_bonificacion", $this->getCantidad_bonificacion());
                $sentencia->bindParam(":p_descripcion", $this->getDescripcion());
                $sentencia->bindParam(":p_nombreimagen", $this->getNombreimagen());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'producto';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla producto.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function leerDatos($p_codigoProducto) {
        try {
            $sql = "select * from producto where id_producto = :p_codigoProducto;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoProducto", $p_codigoProducto);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function leerDatos2($p_codigoProducto) {
        try {
            $sql = "select p.*,m.nombre as marca,c.nombre as categoria from producto p inner join categoria c on c.id_marca=p.id_marca and c.id_categoria=p.id_categoria inner join marca m on m.id_marca=c.id_marca where p.id_producto = :p_codigoProducto";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoProducto", $p_codigoProducto);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function obtenerFoto($codigoArticulo) {
        $foto = "../imagenes/" . $codigoArticulo;

        if (file_exists($foto)) {
            $foto = $foto;
        } else {
            $foto = "none";
        }

        if ($foto == "none") {
            return $foto;
        } else {
            return Funciones::$DIRECCION_WEB_SERVICE . $foto;
        }
    }

}
