<?php

require_once '../datos/Conexion.clase.php';
class Sesion extends Conexion{
    
    private $dni;
    private $clave;
    
    function getDni() {
        return $this->dni;
    }

    function getClave() {
        return $this->clave;
    }

    function setDni($dni) {
        $this->dni = $dni;
    }

    function setClave($clave) {
        $this->clave = $clave;
    }

    public function validarSesionPersonal(){
        try {
            $sql = "select * from f_validar_sesion_personal( :p_dni , :p_clave)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni",$this->getDni());
            $sentencia->bindParam(":p_clave",$this->getClave());
            $sentencia->execute();
            return $sentencia->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            throw $ex;
        }
    }    

    public function validarSesionCliente(){
        try {
            $sql = "select * from f_validar_sesion_cliente( :p_dni , :p_clave)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni",$this->getDni());
            $sentencia->bindParam(":p_clave",$this->getClave());
            $sentencia->execute();
            return $sentencia->fetch(PDO::FETCH_ASSOC);
        } catch (Exception $ex) {
            throw $ex;
        }
    }  
        
}

