<?php

require_once '../datos/Conexion.clase.php';

class Categoria extends Conexion {

    private $id_categoria;
    private $nombre;
    private $id_marca;
    
    function getId_categoria() {
        return $this->id_categoria;
    }

    function getNombre() {
        return $this->nombre;
    }

    function getId_marca() {
        return $this->id_marca;
    }

    function setId_categoria($id_categoria) {
        $this->id_categoria = $id_categoria;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    
    public function cargarCategoria($p_tipoMarca) {
        try {
            $sql = "SELECT *  FROM categoria Where id_marca = :p_tipoMarca order by 2";

            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_tipoMarca", $p_tipoMarca);
            $sentencia->execute();

            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function listarCategoria() {
        try {
            $sql = "
                    SELECT 
                      categoria.id_categoria, 
                      categoria.nombre as categoria, 
                      marca.id_marca, 
                      marca.nombre as marca
                    FROM 
                      public.categoria, 
                      public.marca
                    WHERE 
                      marca.id_marca = categoria.id_marca
                    ORDER BY 2;
                    ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoMarca,$p_codigoCategoria) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM categoria where id_marca = :p_codigoMarca AND id_categoria= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update categoria set nombre= :p_nombre where id_marca = :p_codigoMarca AND id_categoria= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_marca());
            $sentencia->bindParam(":p_codigoCategoria", $this->getId_categoria());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla marca.");
        }
    }
    
    public function agregar() {
       $this->dblink->beginTransaction();
        try {
            
            $sql = "select * from f_agregar_categoria(:p_id_marca,:p_nombre)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_marca", $this->getId_marca());
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_codigoMarca,$p_codigoCategoria) {
        try {
            $sql = "select * from categoria where id_marca = :p_codigoMarca AND id_categoria= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
