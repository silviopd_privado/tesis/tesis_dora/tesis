<?php

require_once '../datos/Conexion.clase.php';

class Personal extends Conexion {

    private $dni_personal;
    private $apellido_paterno;
    private $apellido_materno;
    private $nombres;
    private $direccion;
    private $telefono;
    private $email;
    private $clave;
    private $id_departamento;
    private $id_provincia;
    private $id_distrito;
    private $id_area;
    private $id_cargo;
    private $estado;

    function getDni_personal() {
        return $this->dni_personal;
    }

    function getApellido_paterno() {
        return $this->apellido_paterno;
    }

    function getApellido_materno() {
        return $this->apellido_materno;
    }

    function getNombres() {
        return $this->nombres;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getEmail() {
        return $this->email;
    }

    function getClave() {
        return $this->clave;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function getId_area() {
        return $this->id_area;
    }

    function getId_cargo() {
        return $this->id_cargo;
    }

    function getEstado() {
        return $this->estado;
    }

    function setDni_personal($dni_personal) {
        $this->dni_personal = $dni_personal;
    }

    function setApellido_paterno($apellido_paterno) {
        $this->apellido_paterno = $apellido_paterno;
    }

    function setApellido_materno($apellido_materno) {
        $this->apellido_materno = $apellido_materno;
    }

    function setNombres($nombres) {
        $this->nombres = $nombres;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setClave($clave) {
        $this->clave = $clave;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }

    function setId_area($id_area) {
        $this->id_area = $id_area;
    }

    function setId_cargo($id_cargo) {
        $this->id_cargo = $id_cargo;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    public function listarPersonal() {
        try {
            $sql = "
                    SELECT 
                      personal.dni_personal, 
                      (personal.apellido_paterno ||' '||  personal.apellido_materno || ' , '|| personal.nombres)::varchar as nombre, 
                      personal.direccion, 
                      personal.telefono, 
                      personal.email,
                      departamento.nombre as departamento, 
                      provincia.nombre as provincia, 
                      distrito.nombre as distrito, 
                      area.nombre as area, 
                      cargo.nombre as cargo, 
                      (case when personal.estado = 'A' then 'ACTIVO' else 'INACTIVO' end)::varchar as estado 
                    FROM 
                      public.personal, 
                      public.departamento, 
                      public.distrito, 
                      public.provincia, 
                      public.area, 
                      public.cargo
                    WHERE 
                      departamento.id_departamento = provincia.id_departamento AND
                      distrito.id_departamento = personal.id_departamento AND
                      distrito.id_provincia = personal.id_provincia AND
                      distrito.id_distrito = personal.id_distrito AND
                      provincia.id_departamento = distrito.id_departamento AND
                      provincia.id_provincia = distrito.id_provincia AND
                      area.id_area = personal.id_area AND
                      cargo.id_cargo = personal.id_cargo;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {
            $sql = "select * from f_agregar_personal(:p_dni_personal,:p_apellido_paterno,:p_apellido_materno,:p_nombres,:p_direccion,:p_telefono,:p_email,:p_clave,:p_id_departamento,:p_id_provincia,:p_id_distrito,:p_id_area,:p_id_cargo)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni_personal", $this->getDni_personal());
            $sentencia->bindParam(":p_apellido_paterno", $this->getApellido_paterno());
            $sentencia->bindParam(":p_apellido_materno", $this->getApellido_materno());
            $sentencia->bindParam(":p_nombres", $this->getNombres());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->bindParam(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindParam(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindParam(":p_id_distrito", $this->getId_distrito());
            $sentencia->bindParam(":p_id_area", $this->getId_area());
            $sentencia->bindParam(":p_id_cargo", $this->getId_cargo());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }

    public function leerDatos($p_codigoPersonal) {
        try {
            $sql = "select * from personal where dni_personal = :p_codigoPersonal;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoPersonal", $p_codigoPersonal);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.personal
                      SET apellido_paterno=:p_apellido_paterno, apellido_materno=:p_apellido_materno, nombres=:p_nombres, 
                          direccion=:p_direccion, telefono=:p_telefono, email=:p_email, clave=:p_clave, id_departamento=:p_id_departamento, 
                          id_provincia=:p_id_provincia, id_distrito=:p_id_distrito, id_area=:p_id_area, id_cargo=:p_id_cargo, estado=:p_estado
                    WHERE dni_personal=:p_dni_personal";
            $sentencia = $this->dblink->prepare($sql);            
            $sentencia->bindParam(":p_apellido_paterno", $this->getApellido_paterno());
            $sentencia->bindParam(":p_apellido_materno", $this->getApellido_materno());
            $sentencia->bindParam(":p_nombres", $this->getNombres());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->bindParam(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindParam(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindParam(":p_id_distrito", $this->getId_distrito());
            $sentencia->bindParam(":p_id_area", $this->getId_area());
            $sentencia->bindParam(":p_id_cargo", $this->getId_cargo());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_dni_personal", $this->getDni_personal());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla personal.");
        }
    }

    public function eliminar($p_codigoProducto) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM personal where dni_personal = :p_dni_personal;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni_personal", $p_codigoProducto);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

}
