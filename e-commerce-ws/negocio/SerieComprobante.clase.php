<?php

require_once '../datos/Conexion.clase.php';

class SerieComprobante extends Conexion {

    private $id_tipo_comprobante;
    private $numero_serie;
    private $numero_documento;
    private $ultimo_numero_documento;

    function getId_tipo_comprobante() {
        return $this->id_tipo_comprobante;
    }

    function getNumero_serie() {
        return $this->numero_serie;
    }

    function getNumero_documento() {
        return $this->numero_documento;
    }

    function setId_tipo_comprobante($id_tipo_comprobante) {
        $this->id_tipo_comprobante = $id_tipo_comprobante;
    }

    function setNumero_serie($numero_serie) {
        $this->numero_serie = $numero_serie;
    }

    function setNumero_documento($numero_documento) {
        $this->numero_documento = $numero_documento;
    }
    
    function getUltimo_numero_documento() {
        return $this->ultimo_numero_documento;
    }

    function setUltimo_numero_documento($ultimo_numero_documento) {
        $this->ultimo_numero_documento = $ultimo_numero_documento;
    }
            
    public function listarSerieComprobante() {
        try {
            $sql = "
                    SELECT 
                      tipo_comprobante.descripcion as tipo_comprobante, 
                      tipo_comprobante.id_tipo_comprobante, 
                      serie_comprobante.numero_serie, 
                      serie_comprobante.numero_documento,
                      serie_comprobante.ultimo_numero
                    FROM 
                      public.serie_comprobante, 
                      public.tipo_comprobante
                    WHERE 
                      tipo_comprobante.id_tipo_comprobante = serie_comprobante.id_tipo_comprobante
                    ORDER BY
                      1;                    ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_codigoMarca, $p_codigoCategoria) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM serie_comprobante where id_tipo_comprobante = :p_codigoMarca AND numero_serie= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update serie_comprobante set numero_documento= :p_nombre,ultimo_numero = :p_ultimo_numero where id_tipo_comprobante = :p_codigoMarca AND numero_serie= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNumero_documento());
            $sentencia->bindParam(":p_ultimo_numero", $this->getUltimo_numero_documento());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_tipo_comprobante());
            $sentencia->bindParam(":p_codigoCategoria", $this->getNumero_serie());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla serie comprobante.");
        }
    }

    public function agregar() {
        $this->dblink->beginTransaction();
        try {

            $sql = "INSERT INTO public.serie_comprobante(id_tipo_comprobante, numero_serie, numero_documento,ultimo_numero) VALUES (:p_id_marca2,:p_id_marca,:p_nombre,:p_ultimo_numero)";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_marca2", $this->getId_tipo_comprobante());
            $sentencia->bindParam(":p_id_marca", $this->getNumero_serie());
            $sentencia->bindParam(":p_nombre", $this->getNumero_documento());
            $sentencia->bindParam(":p_ultimo_numero", $this->getUltimo_numero_documento());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function leerDatos($p_codigoMarca, $p_codigoCategoria) {
        try {
            $sql = "select * from serie_comprobante where id_tipo_comprobante = :p_codigoMarca AND numero_serie= :p_codigoCategoria;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->bindParam(":p_codigoCategoria", $p_codigoCategoria);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
