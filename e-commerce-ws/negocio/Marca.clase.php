<?php

require_once '../datos/Conexion.clase.php';

class Marca extends Conexion {

    private $id_marca;
    private $nombre;
    
    function getId_marca() {
        return $this->id_marca;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId_marca($id_marca) {
        $this->id_marca = $id_marca;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    
    public function cargarMarca() {
        try {
            $sql = "select * from marca order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoMarca) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from marca where id_marca = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update marca set nombre= :p_nombre where id_marca = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_marca());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla marca.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('marca') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $categoriaid = $resultado["nc"];
                $this->setId_marca($categoriaid);
                $sql = "INSERT INTO public.marca(id_marca, nombre) VALUES (:p_codigoMarca, :p_nombre);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_codigoMarca", $this->getId_marca());
                $sentencia->bindParam(":p_nombre", $this->getNombre());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'marca';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla marca.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_codigoMarca) {
        try {
            $sql = "select * from marca where id_marca = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
