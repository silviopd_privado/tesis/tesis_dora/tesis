<?php

require_once '../datos/Conexion.clase.php';

class Cargo extends Conexion {

    private $id_cargo;
    private $nombre;
    
    function getId_cargo() {
        return $this->id_cargo;
    }

    function getNombre() {
        return $this->nombre;
    }

    function setId_cargo($id_cargo) {
        $this->id_cargo = $id_cargo;
    }

    function setNombre($nombre) {
        $this->nombre = $nombre;
    }

    
    public function cargarCargo() {
        try {
            $sql = "select * from cargo order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function eliminar($p_codigoMarca) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from cargo where id_cargo = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update cargo set nombre= :p_nombre where id_cargo = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getNombre());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_cargo());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla marca.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('cargo') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $categoriaid = $resultado["nc"];
                $this->setId_cargo($categoriaid);
                $sql = "INSERT INTO public.cargo(id_cargo, nombre) VALUES (:p_codigoMarca, :p_nombre);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_codigoMarca", $this->getId_cargo());
                $sentencia->bindParam(":p_nombre", $this->getNombre());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'cargo';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla cargo.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_codigoMarca) {
        try {
            $sql = "select * from cargo where id_cargo = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
}
