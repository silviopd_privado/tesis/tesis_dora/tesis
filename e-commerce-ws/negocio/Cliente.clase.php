<?php

require_once '../datos/Conexion.clase.php';

class Cliente extends Conexion {

    private $id_cliente;
    private $nombre_razonsocial;
    private $direccion;
    private $telefono;
    private $celular;
    private $email;
    private $clave;
    private $id_departamento;
    private $id_provincia;
    private $id_distrito;
    private $estado;
    
    function getId_cliente() {
        return $this->id_cliente;
    }

    function getNombre_razonsocial() {
        return $this->nombre_razonsocial;
    }

    function getDireccion() {
        return $this->direccion;
    }

    function getTelefono() {
        return $this->telefono;
    }

    function getCelular() {
        return $this->celular;
    }

    function getEmail() {
        return $this->email;
    }

    function getClave() {
        return $this->clave;
    }

    function getId_departamento() {
        return $this->id_departamento;
    }

    function getId_provincia() {
        return $this->id_provincia;
    }

    function getId_distrito() {
        return $this->id_distrito;
    }

    function setId_cliente($id_cliente) {
        $this->id_cliente = $id_cliente;
    }

    function setNombre_razonsocial($nombre_razonsocial) {
        $this->nombre_razonsocial = $nombre_razonsocial;
    }

    function setDireccion($direccion) {
        $this->direccion = $direccion;
    }

    function setTelefono($telefono) {
        $this->telefono = $telefono;
    }

    function setCelular($celular) {
        $this->celular = $celular;
    }

    function setEmail($email) {
        $this->email = $email;
    }

    function setClave($clave) {
        $this->clave = $clave;
    }

    function setId_departamento($id_departamento) {
        $this->id_departamento = $id_departamento;
    }

    function setId_provincia($id_provincia) {
        $this->id_provincia = $id_provincia;
    }

    function setId_distrito($id_distrito) {
        $this->id_distrito = $id_distrito;
    }

    function getEstado() {
        return $this->estado;
    }

    function setEstado($estado) {
        $this->estado = $estado;
    }

    public function agregar() {
        $this->dblink->beginTransaction();

        try {
            $sql = "select * from f_agregar_cliente(:p_id_cliente, :p_nombre_razonsocial, :p_direccion, :p_telefono, :p_celular,:p_email, :p_clave, :p_id_departamento, :p_id_provincia, :p_id_distrito);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $this->getId_cliente());
            $sentencia->bindParam(":p_nombre_razonsocial", $this->getNombre_razonsocial());
            $sentencia->bindParam(":p_direccion", $this->getDireccion());
            $sentencia->bindParam(":p_telefono", $this->getTelefono());
            $sentencia->bindParam(":p_celular", $this->getCelular());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->bindParam(":p_id_departamento", $this->getId_departamento());
            $sentencia->bindParam(":p_id_provincia", $this->getId_provincia());
            $sentencia->bindParam(":p_id_distrito", $this->getId_distrito());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }
        
    public function agregarClientePersonal() {
        $this->dblink->beginTransaction();

        try {
            $sql = "INSERT INTO public.cliente(id_cliente, nombre_razonsocial, email, clave)
                    VALUES (:p_id_cliente, :p_nombre_razonsocial, :p_email, :p_clave);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_cliente", $this->getId_cliente());
            $sentencia->bindParam(":p_nombre_razonsocial", $this->getNombre_razonsocial());            
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->execute();

            $this->dblink->commit();

            return true; //significa que todo se ha ejecutado correctamente
        } catch (Exception $exc) {
            $this->dblink->rollBack(); //Extornar toda la transacción
            throw $exc;
        }

        return false;
    }
    
    public function listarClientesPersonal() {
        try {
            $sql = "
                    SELECT 
                      id_cliente,
                      upper(nombre_razonsocial) as nombre_razonsocial,
                      email,
                      (case when cliente.estado = 'A' then 'ACTIVO' else 'INACTIVO' end)::varchar as estado
                    FROM 
                      public.cliente
            ";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function leerDatos($p_codigoPersonal) {
        try {
            $sql = "select * from cliente where id_cliente = :p_codigoPersonal;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoPersonal", $p_codigoPersonal);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.cliente
                      SET nombre_razonsocial=:p_nombres, email=:p_email, clave=:p_clave,estado=:p_estado
                    WHERE id_cliente=:p_dni_personal";
            $sentencia = $this->dblink->prepare($sql);            
            $sentencia->bindParam(":p_nombres", $this->getNombre_razonsocial());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->bindParam(":p_estado", $this->getEstado());
            $sentencia->bindParam(":p_dni_personal", $this->getId_cliente());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla cliente.");
        }
    }
    
    public function editarClientePassword() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.cliente
                      SET nombre_razonsocial=:p_nombres, email=:p_email, clave=:p_clave
                    WHERE id_cliente=:p_dni_personal";
            $sentencia = $this->dblink->prepare($sql);            
            $sentencia->bindParam(":p_nombres", $this->getNombre_razonsocial());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_clave", $this->getClave());
            $sentencia->bindParam(":p_dni_personal", $this->getId_cliente());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla cliente.");
        }
    }
    
    public function editarCliente() {
        $this->dblink->beginTransaction();
        try {
            $sql = "
                    UPDATE public.cliente
                      SET nombre_razonsocial=:p_nombres, email=:p_email
                    WHERE id_cliente=:p_dni_personal";
            $sentencia = $this->dblink->prepare($sql);            
            $sentencia->bindParam(":p_nombres", $this->getNombre_razonsocial());
            $sentencia->bindParam(":p_email", $this->getEmail());
            $sentencia->bindParam(":p_dni_personal", $this->getId_cliente());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla cliente.");
        }
    }

    public function eliminar($p_codigoProducto) {
        $this->dblink->beginTransaction();
        try {
            $sql = "DELETE FROM cliente where id_cliente = :p_dni_personal;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_dni_personal", $p_codigoProducto);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }

    public function comprobarPassword($p_usuario,$p_password) {
        try {
            $sql = "select * from f_validar_password_cliente(:p_usuario,:p_password);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_usuario", $p_usuario);
            $sentencia->bindParam(":p_password", $p_password);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
}
