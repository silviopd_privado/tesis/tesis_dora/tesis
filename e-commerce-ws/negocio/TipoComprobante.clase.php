<?php

require_once '../datos/Conexion.clase.php';

class TipoComprobante extends Conexion {

    private $id_tipo_comprobante;
    private $descripcion;
    
    function getId_tipo_comprobante() {
        return $this->id_tipo_comprobante;
    }

    function getDescripcion() {
        return $this->descripcion;
    }

    function setId_tipo_comprobante($id_tipo_comprobante) {
        $this->id_tipo_comprobante = $id_tipo_comprobante;
    }

    function setDescripcion($descripcion) {
        $this->descripcion = $descripcion;
    }

    
    
    public function cargarTipoComprobante() {
        try {
            $sql = "select * from tipo_comprobante order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoMarca) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from tipo_comprobante where id_tipo_comprobante = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update tipo_comprobante set descripcion= :p_nombre where id_tipo_comprobante = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getDescripcion());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_tipo_comprobante());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla marca.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            
            $sql = "INSERT INTO public.tipo_comprobante(id_tipo_comprobante, descripcion) VALUES (:p_id_marca, :p_nombre);";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_id_marca", $this->getId_tipo_comprobante());
            $sentencia->bindParam(":p_nombre", $this->getDescripcion());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_codigoMarca) {
        try {
            $sql = "select * from tipo_comprobante where id_tipo_comprobante = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
