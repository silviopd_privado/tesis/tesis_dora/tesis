<?php

require_once '../datos/Conexion.clase.php';

class FormaPago extends Conexion {

    private $id_forma_pago;
    private $tipo_pago;
    
    function getId_forma_pago() {
        return $this->id_forma_pago;
    }

    function getTipo_pago() {
        return $this->tipo_pago;
    }

    function setId_forma_pago($id_forma_pago) {
        $this->id_forma_pago = $id_forma_pago;
    }

    function setTipo_pago($tipo_pago) {
        $this->tipo_pago = $tipo_pago;
    }

    public function cargarFormaPago() {
        try {
            $sql = "select * from forma_pago order by 2";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }
    
    public function eliminar($p_codigoMarca) {
        $this->dblink->beginTransaction();
        try {
            $sql = "delete from forma_pago where id_forma_pago = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function editar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "update forma_pago set tipo_pago= :p_nombre where id_forma_pago = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_nombre", $this->getTipo_pago());
            $sentencia->bindParam(":p_codigoMarca", $this->getId_forma_pago());
            $sentencia->execute();
            $this->dblink->commit();
            return true;
        } catch (Exception $ex) {
            throw new Exception("No se ha configurado el correlativo para la tabla forma pago.");
        }
    }
    
    public function agregar() {
        $this->dblink->beginTransaction();
        try {
            $sql = "select * from f_generar_correlativo('forma_pago') as nc;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->execute();
            $resultado = $sentencia->fetch();
            if ($sentencia->rowCount()) {
                $categoriaid = $resultado["nc"];
                $this->setId_forma_pago($categoriaid);
                $sql = "INSERT INTO public.forma_pago(id_forma_pago, tipo_pago) VALUES (:p_codigoMarca, :p_nombre);";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->bindParam(":p_codigoMarca", $this->getId_forma_pago());
                $sentencia->bindParam(":p_nombre", $this->getTipo_pago());
                $sentencia->execute();
                $sql = "UPDATE correlativo SET numero = numero + 1 WHERE tabla = 'forma_pago';";
                $sentencia = $this->dblink->prepare($sql);
                $sentencia->execute();
                $this->dblink->commit();
                return true;
            } else {
                throw new Exception("No se ha configurado el correlativo para la tabla marca.");
            }
        } catch (Exception $ex) {
            $this->dblink->rollBack();
            throw $ex;
        }
    }
    
    public function leerDatos($p_codigoMarca) {
        try {
            $sql = "select * from forma_pago where id_forma_pago = :p_codigoMarca;";
            $sentencia = $this->dblink->prepare($sql);
            $sentencia->bindParam(":p_codigoMarca", $p_codigoMarca);
            $sentencia->execute();
            $resultado = $sentencia->fetchAll(PDO::FETCH_ASSOC);
            return $resultado;
        } catch (Exception $exc) {
            throw $exc;
        }
    }

}
